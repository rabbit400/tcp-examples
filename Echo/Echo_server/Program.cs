﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Echo_server
{
    class Program
    {
        static readonly bool Write_data_to_console = true;
        static readonly int default_port = 42069;
        static void Main(string[] args)
        {
            int port = default_port;

            Console.WriteLine("Hello Server!");

            TcpListener new_connection_listener = new TcpListener(IPAddress.Any, port);
            new_connection_listener.Start();
            
            while (true)
            {
                // AcceptTcpClient() blocks and waits until someone tries to connect.
                // When we get a connection we start a new thread to read and process data.
                // Then wait for the next connection attempt.
                // Note that we never close the client here - reader thread has that responsibility.
                // Also note that we can have MANY reader threads if there are many clients.
                // ... so dont use this threading style for a high load service.
                TcpClient client = new_connection_listener.AcceptTcpClient();
                new Thread(() => Run_reader_thread(client)).Start();
            }
        }

        static void Run_reader_thread(TcpClient client)
        {
            Console.WriteLine("new connection");
            try
            {
                // client.GetStream() can throw networking exceptions - for example if the client dissconnected immediately.
                // using statements make sure we dont forget to close the stream and client
                // (stackoverflow says we only need to close one of client and stream - but we close both!)
                using (client)
                using (NetworkStream stream = client.GetStream())
                {
                    // Read data and send back till we get an exception.
                    // Its probably much better performance to use Read(byte[] buffer) with Write(byte[] buffer).
                    // See the client for that example - here we show the single byte version.
                    while (true)
                    {
                        // Either of ReadByte() and WriteByte(...) can throw various IOExceptions
                        // or ObjectDisposedException if stream is closed or broken.
                        // Simplest way to write code is to just handle them all the same way...
                        // Take them to mean "stream probably closed for some reason".
                        // Close stream, do any clean up and end thread.
                        // Its up to the client to reconnect if they want - our new-connections-thread is still running.

                        // ReadByte() returns the next byte as an ...int
                        //            if there is no data it blocks and wait till there is data
                        //            if stream is closed it returns -1
                        int read_result = stream.ReadByte();
                        if (read_result == -1)
                            throw new EndOfStreamException();
                        stream.WriteByte((byte)read_result);

                        if (Write_data_to_console)
                        {
                            Console.Out.Write((char)read_result);
                            Console.Out.Flush();
                        }
                    }
                }
            }
            catch (IOException) { }             // we will usually do the same clean up regardless of what exception
            catch (ObjectDisposedException) { } // absolutely you should log the exception in real code
            
            // Because our program is simple we have no clean-up to do.
            // The stream was already closed/Disposed by our using statements.
            // No references to anything exists outside this thread so the GC
            // will clean everything after thread ends.

            // In a more complex server we would likely have to do
            // some clean-up but thats for later examples.
        }
    }
}
