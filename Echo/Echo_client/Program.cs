﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Echo_client
{
    class Program
    {
        static readonly string default_host = "localhost";
        static readonly int default_port = 42069;
        static readonly bool print_buffer_delimiters = true; // enable to show delimeters between the data blocks
        static void Main(string[] args)
        {
            // We create a connection.
            // Then
            // - Start a reader thread that reads bytes and writes them to console (as ascii).
            // While
            // - Main thread read lines from console and send them to the server (as ascii).
            //
            // If everything works we get:
            // - User types lines into console.
            // - The data are pingponged via the server.
            // - Received in the reader thread and written to console.
            // - If/when the connection goes down we just exit.
            //
            // To keep the code simple:
            // - user cannot retry if connecting fails
            // - user can not reconnect
            // - cannot exit the program from the program (use ctrl-C)
            
            Console.WriteLine("Hello client! Connecting...");

            string host = default_host;
            int port = default_port;

            TcpClient client = new TcpClient();
            try
            {
                client.Connect(host, port);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Failed conneting, exception: " + ex);
                return;
            }

            Console.WriteLine("Connected");

            // runs the read loop in a new thread
            // runs the write loop in main thread
            // when the connection is dead the write loop ends and program finishes
            // (No, there is no way for the user to exit the program - use ctrl-C)
            using (var stream = client.GetStream())
            {
                new Thread(() => Run_read_loop(stream)).Start();
                Run_write_loop(stream);
            }
        }

        static void Run_write_loop(NetworkStream stream)
        {
            try
            {
                while (true)
                {
                    // Console.ReadLine() strips the line ending.
                    // We add it back to make the output look nicer.
                    // (Whatever we send comes back as-is and our reader thread writes it to console.)
                    string s = Console.ReadLine() + Environment.NewLine;
                    // We use ASCII so we can know a single byte matches a single character
                    byte[] bytes = System.Text.Encoding.ASCII.GetBytes(s);
                    stream.Write(bytes);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("\n Writer closed: " + ex);
            }
            catch (ObjectDisposedException ex)
            {
                Console.WriteLine("\n Writer closed: " + ex);
            }
        }

        static void Run_read_loop(NetworkStream stream)
        {
            try
            {
                byte[] buffer = new byte[100];
                while (true)
                {
                    // Read(buffer) will put data from the stream into buffer and return number of bytes read.
                    //  - If there is no data available it blocks and wait for data.
                    //  - If there is more data than fits in buffer, the extra data is saved for next Read.
                    // Can return less data than buffer.length (but not 0 bytes).
                    // So we NEED to check num_bytes_read.
                    int num_bytes_read = stream.Read(buffer);

                    // Minor note
                    // Documentation on NetworkStream.Read is not clear that its blocking (will wait for data).
                    // (https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.networkstream.read?view=net-5.0)
                    // Documantation on TcpClient.GetStream() is clear on this.
                    // "[...] Call the Read method to receive data arriving from the remote host. Both of these 
                    //  methods block until the specified operation is performed. "
                    // (https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.tcpclient.getstream?view=net-5.0)

                    // Again, it is important that we use num_bytes_read here.
                    string data_as_string = System.Text.Encoding.ASCII.GetString(buffer, 0, num_bytes_read);

                    Console.Out.Write(data_as_string);
                    if (print_buffer_delimiters)
                        Console.Out.Write(" | ");
                    Console.Out.Flush();
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("\n Reader closed: " + ex);
            }
            catch (ObjectDisposedException ex)
            {
                Console.WriteLine("\n Reader closed: " + ex);
            }
        }
    }
}
