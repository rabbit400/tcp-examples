﻿using Protocol;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Chat_server_using_reflection
{
    // This chat server has a global state in:
    //  - mapping client to username
    //  - mapping username to cliet
    //  - keeping a NetworkStream (and byte_writer) reference per client
    // All these are stored together in Current_users.
    //
    // Threading solution:
    //      A single thread handles "events", modifies the global state and send any
    //      writes/replies to clients.
    //      Many other threads exist - they do nothing to global state, only does
    //          obviously-single-threaded work and pushes events onto the event queues.
    // There are four queues for events:
    //      Incomming_connections, Incomming_messages, Incomming_read_errors
    //      and Incomming_new_connection_errors.
    // There are four types of threads:
    //  - main thread 
    //       - does startup
    //       - starts new-connection-listener thread
    //       - then becomes event handler thread.
    //  - one new connection listener thread
    //       - listens for new connections and put them onto Incomming_connections
    //       - puts any exception on Incomming_new_connection_errors
    //       - ends after any exception
    //  - many client reader threads
    //      -  one per client
    //      - listens for new data and parses it into messages
    //      - puts the messages onto Incomming_messages
    //      - puts any exceptions onto Incomming_read_errors 
    //      - ends after any exception (event handler thread does clean-up)
    //  - one event handler thread (main thread after startup)
    //      - handles any new events on the three event queues
    //          - New connection - create new client object and reader thread
    //          - New message, do one of:
    //              - new_username: set new username and notify all clients
    //              - tell_all: send the text to all clients
    //              - whisper_to: send whisper text to targeted clients
    //          - New reader error - remove that client data and close clients stream
    //
    // By making all state modifications in a single centralized thread we make thread-safety very simple.
    class Server_using_reflection_program
    {
        static readonly int Port = 42069;
        static readonly bool Use_reflection = true;

        // The Current_user hash-set is program state.
        // Only accessed from the event handler thread for thread safety.
        static readonly HashSet<Connected_user> Current_users = new HashSet<Connected_user>();

        // New connections from the new-connection-listener thread.
        static readonly ConcurrentQueue<TcpClient> Incomming_connections = new ConcurrentQueue<TcpClient>();

        // Messages from a client reader thread.
        static readonly ConcurrentQueue<(Connected_user user, Protocol_message message)> Incomming_messages = new ConcurrentQueue<(Connected_user, Protocol_message)>();

        // Exceptions from a client reader thread.
        // This always means the reader thread is ended.
        // I.e. event handler thread should remove the Connected_user and do clean up.
        static readonly ConcurrentQueue<(Connected_user user, Exception exception)> Incomming_read_errors = new ConcurrentQueue<(Connected_user, Exception)>();

        // Exceptions from the new-connection listener thread.
        // This always mean the listener thread is ended and program should end.
        static readonly ConcurrentQueue<Exception> Incomming_new_connection_errors = new ConcurrentQueue<Exception>();

        static readonly Protocol_message_types_map Protocol_mapper =
            Use_reflection
            ? Simple_protocol_message_deserialization_map
                    .Read_types_from_assembly(typeof(Set_username_message).Assembly)
            : Simple_protocol_message_deserialization_map
                    .Create_map_for_types(
                    typeof(Set_username_message),
                    typeof(Whisper_to_message),
                    typeof(Tell_all_message),
                    typeof(New_username_message),
                    typeof(Whispered_from_message),
                    typeof(Told_from_message)
                    );

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            TcpListener new_connection_listener = new TcpListener(IPAddress.Any, Port);
            try
            {
                new_connection_listener.Start();
            }
            catch (SocketException se)
            {
                Console.WriteLine("Failed opening listen socket: " + se);
                return;
            }

            // start a thread listening for incomming connections
            new Thread(() => Run_listen_for_new_connections(new_connection_listener)).Start();

            // handle events - i.e. main thread becomes event thread
            Run_handle_incomming_events();
        }
        public static void Run_listen_for_new_connections(TcpListener listener)
        {
            try
            {
                while (true)
                {
                    // can throw socket exception
                    TcpClient new_connection = listener.AcceptTcpClient();
                    Incomming_connections.Enqueue(new_connection);
                }
            }
            catch (SocketException se)
            {
                Incomming_new_connection_errors.Enqueue(se);
            }
        }
        public static void Run_reader_thread(Connected_user user, Byte_reader byte_reader)
        {
            Protocol_reader reader = new Simple_protocol_reader(byte_reader, Protocol_mapper);
            try
            {
                while (true)
                {
                    var next_message = reader.Read_message();
                    Incomming_messages.Enqueue((user, next_message));
                }
            }
            catch (Exception exception)
            {
                Incomming_read_errors.Enqueue((user, exception));
            }
        }
        public static void Run_handle_incomming_events()
        {
            while (true)
            {
                // if there is a new connection
                // - create client data object
                // - start reader thread
                if (Incomming_connections.TryDequeue(out var new_connection))
                {
                    // create per user object
                    NetworkStream stream = new_connection.GetStream();
                    Connected_user user = new Connected_user(stream, Protocol_mapper);

                    // START THREAD reading data from the new user
                    Byte_reader reader = new Buffered_network_byte_reader(stream);
                    new Thread(() => Run_reader_thread(user, reader)).Start();

                    // store the new user object
                    Current_users.Add(user);
                }
                // if a reader thread failed - time to close the connection
                else if (Incomming_read_errors.TryDequeue(out var err))
                {
                    var (user, exception) = err;
                    // remove the user object and close the stream (in case its not dead already)
                    Current_users.Remove(user);
                    err.user.Dispose();
                    Console.WriteLine("Disconnected user, cause: " + exception);
                }
                // if the listener thread failed - end program
                else if (Incomming_new_connection_errors.TryDequeue(out var listen_error))
                {
                    Console.WriteLine("Error listening for new connections: " + listen_error);
                    // return here ends the server program
                    return;
                }
                // if there is a new message from a client
                else if (Incomming_messages.TryDequeue(out var msg))
                {
                    // Note that on any error here we close (dispose) the corresponding connection.
                    // This will show up as an exception in the reader thread and then we read it
                    // from the Incomming_errors queue and there we handle proper cleanup.
                    var (user, command) = msg;
                    switch (command)
                    {
                        case Set_username_message cmd:
                            {
                                // update our data, then send new_username to all clients
                                user.Name = cmd.Username;
                                var message_to_send = new New_username_message(user.Name);
                                foreach (var target in Current_users)
                                    try { target.Send(message_to_send); }
                                    catch { target.Dispose(); }
                            }
                            break;
                        case Whisper_to_message cmd:
                            {
                                // send the whisper-text to all clients with the target username
                                var message_to_send = new Whispered_from_message(user.Name, cmd.Message);
                                var targets = Current_users
                                    .Where(u => u.Name == cmd.Target_username);
                                foreach (var target in targets)
                                    try { target.Send(message_to_send); }
                                    catch { target.Dispose(); }
                            }
                            break;
                        case Tell_all_message cmd:
                            // send the text to all clients
                            var message_to_send_ = new Told_from_message(user.Name, cmd.Text);
                            foreach (var target in Current_users)
                                try { target.Send(message_to_send_); }
                                catch { target.Dispose(); }
                            break;
                        default:
                            // any unlisted type - should never happen
                            // we handle it like if a server command was sent by client
                            // not because it makes sense but because we dont care in this silly example program
                        case Whispered_from_message _:
                        case Told_from_message _:
                        case New_username_message _:
                            // this is an error, clients are not allowed to send these commands
                            // dispose closes stream
                            // -> reader thread gets error
                            // -> we get reader error event in Incomming_read_errors
                            // -> we handle that error and do clean-up
                            user.Dispose();
                            break;
                    }
                }
            }
        }
    }


    public class Connected_user : IDisposable
    {
        private readonly NetworkStream NetStream;
        private readonly Buffered_network_byte_writer Network_writer;
        private readonly Protocol_writer Protocol_writer;
        public Utf8_string Name { get; set; }
        public void Send(Protocol_message msg)
        {
            Protocol_writer.Write_message(msg);
            Network_writer.Flush();
        }
        public Connected_user(NetworkStream stream, Protocol_message_types_map map)
        {
            NetStream = stream;
            Network_writer = new Buffered_network_byte_writer(stream);
            Protocol_writer = new Simple_protocol_writer(Network_writer, map);
        }
        public void Dispose() => NetStream.Dispose();
    }
    public class Utf8_string : IEnumerable<byte>, IEquatable<Utf8_string>
    {
        private readonly byte[] m_Bytes;
        public int Length => m_Bytes.Length;
        public IEnumerator<byte> GetEnumerator() { foreach (var b in m_Bytes) yield return b; }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static readonly Utf8_string Empty = new Utf8_string("");
        private Utf8_string(byte[] bytes) => m_Bytes = bytes;
        public Utf8_string() => m_Bytes = null;
        public Utf8_string(string s) => m_Bytes = UTF8Encoding.UTF8.GetBytes(s);
        public override string ToString() => UTF8Encoding.UTF8.GetString(m_Bytes);
        public bool Equals(Utf8_string other)
        {
            if (other != null || m_Bytes.Length != other.m_Bytes.Length) return false;
            for (int i = 0; i < m_Bytes.Length; i++)
                if (m_Bytes[i] != other.m_Bytes[i])
                    return false;
            return true;
        }
        public override bool Equals(object obj) => Equals(obj as Utf8_string);
        public override int GetHashCode()
        {
            int h = 934768;
            foreach (var b in m_Bytes) h = h * 17 + b;
            return h;
        }
        public void Write(Byte_writer w) => w.Write_byte_array(m_Bytes);
        public static Utf8_string Read(Byte_reader r) => new Utf8_string(r.Read_byte_array());
    }

    [Protocol_message(Protocol_message_type_id.Types_enum.Set_username)]
    public class Set_username_message : Protocol_message
    {
        public readonly Utf8_string Username;
        public Set_username_message(Utf8_string username) => Username = username;
        public void Write(Byte_writer writer)
        {
            Username.Write(writer);
        }
        public static Set_username_message Read(Byte_reader reader)
        {
            Utf8_string username = Utf8_string.Read(reader);
            return new Set_username_message(username);
        }
    }
    [Protocol_message(Protocol_message_type_id.Types_enum.Whisper_to)]
    public class Whisper_to_message : Protocol_message
    {
        public readonly Utf8_string Target_username;
        public readonly Utf8_string Message;
        public Whisper_to_message(Utf8_string target_username, Utf8_string message)
        {
            Target_username = target_username;
            Message = message;
        }
        public void Write(Byte_writer writer)
        {
            Target_username.Write(writer);
            Message.Write(writer);
        }
        public static Whisper_to_message Read(Byte_reader reader)
        {
            var target_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Whisper_to_message(target_username, message);
        }
    }
    [Protocol_message(Protocol_message_type_id.Types_enum.Tell_all)]
    public class Tell_all_message : Protocol_message
    {
        public readonly Utf8_string Text;
        public Tell_all_message(Utf8_string text) => Text = text;
        public void Write(Byte_writer writer)
        {
            Text.Write(writer);
        }
        public static Tell_all_message Read(Byte_reader reader)
        {
            var text = Utf8_string.Read(reader);
            return new Tell_all_message(text);
        }
    }
    [Protocol_message(Protocol_message_type_id.Types_enum.New_username)]
    public class New_username_message : Protocol_message
    {
        public readonly Utf8_string Username;
        public New_username_message(Utf8_string text) => Username = text;
        public void Write(Byte_writer writer)
        {
            Username.Write(writer);
        }
        public static New_username_message Read(Byte_reader reader)
        {
            var text = Utf8_string.Read(reader);
            return new New_username_message(text);
        }
    }
    [Protocol_message(Protocol_message_type_id.Types_enum.Whispered_from)]
    public class Whispered_from_message : Protocol_message
    {
        public readonly Utf8_string Sender_username;
        public readonly Utf8_string Text;
        public Whispered_from_message(Utf8_string target_username, Utf8_string message)
        {
            Sender_username = target_username;
            Text = message;
        }
        public void Write(Byte_writer writer)
        {
            Sender_username.Write(writer);
            Text.Write(writer);
        }
        public static Whispered_from_message Read(Byte_reader reader)
        {
            var sender_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Whispered_from_message(sender_username, message);
        }
    }
    [Protocol_message(Protocol_message_type_id.Types_enum.Told_from)]
    public class Told_from_message : Protocol_message
    {
        public readonly Utf8_string Sender_username;
        public readonly Utf8_string Text;
        public Told_from_message(Utf8_string target_username, Utf8_string message)
        {
            Sender_username = target_username;
            Text = message;
        }
        public void Write(Byte_writer writer)
        {
            Sender_username.Write(writer);
            Text.Write(writer);
        }
        public static Told_from_message Read(Byte_reader reader)
        {
            var sender_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Told_from_message(sender_username, message);
        }
    }
}
