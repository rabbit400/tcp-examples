﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;

namespace Protocol
{
    public class Growing_byte_buffer
    {
        private byte[] m_Buffer;
        private int m_Num_bytes;

        public Growing_byte_buffer(int initial_capacity = 16)
            => m_Buffer = new byte[initial_capacity];
        public void Clear()
            => m_Num_bytes = 0;
        public ReadOnlySpan<byte> Current_contents
            => new ReadOnlySpan<byte>(m_Buffer, 0, m_Num_bytes);

        public void Append_byte(byte b)
        {
            if (m_Num_bytes >= m_Buffer.Length) Grow_buffer();
            m_Buffer[m_Num_bytes++] = b;
        }
        private void Grow_buffer()
        {
            byte[] new_buffer = new byte[2 * m_Buffer.Length + 1];
            Array.Copy(m_Buffer, new_buffer, m_Num_bytes);
            m_Buffer = new_buffer;
        }
    }
    public class Buffered_network_byte_writer : Byte_writer
    {
        private readonly NetworkStream m_Stream;
        private readonly Growing_byte_buffer m_Buffer = new Growing_byte_buffer();

        public Buffered_network_byte_writer(NetworkStream stream)
            => m_Stream = stream;
        public void Write_byte(byte b)
            => m_Buffer.Append_byte(b);

        public void Flush()
        {
            m_Stream.Write(m_Buffer.Current_contents);
            m_Buffer.Clear();
        }
    }
    public class Buffered_network_byte_reader : Byte_reader
    {
        private readonly NetworkStream m_Stream;
        private readonly byte[] m_Buffer;
        private int m_Num_bytes_in_buffer = 0;
        private int m_Num_bytes_read = 0;

        public Buffered_network_byte_reader(NetworkStream stream, int buffer_size = 1024)
        {
            m_Stream = stream;
            m_Buffer = new byte[buffer_size];
        }
        public byte Read_byte()
        {
            if (m_Num_bytes_read >= m_Num_bytes_in_buffer)
            { // if we delivered all data in buffer its time to fetch more
                m_Num_bytes_in_buffer = m_Stream.Read(m_Buffer); // can throw - like it should!
                m_Num_bytes_read = 0;
            }
            return (byte)m_Buffer[m_Num_bytes_read++];
        }
    }
    public class Simple_protocol_writer : Protocol_writer
    {
        private readonly Protocol_message_types_map m_Mapper;

        public readonly Byte_writer Byte_writer;
        Byte_writer Protocol_writer.Byte_writer => Byte_writer;

        public Simple_protocol_writer(Byte_writer writer, Protocol_message_types_map map)
        {
            m_Mapper = map;
            Byte_writer = writer;
        }
        public void Write_message(Protocol_message message)
        {
            m_Mapper.Get_id(message).Write(Byte_writer);
            message.Write(Byte_writer);
        }
    }
    public class Simple_protocol_reader : Protocol_reader
    {
        private readonly Protocol_message_types_map m_Mapper;

        public readonly Byte_reader Byte_reader;
        Byte_reader Protocol_reader.Byte_reader => Byte_reader;

        public Simple_protocol_reader(Byte_reader byte_reader, Protocol_message_types_map mapper)
        {
            Byte_reader = byte_reader;
            m_Mapper = mapper;
        }
        public Protocol_message Read_message()
        {
            Protocol_message_type_id type = Protocol_message_type_id.Read(Byte_reader);
            var deserializer = m_Mapper.Get_reader(type);
            return deserializer(Byte_reader);
        }
    }
    public static class Basic_types_serialization
    {
        public static void Write_uint32(this Byte_writer w, UInt32 u)
        {
            byte b0 = (byte)((u / (256u * 256u * 256u)) % 256);
            byte b1 = (byte)((u / (256u * 256u)) % 256);
            byte b2 = (byte)((u / (256u)) % 256);
            byte b3 = (byte)((u) % 256);
            w.Write_byte(b3);
            w.Write_byte(b2);
            w.Write_byte(b1);
            w.Write_byte(b0);
        }
        public static UInt32 Read_uint32(this Byte_reader r)
        {
            UInt32 ret = 0;
            ret += r.Read_byte();
            ret += r.Read_byte() * 256u;
            ret += r.Read_byte() * 256u * 256u;
            ret += r.Read_byte() * 256u * 256u * 256u;
            return ret;
        }
        public static void Write_byte_array(this Byte_writer w, byte[] array)
        {
            w.Write_uint32((UInt32)array.Length);
            foreach (var b in array) w.Write_byte(b);
        }
        public static byte[] Read_byte_array(this Byte_reader r)
        {
            UInt32 n = r.Read_uint32();
            byte[] array = new byte[n];
            for (int i = 0; i < n; i++) array[i] = r.Read_byte();
            return array;
        }
    }
    /// <summary>
    /// Simple implementation of mapping both message type to message type id
    /// and message type id to deserializer. 
    /// </summary>
    /// Use the static functions to create a mapping.
    ///  - Create_map_for_types for a list of hardcoded types.
    ///  - Read_types_from_assembly
    public class Simple_protocol_message_deserialization_map : Protocol_message_types_map
    {
        private readonly ImmutableDictionary<Protocol_message_type_id, Func<Byte_reader, Protocol_message>>
            m_Type_id_to_deserializer;
        private readonly ImmutableDictionary<Type, Protocol_message_type_id>
            m_Type_to_type_id;

        public Protocol_message_type_id Get_id(Type type)
            => m_Type_to_type_id[type];
        public Func<Byte_reader, Protocol_message> Get_reader(Protocol_message_type_id message_type)
            => m_Type_id_to_deserializer[message_type];

        private Simple_protocol_message_deserialization_map(
            ImmutableDictionary<Protocol_message_type_id, Func<Byte_reader, Protocol_message>> type_id_to_deserializer,
            ImmutableDictionary<Type, Protocol_message_type_id> type_to_type_id)
        {
            m_Type_id_to_deserializer = type_id_to_deserializer;
            m_Type_to_type_id = type_to_type_id;
        }

        public static Simple_protocol_message_deserialization_map
            Create_map_for_types(params Type[] message_types)
            => Create_map_for_types((IEnumerable<Type>)message_types);

        public static Simple_protocol_message_deserialization_map
            Create_map_for_types(IEnumerable<Type> message_types)
        {
            var type_id_to_deserializer = ImmutableDictionary
                .CreateBuilder<Protocol_message_type_id, Func<Byte_reader, Protocol_message>>();
            var type_to_type_id = ImmutableDictionary
                .CreateBuilder<Type, Protocol_message_type_id>();

            // used only to give better error messages
            var type_id_to_type = new Dictionary<Protocol_message_type_id, Type>();

            foreach (var t in message_types)
            {
                // throws an informative exception if t breaks the Protocol_message contract
                Enforce_valid_message_type(t);

                var type_id = Get_id_attribute(t).Message_type_id;
                var deserializer = Create_deserializer(t);

                if (!type_id_to_type.TryAdd(type_id, t))
                {
                    string msg = "Duplicate type id, Id: " + type_id + "; "
                        + "First type: " + type_id_to_type[type_id] + "; "
                        + "Second type: " + t + ";";
                    throw new ArgumentException(msg);
                }

                if (!type_to_type_id.TryAdd(t, type_id))
                    throw new ArgumentException("Duplicate type: " + t);

                type_id_to_deserializer.Add(type_id, deserializer);
            }

            return new Simple_protocol_message_deserialization_map(
                type_id_to_deserializer.ToImmutable(),
                type_to_type_id.ToImmutable());
        }

        public static Simple_protocol_message_deserialization_map Read_types_from_assembly(params Assembly[] assemblies)
        {
            List<Type> message_types = new List<Type>();

            foreach (var assembly in assemblies.Distinct())
            {
                foreach (var type in assembly.GetTypes())
                {
                    // A valid protocol message type must both:
                    //  1) implement the Protocol_message interface
                    //  2) have a Protocol_message attribute
                    //  - If only 1: exception is thrown in Create_map_for_types.
                    //  - If only 2: we throw exception here, with a more helpful message.
                    //
                    // Note: does not apply to abstract classes or interfaces.
                    // I.e. they may implement the interface but must NOT have the attribute.
                    // The attribute mandate is (naturally) passed on to any "real" class inheriting them.
                    if (Is_non_abstract_protocol_message_implementation(type))
                    {
                        message_types.Add(type);
                    }
                    else if (type.GetCustomAttribute<Protocol_messageAttribute>() != null)
                    {
                        string msg = "A type with Protocol_message attribute is not a valid protocol message type. "
                            + "Requirements on type: must implement the Protocol_message -interface-, "
                            + " not be abstract, and not itself be an interface. "
                            + "Type: " + type;
                        throw new ArgumentException(msg);
                    }
                }
            }

            return Create_map_for_types(message_types);

            bool Is_non_abstract_protocol_message_implementation(Type t)
                => typeof(Protocol_message).IsAssignableFrom(t)
                && (!t.IsAbstract)
                && (!t.IsInterface);
        }
        /// <summary>
        /// Throws an informative exception if t is not a valid protocol message type.
        /// </summary>
        /// Rules:
        ///  - Implements Protocol_message interface.
        ///  - Is not interface.
        ///  - Is not abstract.
        ///  - Is not generic.
        ///  - Has a Protocol_message attibute.
        ///  - Has a single method matching: Read(Byte_reader).
        ///  - Read is public.
        ///  - Read is static.
        ///  - Read returns type t.
        private static void Enforce_valid_message_type(Type t)
        {
            if (!typeof(Protocol_message).IsAssignableFrom(t))
                throw new ArgumentException("Type does not implement Protocol_message, type: " + t);
            if (t.IsAbstract || t.IsInterface)
                throw new ArgumentException("Abstract protocol message type  not allowed, type: " + t);
            if (t.IsGenericType)
                throw new ArgumentException("Generic protocol message type not allowed, type: " + t);

            if (null == t.GetCustomAttribute<Protocol_messageAttribute>())
                throw new ArgumentException("Protocol_message type missing Protocol_message attribute, type: " + t);

            MethodInfo method;
            try
            {
                method = t.GetMethod("Read", new Type[] { typeof(Byte_reader) });
            }
            catch (AmbiguousMatchException)
            {
                throw new ArgumentException(
                    "More than one method named Read for single argument of type Byte_reader. "
                    + "Type: " + t);
            }
            if (!method.IsStatic) throw new ArgumentException("Non-static Read method not allowed, type: " + t);
            if (!method.IsPublic) throw new ArgumentException("Non-public Read method not allowed, type: " + t);
            if (method.ReturnType != t)
                throw new ArgumentException("Read method does not return the message's own type, type: " + t);
        }

        // Assumes Enforce_valid_message_type(t) is called first
        private static Protocol_messageAttribute Get_id_attribute(Type t)
            => t.GetCustomAttribute<Protocol_messageAttribute>();
        
        // Assumes Enforce_valid_message_type(t) is called first
        private static Func<Byte_reader, Protocol_message> Create_deserializer(Type t)
        {
            MethodInfo method = t.GetMethod("Read", new Type[] { typeof(Byte_reader) });
            var deleg = method.CreateDelegate(typeof(Func<Byte_reader, Protocol_message>));
            return (Func<Byte_reader, Protocol_message>)deleg;
        }
    }
}
