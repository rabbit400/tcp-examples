﻿using System;

namespace Protocol
{
    public interface Byte_writer
    {
        public void Write_byte(byte b);
    }
    public interface Byte_reader
    {
        public byte Read_byte();
    }
    public interface Protocol_writer
    {
        public Byte_writer Byte_writer { get; }
        public void Write_message(Protocol_message message);
    }
    public interface Protocol_reader
    {
        public Byte_reader Byte_reader { get; }
        public Protocol_message Read_message();
    }
    /// <summary>
    /// Interface for "message" types that have an associated Id. 
    /// Thus they can be written to a protocol writer where the Id tells any
    /// reader what specific message type to read/deserialize.
    /// </summary>
    /// Non-abstract classes implementing this interface:
    ///  - Must not be generic.
    ///  - Must have a static deserialization function called Read(Byte_reader reader).
    ///  - Read must return an instance of the class.
    ///    As in 'Cute_msg_type Read(Byte_reader)', not 'Protocol_message Read(Byte_reader)'.
    ///  - Must have a Protocol_message attribute with a unique type id.
    /// This is enforced by the reflection code when building a mapping
    /// from type ids to deserializers.
    ///
    // Illustrates how to set the attribute.
    /* [Protocol_message(Protocol_message_type_id.Types_enum.Message_type_value)] */
    public interface Protocol_message
    {
        public void Write(Byte_writer writer);
        // Illustrates the mandatory Read function.
        // public static Implementing_type Read(Byte_reader reader);
    }
    /// <summary>
    /// Id for the type of a message used by Protocol_writer and Protocol_reader.
    /// Written as a prefix before any message so that a reader know which message
    /// type to deserialize.
    /// </summary>
    public class Protocol_message_type_id : IEquatable<Protocol_message_type_id>
    {
        public enum Types_enum : byte
        {
            Set_username = 1,
            Whisper_to,
            Tell_all,
            Whispered_from,
            Told_from,
            New_username,
        }

        public readonly Types_enum Type_id;
        public Protocol_message_type_id(Types_enum type_id)
            => Type_id = type_id;

        public bool Equals(Protocol_message_type_id other)
            => other != null && Type_id == other.Type_id;
        public override bool Equals(object obj) => Equals(obj as Protocol_message_type_id);
        public override int GetHashCode() => 876 + Type_id.GetHashCode();
        public override string ToString() => "Type id: " + Type_id;

        public void Write(Byte_writer w)
            => w.Write_byte((byte)Type_id);
        public static Protocol_message_type_id Read(Byte_reader r)
            => new Protocol_message_type_id((Types_enum)r.Read_byte());
    }
    /// <summary>
    /// Attribute with a message type id for Protocol_message implementations.
    /// </summary>
    /// A type with this attribute MUST be a non-abstract Protocol_message implementation.
    /// A non-abstract Protocol_message implementation MUST have this attribute.
    /// See Protocol_message documentation for more details.
    public class Protocol_messageAttribute : Attribute
    {
        public Protocol_message_type_id Message_type_id;
        public Protocol_messageAttribute(Protocol_message_type_id type_id)
            => Message_type_id = type_id;
        public Protocol_messageAttribute(Protocol_message_type_id.Types_enum type_enum)
            => Message_type_id = new Protocol_message_type_id(type_enum);
    }
    /// <summary>
    /// Interface used by protocol writers and readers to map a type to message id
    /// or a message id to a deserializer function.
    /// </summary>
    public interface Protocol_message_types_map
    {
        public Protocol_message_type_id Get_id(Type type);
        public Protocol_message_type_id Get_id(Protocol_message e) => Get_id(e.GetType());
        public Func<Byte_reader, Protocol_message> Get_reader(Protocol_message_type_id message_type);
    }
}
