﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;

namespace Protocol
{
    public class Growing_byte_buffer
    {
        private byte[] m_Buffer;
        private int m_Num_bytes;

        public Growing_byte_buffer(int initial_capacity = 16)
            => m_Buffer = new byte[initial_capacity];
        public void Clear()
            => m_Num_bytes = 0;
        public ReadOnlySpan<byte> Current_contents
            => new ReadOnlySpan<byte>(m_Buffer, 0, m_Num_bytes);

        public void Append_byte(byte b)
        {
            if (m_Num_bytes >= m_Buffer.Length) Grow_buffer();
            m_Buffer[m_Num_bytes++] = b;
        }
        private void Grow_buffer()
        {
            byte[] new_buffer = new byte[2 * m_Buffer.Length + 1];
            Array.Copy(m_Buffer, new_buffer, m_Num_bytes);
            m_Buffer = new_buffer;
        }
    }
    public class Buffered_network_byte_writer : Byte_writer
    {
        private readonly NetworkStream m_Stream;
        private readonly Growing_byte_buffer m_Buffer = new Growing_byte_buffer();

        public Buffered_network_byte_writer(NetworkStream stream)
            => m_Stream = stream;
        public void Write_byte(byte b)
            => m_Buffer.Append_byte(b);

        public void Flush()
        {
            m_Stream.Write(m_Buffer.Current_contents);
            m_Buffer.Clear();
        }
    }
    public class Buffered_network_byte_reader : Byte_reader
    {
        private readonly NetworkStream m_Stream;
        private readonly byte[] m_Buffer;
        private int m_Num_bytes_in_buffer = 0;
        private int m_Num_bytes_read = 0;

        public Buffered_network_byte_reader(NetworkStream stream, int buffer_size = 1024)
        {
            m_Stream = stream;
            m_Buffer = new byte[buffer_size];
        }
        public byte Read_byte()
        {
            if (m_Num_bytes_read >= m_Num_bytes_in_buffer)
            { // if we delivered all data in buffer its time to fetch more
                m_Num_bytes_in_buffer = m_Stream.Read(m_Buffer); // can throw - like it should!
                m_Num_bytes_read = 0;
            }
            return (byte)m_Buffer[m_Num_bytes_read++];
        }
    }
    public class Simple_protocol_writer : Protocol_writer
    {
        public readonly Byte_writer Byte_writer;
        Byte_writer Protocol_writer.Byte_writer => Byte_writer;

        public Simple_protocol_writer(Byte_writer writer)
            => Byte_writer = writer;

        public void Write_message(Protocol_message message)
        {
            message.Message_type_id.Write(Byte_writer);
            message.Write(Byte_writer);
        }
    }
    public class Simple_protocol_reader : Protocol_reader
    {
        private readonly Protocol_deserialization_map m_Mapper;

        public readonly Byte_reader Byte_reader;
        Byte_reader Protocol_reader.Byte_reader => Byte_reader;

        public Simple_protocol_reader(Byte_reader byte_reader, Protocol_deserialization_map mapper)
        {
            Byte_reader = byte_reader;
            m_Mapper = mapper;
        }
        public Protocol_message Read_message()
        {
            Protocol_message_type_id type = Protocol_message_type_id.Read(Byte_reader);
            var deserializer = m_Mapper.Get_reader(type);
            return deserializer(Byte_reader);
        }
    }
    public static class Basic_types_serialization
    {
        public static void Write_uint32(this Byte_writer w, UInt32 u)
        {
            byte b0 = (byte)((u / (256u * 256u * 256u)) % 256);
            byte b1 = (byte)((u / (256u * 256u)) % 256);
            byte b2 = (byte)((u / (256u)) % 256);
            byte b3 = (byte)((u) % 256);
            w.Write_byte(b3);
            w.Write_byte(b2);
            w.Write_byte(b1);
            w.Write_byte(b0);
        }
        public static UInt32 Read_uint32(this Byte_reader r)
        {
            UInt32 ret = 0;
            ret += r.Read_byte();
            ret += r.Read_byte() * 256u;
            ret += r.Read_byte() * 256u * 256u;
            ret += r.Read_byte() * 256u * 256u * 256u;
            return ret;
        }
        public static void Write_byte_array(this Byte_writer w, byte[] array)
        {
            w.Write_uint32((UInt32)array.Length);
            foreach (var b in array) w.Write_byte(b);
        }
        public static byte[] Read_byte_array(this Byte_reader r)
        {
            UInt32 n = r.Read_uint32();
            byte[] array = new byte[n];
            for (int i = 0; i < n; i++) array[i] = r.Read_byte();
            return array;
        }
    }
    public class Simple_protocol_deserialization_map : Protocol_deserialization_map
    {
        private readonly ImmutableDictionary<Protocol_message_type_id, Func<Byte_reader, Protocol_message>> m_Map;
        public Simple_protocol_deserialization_map(ImmutableDictionary<Protocol_message_type_id, Func<Byte_reader, Protocol_message>> map)
            => m_Map = map;
        public Func<Byte_reader, Protocol_message> Get_reader(Protocol_message_type_id message_type)
            => m_Map[message_type];

        public static Simple_protocol_deserialization_map
            Create_map_for_message_types(params Protocol_message[] message_instances)
        {
            var ret = ImmutableDictionary
                .CreateBuilder<Protocol_message_type_id, Func<Byte_reader, Protocol_message>>();
            foreach (var message in message_instances)
            {
                ret.Add(message.Message_type_id, message.Read);
            }
            return new Simple_protocol_deserialization_map(ret.ToImmutable());
        }

        public static Simple_protocol_deserialization_map Read_from_assembly(params Assembly[] assemblies)
        {
            var ret = ImmutableDictionary
                .CreateBuilder<Protocol_message_type_id, Func<Byte_reader, Protocol_message>>();

            bool Implements_protocol_message(Type t)
                => typeof(Protocol_message).IsAssignableFrom(t);
            bool Is_not_abstract(Type t)
                => !(t.IsAbstract || t.IsInterface);
            bool Is_protocol_message_type(Type t)
                => Implements_protocol_message(t)
                && Is_not_abstract(t);

            foreach (var assembly in assemblies.Distinct())
            {
                foreach (var type in assembly.GetTypes().Where(Is_protocol_message_type))
                {
                    if (type.IsGenericType)
                        throw new ArgumentException(
                            "Generic protocol message type not allowed, type: " + type);

                    // Throws an informative exception if there is no empty contructor.
                    Protocol_message empty_constructed_instance
                        = Use_empty_constructor_on_protocol_message_type(type);

                    var type_id = empty_constructed_instance.Message_type_id;
                    Func<Byte_reader, Protocol_message> deserializer = empty_constructed_instance.Read;

                    if (ret.ContainsKey(type_id))
                        throw new ArgumentException("Duplicate protocol message type ids. "
                            + "Type id " + type_id + " is used more than once. "
                            + "Second encounter fo this id is for type: " + type);

                    ret.Add(type_id, deserializer);
                }
            }

            return new Simple_protocol_deserialization_map(ret.ToImmutable());
        }

        private static Protocol_message Use_empty_constructor_on_protocol_message_type(Type type)
        {
            try
            {
                return (Protocol_message)Activator.CreateInstance(type);
            }
            catch (MissingMethodException)
            {
                throw new ArgumentException(
                    "Protocol message type with no empty constructor, type: " + type);
            }
            catch (MethodAccessException)
            {
                throw new ArgumentException(
                    "Missing access rights to empty constructor for protocol message type: " + type);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "Exception trying to use empty constructor on protocol message type:" + type, ex);
            }
        }
    }
}
