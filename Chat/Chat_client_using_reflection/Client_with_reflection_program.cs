﻿using Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Chat_client_using_reflection
{
    class Client_with_reflection_program
    {
        static readonly string Host = "localhost";
        static readonly int Port = 42069;
        static readonly bool Use_reflection = true;
        static void Main()
        {
            Console.WriteLine("Hello second client!");

            Protocol_deserialization_map protocol_mapper;
            if (Use_reflection)
            { // this will create a map for all types in the assembly
                protocol_mapper = Simple_protocol_deserialization_map
                    .Read_from_assembly(typeof(Set_username_message).Assembly);
            }
            else
            { // this will create a map for the hardcoded types only
                protocol_mapper = Simple_protocol_deserialization_map
                    .Create_map_for_message_types(
                    new Set_username_message(),
                    new Whisper_to_message(),
                    new Tell_all_message(),
                    new New_username_message(),
                    new Whispered_from_message(),
                    new Told_from_message()
                    );
            }

            TcpClient client = new TcpClient();
            try
            {
                client.Connect(Host, Port);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Failed connecting: " + ex.ToString());
                return;
            }

            Console.WriteLine("Whats your username?");
            string our_username = Console.ReadLine();

            using (NetworkStream stream = client.GetStream())
            {
                // start reader thread
                {
                    var byte_reader = new Buffered_network_byte_reader(stream);
                    var protocol_reader = new Simple_protocol_reader(byte_reader, protocol_mapper);
                    new Thread(() => Run_reader(protocol_reader))
                        .Start();
                }

                var byte_writer = new Buffered_network_byte_writer(stream);
                var protocol_writer = new Simple_protocol_writer(byte_writer);

                try
                {
                    // Send a "set our name" message
                    {
                        var username_utf8 = new Utf8_string(our_username);
                        var set_username_message = new Set_username_message(username_utf8);
                        protocol_writer.Write_message(set_username_message);
                        byte_writer.Flush();
                    }

                    while (true)
                    {
                        // send "tell all" message
                        string line = Console.ReadLine();
                        var tell_all_message = new Tell_all_message(new Utf8_string(line));
                        protocol_writer.Write_message(tell_all_message);
                        byte_writer.Flush();
                    }
                }
                catch (Exception e) when (e is IOException || e is ObjectDisposedException)
                {
                    Console.WriteLine("Connection closed: " + e);
                }
            }
        }

        static void Run_reader(Protocol_reader reader)
        {
            try
            {
                while (true)
                {
                    // read what type of message the server sent
                    var message = reader.Read_message();
                    // the type tells us how to read the message body

                    switch (message)
                    {
                        case Whispered_from_message msg:
                            Console.WriteLine(msg.Sender_username + " (whispers): " + msg.Text);
                            break;
                        case Told_from_message msg:
                            Console.WriteLine(msg.Sender_username + " : " + msg.Text);
                            break;
                        case New_username_message msg:
                            Console.WriteLine("New username: " + msg.Username);
                            break;
                        case Set_username_message _: // these client commands should never
                        case Whisper_to_message _:   // be sent from the server
                        case Tell_all_message _:     // its an ERROR if we receive them
                            throw new IOException("Protocol Error: Received client command from server.");
                        default:
                            throw new IOException("Protocol Error: Received unrecognized message type.");
                    };
                }
            }
            catch (Exception e) when (e is IOException || e is ObjectDisposedException)
            {
                Console.WriteLine("Read stream closed: " + e);
            }
        }
    }
    public class Utf8_string : IEnumerable<byte>, IEquatable<Utf8_string>
    {
        private readonly byte[] m_Bytes;
        public int Length => m_Bytes.Length;
        public IEnumerator<byte> GetEnumerator() { foreach (var b in m_Bytes) yield return b; }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static readonly Utf8_string Empty = new Utf8_string("");
        private Utf8_string(byte[] bytes) => m_Bytes = bytes;
        public Utf8_string() => m_Bytes = null;
        public Utf8_string(string s) => m_Bytes = UTF8Encoding.UTF8.GetBytes(s);
        public override string ToString() => UTF8Encoding.UTF8.GetString(m_Bytes);
        public bool Equals(Utf8_string other)
        {
            if (other != null || m_Bytes.Length != other.m_Bytes.Length) return false;
            for (int i = 0; i < m_Bytes.Length; i++)
                if (m_Bytes[i] != other.m_Bytes[i])
                    return false;
            return true;
        }
        public override bool Equals(object obj) => Equals(obj as Utf8_string);
        public override int GetHashCode()
        {
            int h = 934768;
            foreach (var b in m_Bytes) h = h * 17 + b;
            return h;
        }
        public void Write(Byte_writer w) => w.Write_byte_array(m_Bytes);
        public static Utf8_string Read(Byte_reader r) => new Utf8_string(r.Read_byte_array());
    }
    public class Set_username_message : Protocol_message
    {
        public readonly Utf8_string Username;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.Set_username);

        public Set_username_message() : this(Utf8_string.Empty) { }
        public Set_username_message(Utf8_string username) => Username = username;
        public void Write(Byte_writer writer)
        {
            Username.Write(writer);
        }
        public static Set_username_message Read(Byte_reader reader)
        {
            var username = Utf8_string.Read(reader);
            return new Set_username_message(username);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
    public class Whisper_to_message : Protocol_message
    {
        public readonly Utf8_string Target_username;
        public readonly Utf8_string Message;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.Whisper_to);

        public Whisper_to_message() : this(Utf8_string.Empty, Utf8_string.Empty) { }
        public Whisper_to_message(Utf8_string target_username, Utf8_string message)
        {
            Target_username = target_username;
            Message = message;
        }
        public void Write(Byte_writer writer)
        {
            Target_username.Write(writer);
            Message.Write(writer);
        }
        public static Whisper_to_message Read(Byte_reader reader)
        {
            var target_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Whisper_to_message(target_username, message);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
    public class Tell_all_message : Protocol_message
    {
        public readonly Utf8_string Text;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.Tell_all);

        public Tell_all_message() : this(Utf8_string.Empty) { }
        public Tell_all_message(Utf8_string text) => Text = text;
        public void Write(Byte_writer writer)
        {
            Text.Write(writer);
        }
        public static Tell_all_message Read(Byte_reader reader)
        {
            var text = Utf8_string.Read(reader);
            return new Tell_all_message(text);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
    public class New_username_message : Protocol_message
    {
        public readonly Utf8_string Username;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.New_username);

        public New_username_message() : this(Utf8_string.Empty) { }
        public New_username_message(Utf8_string text) => Username = text;
        public void Write(Byte_writer writer)
        {
            Username.Write(writer);
        }
        public static New_username_message Read(Byte_reader reader)
        {
            var text = Utf8_string.Read(reader);
            return new New_username_message(text);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
    public class Whispered_from_message : Protocol_message
    {
        public readonly Utf8_string Sender_username;
        public readonly Utf8_string Text;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.Whispered_from);

        public Whispered_from_message() : this(Utf8_string.Empty, Utf8_string.Empty) { }
        public Whispered_from_message(Utf8_string target_username, Utf8_string message)
        {
            Sender_username = target_username;
            Text = message;
        }
        public void Write(Byte_writer writer)
        {
            Sender_username.Write(writer);
            Text.Write(writer);
        }
        public static Whispered_from_message Read(Byte_reader reader)
        {
            var sender_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Whispered_from_message(sender_username, message);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
    public class Told_from_message : Protocol_message
    {
        public readonly Utf8_string Sender_username;
        public readonly Utf8_string Text;
        public static readonly Protocol_message_type_id Message_type
            = new Protocol_message_type_id(Protocol_message_type_id.Types_enum.Told_from);

        public Told_from_message() : this(Utf8_string.Empty, Utf8_string.Empty) { }
        public Told_from_message(Utf8_string target_username, Utf8_string message)
        {
            Sender_username = target_username;
            Text = message;
        }
        public void Write(Byte_writer writer)
        {
            Sender_username.Write(writer);
            Text.Write(writer);
        }
        public static Told_from_message Read(Byte_reader reader)
        {
            var sender_username = Utf8_string.Read(reader);
            var message = Utf8_string.Read(reader);
            return new Told_from_message(sender_username, message);
        }

        Protocol_message_type_id Protocol_message.Message_type_id => Message_type;
        Protocol_message Protocol_message.Read(Byte_reader reader) => Read(reader);
        void Protocol_message.Write(Byte_writer writer) => Write(writer);
    }
}
