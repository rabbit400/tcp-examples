﻿using System;

namespace Protocol
{
    public interface Byte_writer
    {
        public void Write_byte(byte b);
    }
    public interface Byte_reader
    {
        public byte Read_byte();
    }
    public class Protocol_message_type_id : IEquatable<Protocol_message_type_id>
    {
        public enum Types_enum : byte
        {
            Set_username = 1,
            Whisper_to,
            Tell_all,
            Whispered_from,
            Told_from,
            New_username,
        }

        public readonly Types_enum Type_id;
        public Protocol_message_type_id(Types_enum type_id)
            => Type_id = type_id;

        public bool Equals(Protocol_message_type_id other)
            => other != null && Type_id == other.Type_id;
        public override bool Equals(object obj) => Equals(obj as Protocol_message_type_id);
        public override int GetHashCode() => 876 + Type_id.GetHashCode();
        public override string ToString() => "Type id: " + Type_id;

        public void Write(Byte_writer w)
            => w.Write_byte((byte)Type_id);
        public static Protocol_message_type_id Read(Byte_reader r)
            => new Protocol_message_type_id((Types_enum)r.Read_byte());
    }
    public interface Protocol_deserialization_map
    {
        public Func<Byte_reader, Protocol_message> Get_reader(Protocol_message_type_id message_type);
    }
    public interface Protocol_writer
    {
        public Byte_writer Byte_writer { get; }
        public void Write_message(Protocol_message message);
    }
    public interface Protocol_reader
    {
        public Byte_reader Byte_reader { get; }
        public Protocol_message Read_message();
    }
    /// <summary>
    /// Non-abstract classes implementing this interface:
    ///  - Must have an empty constructor.
    ///  - Must not be generic.
    /// This is enforced by the reflection code when building a mapping
    /// from type ids to deserializers. Both these are aquired by creating
    /// an object with the empty constructor and calling its properties.
    /// </summary>
    /// Yes, we will write a different more complex version of these
    /// interfaces where the empty constructor is not required.
    /// In the Chat_server_using_reflection project.
    public interface Protocol_message
    {
        public Protocol_message_type_id Message_type_id { get; }
        public void Write(Byte_writer writer);
        public Protocol_message Read(Byte_reader reader);
    }
}
