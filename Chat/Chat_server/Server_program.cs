﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// This server example introduces:
//   - Buffered serialization for better performance.
//       (I have not meassured, only assuming...)
//   - Serialization strategy:
//       - Serializable types - here for networking but could be for file storage or other.
//           - enforced using an interface with Write and Read methods, this is not great
//               because the read functionality doesnt actually make sense as an instance
//               method - an instance doesnt really exist till after its read from the data stream.
//           - we also (sneakily) require serializable types to have an empty constructor...
//               this is really ugly but I do it cause I like the neat extension methods.
//               Can you still love me? ♥○♥
//               Absolutely I plan to adress this in future examples, first with a mapping from
//               message-type to deserialization-function, later with reflection!
//   - Threading model with a central state thread and other threads pushing events there.
namespace Chat_server
{
    // This chat server has a global state in:
    //  - mapping client to username
    //  - mapping username to cliet
    //  - keeping a NetworkStream (and byte_writer) reference per client
    // All these are stored together in Current_users.
    //
    // Threading solution:
    //      A single thread handles "events", modifies the global state and send any
    //      writes/replies to clients.
    //      Many other threads exist - they do nothing to global state, only does
    //          obviously-single-threaded work and pushes events onto the event queues.
    // There are four queues for events:
    //      Incomming_connections, Incomming_messages, Incomming_read_errors
    //      and Incomming_new_connection_errors.
    // There are four thread types:
    //  - main thread 
    //       - does startup
    //       - starts new-connection-listener thread
    //       - then becomes event handler thread.
    //  - one new connection listener thread
    //       - listens for new connections and put them onto Incomming_connections
    //       - puts any exception on Incomming_new_connection_errors
    //       - ends after any exception
    //  - many client reader threads
    //      -  one per client
    //      - listens for new data and parses it into messages
    //      - puts the messages onto Incomming_messages
    //      - puts any exceptions onto Incomming_read_errors 
    //      - ends after any exception (event handler thread does clean-up)
    //  - one event handler thread (i.e. main thread after startup)
    //      - handles any new events on the three event queues
    //          - New connection - create new client object and reader thread
    //          - New message, do one of:
    //              - new_username: set new username and notify all clients
    //              - tell_all: send the text to all clients
    //              - whisper_to: send whisper text to targeted clients
    //          - New reader error - remove that client data and close clients stream
    //
    // Note how we handle all non-simple logic in the event loop. 
    // By making any global-state modifications in a single centralized thread 
    // we make thread-safety very simple.
    // Look for collecting all (non-trivially single-threaded) logic into 
    // a single thread when you can.
    // Its cute ♥
    class Server_program
    {
        static readonly int default_port = 42069;
        // The Current_user hash-set is program state.
        // Only accessed from the event handler thread for:
        //   - thread safety
        //   - event ordering (not important here but a good point to learn)
        static readonly HashSet<Connected_user> Current_users = new HashSet<Connected_user>();
        
        // New connections from the new-connection-listener thread.
        static readonly ConcurrentQueue<TcpClient> Incomming_connections = new ConcurrentQueue<TcpClient>();
        
        // Messages from a client reader thread.
        static readonly ConcurrentQueue<(Connected_user user, Message message)> Incomming_messages = new ConcurrentQueue<(Connected_user, Message)>();
        
        // Exceptions from a client reader thread.
        // This always means the reader thread is ended.
        // I.e. event handler thread should remove the Connected_user and do clean up.
        static readonly ConcurrentQueue<(Connected_user user, Exception exception)> Incomming_read_errors = new ConcurrentQueue<(Connected_user, Exception)>();

        // Exceptions from the new-connection listener thread.
        // This always mean the listener thread is ended and program should end.
        static readonly ConcurrentQueue<Exception> Incomming_new_connection_errors = new ConcurrentQueue<Exception>();
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Server!");
            int port = default_port;
         
            TcpListener new_connection_listener = new TcpListener(IPAddress.Any, port);
            try
            {
                new_connection_listener.Start();
            }
            catch (SocketException se)
            {
                Console.WriteLine("Failed opening listen socket: " + se);
                return;
            }
            
            // start a thread listening for incomming connections
            new Thread(() => Run_listen_for_new_connections(new_connection_listener)).Start();

            // handle events - i.e. main thread becomes event thread
            Run_handle_incomming_events();
        }
        public static void Run_listen_for_new_connections(TcpListener listener)
        {
            try
            {
                while (true)
                {
                    // can throw socket exception
                    TcpClient new_connection = listener.AcceptTcpClient();
                    Incomming_connections.Enqueue(new_connection);
                }
            }
            catch (SocketException se)
            {
                Incomming_new_connection_errors.Enqueue(se);
            }
        }
        public static void Run_reader_thread(Connected_user user, Byte_reader reader)
        {
            try
            {
                while (true)
                {
                    var next_message = Message.Read(reader);
                    Incomming_messages.Enqueue((user, next_message));
                }
            }
            catch (Exception exception)
            {
                Incomming_read_errors.Enqueue((user, exception));
            }
        }
        public static void Run_handle_incomming_events()
        {
            while (true)
            {
                // if there is a new connection
                // - create client data object
                // - start reader thread
                if (Incomming_connections.TryDequeue(out var new_connection))
                {
                    // create per user object
                    NetworkStream stream = new_connection.GetStream();
                    Connected_user user = new Connected_user(stream);

                    // START THREAD reading data from the new user
                    Byte_reader reader = new Buffered_network_byte_reader(stream);
                    new Thread(() => Run_reader_thread(user, reader)).Start();

                    // store the new user object
                    Current_users.Add(user);
                }
                // if a reader thread failed - time to close the connection
                else if (Incomming_read_errors.TryDequeue(out var err))
                {
                    var (user, exception) = err;
                    // remove the user object and close the stream (in case its not dead already)
                    Current_users.Remove(user);
                    err.user.Dispose();
                    Console.WriteLine("Disconnected user, cause: " + exception);
                }
                // if the listener thread failed - end program
                else if (Incomming_new_connection_errors.TryDequeue(out var listen_error))
                {
                    Console.WriteLine("Error listening for new connections: " + listen_error);
                    // return here ends the server program
                    return;
                }
                // if there is a new message from a client
                else if (Incomming_messages.TryDequeue(out var msg))
                {
                    // Note that on any error here we close (dispose) the corresponding connection.
                    // This will show up as an exception in the reader thread and then we read it
                    // from the Incomming_errors queue and there we handle proper cleanup.
                    var (user, command) = msg;
                    switch (command.Id)
                    {
                        case Message_type.Set_username:
                            {
                                // update our data, then send new_username to all clients
                                user.Name = ((Set_username_message_body)command.Body).New_username;
                                var body_to_send = new New_username_message_body(user.Name);
                                var message_to_send = new Message(body_to_send);
                                foreach (var target in Current_users)
                                    try { target.Send(message_to_send); }
                                    catch { target.Dispose(); }
                            }
                            break;
                        case Message_type.Whisper_to:
                            {
                                // send the whisper-text to all clients with the target username
                                var whisper_body = (Whisper_to_message_body)command.Body;
                                var body_to_send = new Whispered_from_message_body(user.Name, whisper_body.Text);
                                var message_to_send = new Message(body_to_send);
                                var targets = Current_users
                                    .Where(u => u.Name == whisper_body.Target_name);
                                foreach (var target in targets)
                                    try { target.Send(message_to_send); }
                                    catch { target.Dispose(); }
                            }
                            break;
                        case Message_type.Tell_all:
                            // send the text to all clients
                            var tell_body = (Tell_all_message_body)command.Body;
                            var body_to_send_ = new Told_from_message_body(user.Name, tell_body.Text);
                            var message_to_send_ = new Message(body_to_send_);
                            foreach (var target in Current_users)
                                try { target.Send(message_to_send_); }
                                catch { target.Dispose(); }
                            break;
                        case Message_type.Whispered_from:
                        case Message_type.Told_from:
                        case Message_type.New_username:
                            // this is an error, clients are not allowed to send these commands
                            // dispose closes stream
                            // -> reader thread gets error
                            // -> we get reader error event in Incomming_read_errors
                            // -> we handle that error and do clean-up
                            user.Dispose();
                            break;
                    }
                }
            }
        }
    }

    public interface Byte_writer
    {
        public void Write_byte(byte b);
    }
    public interface Byte_reader
    {
        public byte Read_byte();
    }
    public enum Message_type : byte
    {
        Set_username = 1,
        Whisper_to,
        Tell_all,
        Whispered_from,
        Told_from,
        New_username,
    }
    public interface Message_body
    {
        public void Write(Byte_writer w);
    }
    public class Message
    {
        public readonly Message_type Id = (Message_type)0;
        public readonly Message_body Body = null;
        public Message(Message_body body)
        {
            if (body == null) throw new ArgumentNullException();
            else if (body is Set_username_message_body) Id = Message_type.Set_username;
            else if (body is Whisper_to_message_body) Id = Message_type.Whisper_to;
            else if (body is Tell_all_message_body) Id = Message_type.Tell_all;
            else if (body is Whispered_from_message_body) Id = Message_type.Whispered_from;
            else if (body is Told_from_message_body) Id = Message_type.Told_from;
            else if (body is New_username_message_body) Id = Message_type.New_username;
            else throw new ArgumentException();
            Body = body;
        }

        public static Message Read(Byte_reader r)
        {
            Message_type id = (Message_type)r.Read_byte();
            Message_body body;
            if (id == Message_type.Set_username) body = Set_username_message_body.Read(r);
            else if (id == Message_type.Whisper_to) body = Whisper_to_message_body.Read(r);
            else if (id == Message_type.Tell_all) body = Tell_all_message_body.Read(r);
            else if (id == Message_type.Whispered_from) body = Whispered_from_message_body.Read(r);
            else if (id == Message_type.Told_from) body = Told_from_message_body.Read(r);
            else if (id == Message_type.New_username) body = New_username_message_body.Read(r);
            else throw new System.IO.IOException();
            Message ret = new Message(body);
            if (ret.Id != id) throw new InvalidProgramException("I made a bug");
            return ret;
        }

        public void Write(Byte_writer w)
        {
            w.Write_byte((byte)Id);
            Body.Write(w);
        }
    }
    #region Message bodies
    public class Set_username_message_body : Message_body
    {
        public readonly Utf8_string New_username;
        public Set_username_message_body(Utf8_string name) => New_username = name;
        public static Set_username_message_body Read(Byte_reader r) 
            => new Set_username_message_body(Utf8_string.Read(r));
        public void Write(Byte_writer w)
            => New_username.Write(w);
    }
    public class Whisper_to_message_body : Message_body
    {
        public readonly Utf8_string Target_name;
        public readonly Utf8_string Text;
        public Whisper_to_message_body(Utf8_string target_name, Utf8_string text)
        {
            Target_name = target_name;
            Text = text;
        }
        public static Whisper_to_message_body Read(Byte_reader r)
        {
            Utf8_string target_name = Utf8_string.Read(r);
            Utf8_string text = Utf8_string.Read(r);
            return new Whisper_to_message_body(target_name, text);
        }
        public void Write(Byte_writer w)
        {
            Target_name.Write(w);
            Text.Write(w);
        }
    }
    public class Tell_all_message_body : Message_body
    {
        public readonly Utf8_string Text;
        public Tell_all_message_body(Utf8_string text) => Text = text;
        public static Tell_all_message_body Read(Byte_reader r)
            => new Tell_all_message_body(Utf8_string.Read(r));
        public void Write(Byte_writer w)
            => Text.Write(w);
    }
    public class Whispered_from_message_body : Message_body
    {
        public readonly Utf8_string Sender_name;
        public readonly Utf8_string Text;
        public Whispered_from_message_body(Utf8_string sender_name, Utf8_string text)
        {
            Sender_name = sender_name;
            Text = text;
        }
        public static Whispered_from_message_body Read(Byte_reader r)
        {
            Utf8_string sender_name = Utf8_string.Read(r);
            Utf8_string text = Utf8_string.Read(r);
            return new Whispered_from_message_body(sender_name, text);
        }
        public void Write(Byte_writer w)
        {
            Sender_name.Write(w);
            Text.Write(w);
        }
    }
    public class Told_from_message_body : Message_body
    {
        public readonly Utf8_string Sender_name;
        public readonly Utf8_string Text;
        public Told_from_message_body(Utf8_string sender_name, Utf8_string text)
        {
            Sender_name = sender_name;
            Text = text;
        }
        public static Told_from_message_body Read(Byte_reader r)
        {
            Utf8_string sender_name = Utf8_string.Read(r);
            Utf8_string text = Utf8_string.Read(r);
            return new Told_from_message_body(sender_name, text);
        }
        public void Write(Byte_writer w)
        {
            Sender_name.Write(w);
            Text.Write(w);
        }
    }
    public class New_username_message_body : Message_body
    {
        public readonly Utf8_string Name;
        public New_username_message_body(Utf8_string name) => Name = name;
        public static New_username_message_body Read(Byte_reader r)
            => new New_username_message_body(Utf8_string.Read(r));
        public void Write(Byte_writer w)
            => Name.Write(w);
    }
    #endregion
    /// <summary>
    /// Use Read_byte() to get data. Blocks and wait for data if needed.
    /// Throws IOException or ObjectDisposedException if the underlying is closed.
    /// </summary>
    /// Note that after the underlying is closed Read_byte may still deliver 
    /// some data before it throws - e.g. buffered data.
    public interface Stream_byte_reader : Byte_reader
    { // This interface isnt needed. It is here for documentation and for symmetry.
    }
    /// <summary>
    /// Use WriteByte to write one or more bytes.
    /// Then call Flush to ensure they are sent.
    /// Repeat until an exception is thrown.
    /// When underlying is closed one or both of WriteByte(..) and Flush()
    /// will throw IOException or ObjectDisposedException.
    /// </summary>
    /// By not specifying which of the methods will throw we leave freedom to implementations.
    /// E.g. implementations can use various buffering tactics.
    public interface Stream_byte_writer : Byte_writer
    {
        /// <summary>
        /// Call flush to push any buffered data into underlying.
        /// </summary>
        public void Flush();
    }
    // Used to implement a automatically growing byte buffer.
    // This is essentially a Byte_writer that writes to memory (not a stream).
    // We dont implement Byte_writer only because current names are more descriptive 
    // when we use it as a buffer.
    public class Growing_byte_buffer
    {
        private byte[] m_Buffer;
        private int m_Num_bytes;
        public Growing_byte_buffer(int initial_capacity = 16) => m_Buffer = new byte[initial_capacity];
        public void Clear() => m_Num_bytes = 0;
        public ReadOnlySpan<byte> Current_contents => new ReadOnlySpan<byte>(m_Buffer, 0, m_Num_bytes);
        public void Append_byte(byte b)
        {
            if (m_Num_bytes >= m_Buffer.Length) Grow_buffer();
            m_Buffer[m_Num_bytes++] = b;
        }
        private void Grow_buffer()
        {
            byte[] new_buffer = new byte[2 * m_Buffer.Length];
            Array.Copy(m_Buffer, new_buffer, m_Num_bytes);
            m_Buffer = new_buffer;
        }
    }
    public class Buffered_network_byte_writer : Stream_byte_writer
    {
        private readonly NetworkStream m_Stream;
        private readonly Growing_byte_buffer m_Buffer = new Growing_byte_buffer();
        public Buffered_network_byte_writer(NetworkStream stream) => m_Stream = stream;
        public void Write_byte(byte b) => m_Buffer.Append_byte(b);
        public void Flush()
        {
            m_Stream.Write(m_Buffer.Current_contents);
            m_Buffer.Clear();
        }
    }
    public class Buffered_network_byte_reader : Stream_byte_reader
    {
        private readonly NetworkStream m_Stream;
        private readonly byte[] m_Buffer;
        private int m_Num_bytes_in_buffer = 0;
        private int m_Num_bytes_read = 0;
        public Buffered_network_byte_reader(NetworkStream stream, int buffer_size = 1024)
        {
            m_Stream = stream;
            m_Buffer = new byte[buffer_size];
        }

        public byte Read_byte()
        {
            if (m_Num_bytes_read >= m_Num_bytes_in_buffer)
            { // if we delivered all data in buffer its time to fetch more
                m_Num_bytes_in_buffer = m_Stream.Read(m_Buffer); // can throw - like it should!
                m_Num_bytes_read = 0;
            }
            return (byte)m_Buffer[m_Num_bytes_read++];
        }
    }
    public class Connected_user : IDisposable
    {
        private readonly NetworkStream NetStream;
        private readonly Stream_byte_writer Writer;
        public Utf8_string Name { get; set; }
        public void Send(Message msg)
        {
            msg.Write(Writer);
            Writer.Flush();
        }
        public Connected_user(NetworkStream stream)
        {
            NetStream = stream;
            Writer = new Buffered_network_byte_writer(stream);
        }
        public void Dispose() => NetStream.Dispose();
    }
    public class Utf8_string : IEnumerable<byte>, IEquatable<Utf8_string>
    {
        private readonly byte[] m_Bytes;
        public int Length => m_Bytes.Length;
        public IEnumerator<byte> GetEnumerator() { foreach (var b in m_Bytes) yield return b; }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        private Utf8_string(byte[] bytes) => m_Bytes = bytes;
        public Utf8_string(string s) => m_Bytes = UTF8Encoding.UTF8.GetBytes(s);
        public override string ToString() => UTF8Encoding.UTF8.GetString(m_Bytes);
        public bool Equals(Utf8_string other)
        {
            if (other != null || m_Bytes.Length != other.m_Bytes.Length) return false;
            for (int i = 0; i < m_Bytes.Length; i++) // should use some standard lib function
                if (m_Bytes[i] != other.m_Bytes[i]) // but I always forget the name
                    return false;
            return true;
        }
        public override bool Equals(object obj) => Equals(obj as Utf8_string);
        public override int GetHashCode()
        {
            int h = 934768;
            foreach (var b in m_Bytes) h = h * 17 + b;
            return h;
        }
        public void Write(Byte_writer w) => w.Write_byte_array(m_Bytes);
        public static Utf8_string Read(Byte_reader r) => new Utf8_string(r.Read_byte_array());
    }
    public static class Basic_types_serialization
    {
        public static void Write_uint32(this Byte_writer w, UInt32 u)
        {
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
        }
        public static UInt32 Read_uint32(this Byte_reader r)
        {
            UInt32 ret = 0;
            ret += r.Read_byte();
            ret += r.Read_byte() * 256u;
            ret += r.Read_byte() * 256u * 256u;
            ret += r.Read_byte() * 256u * 256u * 256u;
            return ret;
        }
        public static void Write_byte_array(this Byte_writer w, byte[] array)
        {
            w.Write_uint32((UInt32)array.Length);
            foreach (var b in array) w.Write_byte(b);
        }
        public static byte[] Read_byte_array(this Byte_reader r)
        {
            UInt32 n = r.Read_uint32();
            byte[] array = new byte[n];
            for (int i = 0; i < n; i++) array[i] = r.Read_byte();
            return array;
        }
    }
}
