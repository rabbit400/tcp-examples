﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Chat_client
{
    // First read the example Echo client comments for how the read loop
    // and write loop works. 
    //
    // This chat client example introduces 
    //    - a simple byte-stream protocol
    //    - a strategy for serializing types byte by byte
    //
    // The protocol is rules for how to send data on a byte-stream.
    // This describes what byte by byte data is sent over the
    // connection, NOT how the code or classes are written.
    // Though, of course, the code and classes are normally 
    // quite similar to the procol names.
    //
    // IMPORTANT for most projects you do not need to write this long protocol
    //           definition. Usually youre fine with writing serialization code
    //           and handwave: "the protocol is what the code does".
    //
    // Our protocol consists 6 different message type and differs between
    // client and server side of the connection.
    // Three message types are sent by clients only and three by the server only:
    //   1: Set_username   - client message to change username
    //   2: Whisper_to     - client message to send a whisper to an other user
    //   3: Tell_all       - client message to text everyone on the server
    //   4: Whispered_from - server message to send a whisper
    //   5: Told_from      - server message to send a tell_all to a client
    //   6: New_username   - server message to notify of a new username on the server
    //
    // Now to send a message:
    // Message - everything we send (and receive) is a message
    //        1) 1 byte - T, what the message type is
    //                - value is 1,2,3,4,5 or 6 for the message types listed below
    //        2) bytes - a message body of type T
    //
    // Message bodies:
    // 1: Set_username - client message to change username
    //        body:
    //        1) utf8_string - the new user name
    //
    // 2: Whisper_to - client message to send a whisper to an other user
    //        body:
    //        1) utf8_string - the target username
    //        2) utf8_string - the text
    //
    // 3: Tell_all - client message to text everyone on the server
    //         body:
    //         1) utf8_string - the text
    // 
    // 4: Whispered_from - server message to send a whisper
    //         body:
    //         1) utf8_string - the sender username
    //         2) utf8_string - the text
    //
    // 5: Told_from - server message to send a tell_all to a client
    //         body:
    //         1) utf8_string - the sender username
    //         2) utf8_string - the text
    //
    // 6: New_username - server message to notify of a new username on the server
    //        body:
    //        1) utf8_string - the new user name
    //
    // To send these we also need to specify how to send a utf8_string.
    // (And a byte array and a UInt32.)
    // utf8_string - a utf8 encoded string
    //        body:
    //        1) byte array - the utf8_data
    //
    // byte array - N bytes of data
    //        body:
    //        1) UInt32 - N, length of byte array with the data
    //        2) N bytes - the contents of the array
    //
    // UInt32 - U, a 4 byte unsigned int (byte order is choosen to make the code look good)
    //        body:
    //        1) byte - U % 256 - the least significant byte of U - think U & 0xFF
    //        2) byte - (U/256) % 256 - think (U >> 8) & 0xFF
    //        3) byte - (U/256/256) % 256 - think (U >> 16) & 0xFF
    //        4) byte - (U/256/256/256) % 256 - the most significant byte of U  - think (U >> 24) & 0xFF
    //
    class Client_Program
    {
        static readonly string default_host = "localhost";
        static readonly int default_port = 42069;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Client!");

            string host = default_host;
            int port = default_port;

            TcpClient client = new TcpClient();
            try
            {
                client.Connect(host, port);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Failed connecting: " + ex.ToString());
                return;
            }

            Console.WriteLine("Whats your username?");
            string our_username = Console.ReadLine();

            using (NetworkStream stream = client.GetStream())
            {
                var writer = new Naive_network_byte_writer(stream);
                var reader = new Naive_network_byte_reader(stream);

                // start reader thread
                new Thread(() => Run_reader(reader)).Start();

                // Send a "set our name" message
                writer.Write_byte((byte)Message_type.Set_username);
                writer.Write_string_as_utf8(our_username);
                writer.Flush();

                try
                {
                    while (true)
                    {
                        string line = Console.ReadLine();
                        // send "tell all" message
                        writer.Write_byte((byte)Message_type.Tell_all);
                        writer.Write_string_as_utf8(line);
                        writer.Flush();
                    }
                }
                catch (Exception e) when (e is IOException || e is ObjectDisposedException)
                {
                    Console.WriteLine("Connection closed: " + e);
                }
            }
        }
        static void Run_reader(Byte_reader r)
        {
            try
            {
                while (true)
                {
                    // read what type of message the server sent
                    var message_type = (Message_type)r.Read_Byte();
                    // the type tells us how to read the message body
                    switch (message_type)
                    {
                        case Message_type.Set_username:   // these client commands should never
                        case Message_type.Whisper_to: // be sent from the server
                        case Message_type.Tell_all:   // its an ERROR if we receive them
                            throw new IOException("Protocol Error: Received client command from server.");
                        case Message_type.Whispered_from:
                            // whispered_from message body is 2 strings: name and text
                            string whisperers_name = r.Read_string_from_utf8();
                            string whisper_text = r.Read_string_from_utf8();
                            Console.WriteLine(whisperers_name + " (whispers): " + whisper_text);
                            break;
                        case Message_type.Told_from:
                            // told_from message body is a single string
                            string tellers_username = r.Read_string_from_utf8();
                            string told_text = r.Read_string_from_utf8();
                            Console.WriteLine(tellers_username +  ": " + told_text);
                            break;
                        case Message_type.New_username:
                            // new_username message body is a single string
                            string new_name = r.Read_string_from_utf8();
                            Console.WriteLine("New username: " + new_name);
                            break;
                        default:
                            throw new IOException("Protocol Error: Received unrecognized message type.");
                    }
                }
            }
            catch (Exception e) when (e is IOException || e is ObjectDisposedException)
            {
                Console.WriteLine("Read stream closed: " + e);
            }
        }
    }
    public enum Message_type : byte
    {
        Set_username = 1,
        Whisper_to,
        Tell_all,
        Whispered_from,
        Told_from,
        New_username,
    }
    /// <summary>
    /// Usage: write bytes one at a time till an exception is thrown.
    /// </summary>
    public interface Byte_writer
    {
        public void Write_byte(byte b);
    }
    /// <summary>
    /// Usage: read bytes one at a time till an exception is thrown.
    /// </summary>
    public interface Byte_reader
    {
        public byte Read_Byte();
    }
    public static class Protocol_basic_types
    {
        public static void Write_uint32(this Byte_writer w, UInt32 u)
        {
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
            u /= 256;
            w.Write_byte((byte)(u % 256));
        }
        public static UInt32 Read_uint32(this Byte_reader r)
        {
            UInt32 ret = 0;
            ret += r.Read_Byte();
            ret += r.Read_Byte() * 256u;
            ret += r.Read_Byte() * 256u * 256u;
            ret += r.Read_Byte() * 256u * 256u * 256u;
            return ret;
        }
        public static void Write_string_as_utf8(this Byte_writer w, string s)
        {
            byte[] utf8_bytes = System.Text.UTF8Encoding.UTF8.GetBytes(s);
            w.Write_uint32((UInt32)utf8_bytes.Length);
            foreach (var b in utf8_bytes)
                w.Write_byte(b);
        }
        public static string Read_string_from_utf8(this Byte_reader r)
        {
            UInt32 n = r.Read_uint32();
            byte[] utf8_bytes = new byte[n];
            for (int i = 0; i < n; i++)
                utf8_bytes[i] = r.Read_Byte();
            return System.Text.UTF8Encoding.UTF8.GetString(utf8_bytes);
        }
    }
    // Yes these naive classes have horrible performance.
    // Im using these here anyways.
    // This is why you love me ♥
    // Look to the chat server example for buffered versions.
    public class Naive_network_byte_writer : Byte_writer
    {
        private readonly NetworkStream m_Stream;
        public Naive_network_byte_writer(NetworkStream stream) => m_Stream = stream;
        public void Write_byte(byte b) => m_Stream.WriteByte(b);
        public void Flush() { } //noop
    }
    public class Naive_network_byte_reader : Byte_reader
    {
        private readonly NetworkStream m_Stream;
        public Naive_network_byte_reader(NetworkStream stream) => m_Stream = stream;
        public byte Read_Byte()
        {
            int i = m_Stream.ReadByte(); // can throw which is fine per interface definition
            if (i < 0) throw new EndOfStreamException();
            return (byte)i;
        }
    }
}
