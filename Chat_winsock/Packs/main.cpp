#include "Packs.h"
#include <tuple>
#include <variant>
#include <iostream>

using namespace Packs;

void Test_count()
{
	static_assert(3 == Pack<int, char, std::tuple<>>::Count);
	static_assert(0 == Pack<>::Count);
}
void Test_first()
{
	static_assert(std::is_same_v<short, Pack<short, int, char>::First>);
}
void Test_last()
{
	static_assert(std::is_same_v<char, Pack<short, int, char>::Last>);
}
void Test_remove_first()
{
	static_assert(std::is_same_v<Pack<int, char>, Pack<short, int, char>::Remove_first>);
	static_assert(std::is_same_v<Pack<char>, Pack<short, int, char>::Remove_first::Remove_first>);
	static_assert(std::is_same_v<Pack<>, Pack<short, char>::Remove_first::Remove_first>);
}
void Test_remove_last()
{
	static_assert(std::is_same_v<Pack<short, int>, Pack<short, int, char>::Remove_last>);
	static_assert(std::is_same_v<Pack<short>, Pack<short, int, char>::Remove_last::Remove_last>);
	static_assert(std::is_same_v<Pack<>, Pack<short, char>::Remove_last::Remove_last>);
}
void Test_reverse()
{
	static_assert(std::is_same_v<Pack<>, Pack<>::Reverse>);
	static_assert(std::is_same_v<Pack<int>, Pack<int>::Reverse>);
	static_assert(std::is_same_v<Pack<int, char>, Pack<char, int>::Reverse>);
	static_assert(std::is_same_v<Pack<int, short, char, float>, Pack<float, char, short, int>::Reverse>);
}
void Test_apply()
{
	using sic_t = Pack<short, int, char>::Apply<std::tuple>;
	static_assert(std::is_same_v<std::tuple<short, int, char>, sic_t>);
	sic_t x = std::make_tuple((short)1, 13, 'x');
	static_assert(std::is_same_v<std::variant<>, Pack<>::Apply<std::variant>>);
}
void Test_prepend()
{
	static_assert(std::is_same_v<Pack<int, char, short>, Pack<short>::Prepend<int, char>>);
	static_assert(std::is_same_v<Pack<int>, Pack<>::Prepend<int>>);
}
void Test_append()
{
	static_assert(std::is_same_v<Pack<short, int, char>, Pack<short>::Append<int, char>>);
	static_assert(std::is_same_v<Pack<int>, Pack<>::Append<int>>);
}
void Test_concat()
{
	static_assert(std::is_same_v<Pack<>, Pack<>::Concat<Pack<>>>);
	static_assert(std::is_same_v<Pack<char>, Pack<>::Concat<Pack<char>>>);
	static_assert(std::is_same_v<Pack<int>, Pack<int>::Concat<Pack<>>>);
	static_assert(std::is_same_v<Pack<char, short, int>, Pack<char>::Concat<Pack<short, int>>>);
}
void Test_take()
{
	static_assert(std::is_same_v<Pack<>, Pack<int, char>::Take<0>>);
	static_assert(std::is_same_v<Pack<>, Pack<>::Take<3>>);
	static_assert(std::is_same_v<Pack<int>, Pack<int>::Take<3>>);
	static_assert(std::is_same_v<Pack<int, char, short>, Pack<int, char, short>::Take<3>>);
	static_assert(std::is_same_v<Pack<int, char, short>, Pack<int, char, short, double>::Take<3>>);
	static_assert(std::is_same_v<Pack<>, Pack<>::Take<3>>);
}
void Test_skip()
{
	static_assert(std::is_same_v<Pack<>, Pack<int, char>::Skip<2>>);
	static_assert(std::is_same_v<Pack<>, Pack<>::Skip<3>>);
	static_assert(std::is_same_v<Pack<int>, Pack<int>::Skip<0>>);
	static_assert(std::is_same_v<Pack<char, short>, Pack<int, char, short>::Skip<1>>);
	static_assert(std::is_same_v<Pack<short, double>, Pack<int, char, short, double>::Skip<2>>);
	static_assert(std::is_same_v<Pack<>, Pack<int, short>::Skip<3>>);
}
void Test_subpack()
{
	static_assert(std::is_same_v<Pack<>, Pack<>::Subpack<0, 0>>);
	static_assert(std::is_same_v<Pack<>, Pack<int, char>::Subpack<1, 0>>);
	static_assert(std::is_same_v<Pack<char>, Pack<int, char>::Subpack<1, 1>>);
	static_assert(std::is_same_v<Pack<short>, Pack<short, int, char>::Subpack<0, 1>>);
	static_assert(std::is_same_v<Pack<int, char>, Pack<short, int, char, double>::Subpack<1, 2>>);
}
template <class T> using has_odd_size = std::bool_constant<sizeof(T) % 2 == 1>;
template <class T> using has_even_size = std::negation<std::bool_constant<sizeof(T) % 2 == 1>>;
void Test_filter()
{
	static_assert(std::is_same_v<Pack<>, Pack<>::Filter<has_odd_size>>);
	static_assert(std::is_same_v<Pack<char>, Pack<int, char, double>::Filter<has_odd_size>>);
	static_assert(std::is_same_v<Pack<>, Pack<int, double, short>::Filter<has_odd_size>>);
	static_assert(std::is_same_v<Pack<char[3], signed char>,
					Pack<int, char[3], double, short, signed char>::Filter<has_odd_size>>);
	static_assert(std::is_same_v<Pack<int>,Pack<int, char, char[3]>::Filter<has_even_size>>);
}
template <class T> struct transform_short_to_int : std::type_identity<T> {};
template <> struct transform_short_to_int<short> : std::type_identity<int> {};
void Test_transform()
{
	static_assert(std::is_same_v<Pack<>, Pack<>::Transform<transform_short_to_int>>);
	static_assert(std::is_same_v<Pack<int>, Pack<short>::Transform<transform_short_to_int>>);
	static_assert(std::is_same_v<Pack<char, int, int, float>, Pack<char, int, short, float>::Transform<transform_short_to_int>>);
}
template <class T, class U> using size_less_than = std::bool_constant<(sizeof(T) < sizeof(U))> ;
template <class T, class U> using size_greater_than = std::bool_constant<(sizeof(T) > sizeof(U))> ;
void Test_sort_pack()
{
	static_assert(std::is_same_v<Pack<>, typename Pack<>::Sort<size_less_than>>);
	static_assert(std::is_same_v<Pack<char, signed char, int>,
						typename Pack<char, int, signed char>::Sort<size_less_than>>);
	static_assert(std::is_same_v<Pack<signed char, char, int>,
						typename Pack<int, signed char, char>::Sort<size_less_than>>);
	static_assert(std::is_same_v<Pack<int, short, signed char, char>,
		typename Pack<int, signed char, char, short>::Sort<size_greater_than>>);
	static_assert(std::is_same_v<Pack<double, int, float, short, signed char, char>,
		typename Pack<int, signed char, char, short, float, double>::Sort<size_greater_than>>);
}
void Test_contains()
{
	static_assert(!Pack<>::Contains<int>);
	static_assert(!Pack<float>::Contains<int>);
	static_assert(!Pack<float, double>::Contains<int>);
	static_assert(Pack<int>::Contains<int>);
	static_assert(Pack<float, double, int>::Contains<int>);
	static_assert(Pack<int, float, double>::Contains<int>);
	static_assert(Pack<int, float, double, int>::Contains<int>);
}

void Test_index_of()
{
	static_assert(Pack<int>::Index_of<int> == 0);
	static_assert(Pack<int, int>::Index_of<int> == 0);
	static_assert(Pack<int&, int>::Index_of<int> == 1);
	static_assert(Pack<const int, int>::Index_of<int> == 1);
	static_assert(Pack<int, float, double>::Index_of<int> == 0);
	static_assert(Pack<int, float, double>::Index_of<float> == 1);
	static_assert(Pack<int, float, double>::Index_of<double> == 2);

	// following lines shoud NOT compile
	//static_assert(Pack<int>::Index_of<double> >= 0);
	//static_assert(Pack<>::Index_of<double> >= 0);
}

void example();
int main()
{
	std::cout << "Hallo pack s world." << std::endl;

	Test_count();
	Test_first();
	Test_remove_first();
	Test_remove_last();
	Test_reverse();
	Test_apply();
	Test_prepend();
	Test_append();
	Test_concat();
	Test_take();
	Test_skip();
	Test_subpack();
	Test_filter();
	Test_transform();
	Test_sort_pack();
	Test_contains();
	Test_index_of();

	example();

	return 0;
}

// example 1: implementing transform explicitly
template<class T> struct float_to_double_transform { using type = T; };
template<> struct float_to_double_transform<float> { using type = double; };

// example 2: implementing transform by inheriting std::type_identity
template<class T> struct short_to_int_transform : std::type_identity<T> {};
template<> struct short_to_int_transform<short> : std::type_identity<int> {};

// example 3: "implementing" a filter with a using to an existing std type_trait
template<class T> using numbers_only_filter = std::is_arithmetic<T>;

// example 4: implementing a filter explicitly
template<class T> struct size_larger_than_one_filter
{
	static constexpr bool value = sizeof(T) > 1;
};

// example 5: implementing a Less_than comparison by size
template<class T1, class T2> struct compare_by_size
{ static constexpr bool value = sizeof(T1) < sizeof(T2); };

void example()
{
	std::tuple<float, int, short, char, float, char *, std::tuple<int>> x;

	Packs::Read_pack<decltype(x)>
		::Transform<float_to_double_transform>
		::Transform<short_to_int_transform>
		::Filter<numbers_only_filter>
		::Filter<size_larger_than_one_filter>
		::Sort<compare_by_size>
		::Apply<std::tuple>
		y;
	// and y is now std::tuple<int, int, double, double>
	static_assert(std::is_same_v<decltype(y), std::tuple<int, int, double, double>>);

	// same thing with more words
	using P1 = Packs::Read_pack<decltype(x)>;
	using P2 = P1::Transform<float_to_double_transform>;
	using P3 = P2::Transform<short_to_int_transform>;
	using P4 = P3::Filter<numbers_only_filter>;
	using P5 = P4::Filter<size_larger_than_one_filter>;
	using P6 = P5::Sort<compare_by_size>;
	using P7 = P6::Apply<std::tuple>;
	P7 y2;
	// and y2 is now std::tuple<int, int, double, double>
	static_assert(std::is_same_v<decltype(y2), std::tuple<int, int, double, double>>);

	std::cout << "example ran" << std::endl;
}

