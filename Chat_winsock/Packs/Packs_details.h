#pragma once

#include "Packs_concept.h"
#include <type_traits>

namespace Packs::details
{
#pragma region ....First_type
	template <class Head, class ... Tail>
	struct First_type_s : std::type_identity<Head> {};

	template <class ... Ts>
	using First_type_t = First_type_s<Ts...>::type;
#pragma endregion

#pragma region ....Last_type
	template <class Head, class ... Tail>
	struct Last_type_s : Last_type_s<Tail...> {};

	template <class Head>
	struct Last_type_s<Head> : std::type_identity<Head> {};

	template <class ... Ts>
	using Last_type_t = Last_type_s<Ts...>::type;
#pragma endregion

#pragma region ...Pack_contains
	template<Pack_concept P, class T>
	struct Pack_contains_s;

	template<class T> // empty pack
	struct Pack_contains_s<Packs::Pack<>, T> : std::false_type {};

	template<class T, class ... Tail> // pack head matches
	struct Pack_contains_s<Packs::Pack<T, Tail...>, T> : std::true_type {};

	template<class T, class Head, class ... Tail> // pack head not matching: recurse on tail
	struct Pack_contains_s<Packs::Pack<Head, Tail...>, T>
		: Pack_contains_s<Packs::Pack<Tail...>, T> {};

	template<Pack_concept P, class T>
	inline constexpr bool Pack_contains_v = Pack_contains_s<P, T>::value;
#pragma endregion

#pragma region ...Pack_index_of
	template<Pack_concept P, class T>
	struct Pack_index_of_s;

	template<class T, class ... Tail> // pack starts with T
	struct Pack_index_of_s<Pack<T, Tail...>, T>
		: std::integral_constant<size_t, 0> {};

	template<class T, class Head, class ... Tail> // NOT pack starts with T
	struct Pack_index_of_s<Pack<Head, Tail...>, T>
		: std::integral_constant<size_t,
		1 + Pack_index_of_s<Pack<Tail...>, T>::value>
	{};

	template<Pack_concept P, class T>
	inline constexpr size_t Pack_index_of_v = Pack_index_of_s<P, T>::value;
#pragma endregion

#pragma region ....Apply_pack_with_prefix
	template <template<class...> class Result, Pack_concept P, class ... Prefix>
	struct Apply_pack_with_prefix_s
		: Apply_pack_with_prefix_s<Result, typename P::Remove_first, Prefix..., typename P::First> {};

	template <template<class...> class Result, Pack_concept P, class ... Prefix>
	requires (P::Count == 0)
		struct Apply_pack_with_prefix_s<Result, P, Prefix...>
		: std::type_identity<Result<Prefix...>> {};

	template <template<class...> class Result, Pack_concept P, class ... Prefix>
		using Apply_pack_with_prefix_t = Apply_pack_with_prefix_s<Result, P, Prefix...>::type;
#pragma endregion

#pragma region ....Apply_pack
	template <template <class...> class Result, Pack_concept P>
	using Apply_pack_t = Apply_pack_with_prefix_t<Result, P>;
#pragma endregion

#pragma region ....Read_pack
	template <class>
	struct Read_pack_s;

	template <template<class...> class Variadic_template_type, class ... Ts>
	struct Read_pack_s < Variadic_template_type <Ts...>>
		: std::type_identity<Packs::Pack<Ts...>> {};

	template <class Variadic_type>
	using Read_pack_t = Read_pack_s<Variadic_type>::type;
#pragma endregion

#pragma region ....Remove_last_from_pack_with_prefix
	template <Pack_concept P, class ... Prefix>
	struct Remove_last_from_pack_with_prefix_s
		: Remove_last_from_pack_with_prefix_s<typename P::Remove_first, Prefix..., typename P::First> {};

	template <Pack_concept P, class ... Prefix> requires (P::Count == 1)
		struct Remove_last_from_pack_with_prefix_s<P, Prefix...>
		: std::type_identity<Packs::Pack<Prefix...>> {};

	template <Pack_concept _Pack, class ... Prefix>
	using Remove_last_from_pack_with_prefix_t
		= Remove_last_from_pack_with_prefix_s<_Pack, Prefix...>::type;
#pragma endregion

#pragma region ....Remove_last_from_pack
	template <Pack_concept P>
	using Remove_last_from_pack_t = Remove_last_from_pack_with_prefix_t<P>;
#pragma endregion

#pragma region ....Remove_first_from_pack
	template <Pack_concept P>
	requires (P::Count > 0)
		struct Remove_first_from_pack_s;

	template <class Head, class ... Tail>
	struct Remove_first_from_pack_s<Packs::Pack<Head, Tail...>>
		: std::type_identity<Packs::Pack<Tail...>> {};

	template <Pack_concept P>
	using Remove_first_from_pack_t = Remove_first_from_pack_s<P>::type;
#pragma endregion

#pragma region ....Prepend_to_pack
	template <Pack_concept P, class ...Prefix>
	using Prepend_to_pack_t = Apply_pack_with_prefix_t<Packs::Pack, P, Prefix...>;
#pragma endregion

#pragma region ....Append_to_pack
	template <Pack_concept P, class ...Postfix>
	struct Append_to_pack_s
		: Append_to_pack_s<typename P::Remove_last, typename P::Last, Postfix...> {};

	template <Pack_concept P, class ...Postfix>
	requires (P::Count == 0)
		struct Append_to_pack_s<P, Postfix...>
		: std::type_identity<Packs::Pack<Postfix...>> {};

	template <Pack_concept P, class... Postfix>
	using Append_to_pack_t = Append_to_pack_s<P, Postfix...>::type;
#pragma endregion

#pragma region ....Reverse_types
	template <class...Ts>
	struct Reverse_types_s : std::type_identity<Pack<Ts...>> {};

	template <class ... Ts>
	using Reverse_types_t = Reverse_types_s<Ts...>::type;

	template <class T, class U, class ... Tail>
	struct Reverse_types_s<T, U, Tail...>
		: Append_to_pack_s<Reverse_types_t<Tail...>, U, T> {};
#pragma endregion

#pragma region ....Reverse_pack
	template <Pack_concept P>
	using Reverse_pack_t = Apply_pack_t<Reverse_types_t, P>;
#pragma endregion

#pragma region ....Take_front_of_pack - can take less than n if pack-count is less
	template <Pack_concept P, size_t n>
	struct Take_front_of_pack_s
		: Take_front_of_pack_s<typename P::Remove_last, n> {};

	template <Pack_concept P, size_t n>
	requires (n >= P::Count)
		struct Take_front_of_pack_s<P, n>
		: std::type_identity<P> {};

	template <Pack_concept P, size_t n>
	using Take_front_of_pack_t = Take_front_of_pack_s<P, n>::type;
#pragma endregion

#pragma region ....Skip_front_of_pack - returns empty if the pack doesnt have n types
	template <Pack_concept P, size_t n>
	struct Skip_front_of_pack_s
		: Skip_front_of_pack_s<typename P::Remove_first, n - 1> {};

	template <Pack_concept P, size_t n>
	requires (P::Count == 0 || n == 0)
		struct Skip_front_of_pack_s<P, n>
		: std::type_identity<P> {};

	template <Pack_concept P, size_t n>
	using Skip_front_of_pack_t = Skip_front_of_pack_s<P, n>::type;
#pragma endregion

#pragma region ....Subpack
	template <Pack_concept P, size_t begin = 0, size_t count = P::Count - begin>
	requires (begin + count <= P::Count && begin <= P::Count && count <= P::Count)
		using Subpack_t = Take_front_of_pack_t<Skip_front_of_pack_t<P, begin>, count>;
#pragma endregion

#pragma region ....Concat_packs
	template <Pack_concept P, Pack_concept Q>
	struct Concat_packs_s;

	template <Pack_concept P, class ... Qs>
	struct Concat_packs_s<P, Packs::Pack<Qs...>>
		: Append_to_pack_s<P, Qs...> {};;

	template <Pack_concept P, Pack_concept Q>
	using Concat_packs_t = Concat_packs_s<P, Q>::type;
#pragma endregion

#pragma region ....Filter_pack_with_prefix
	template <template <class> class Filter, Pack_concept P, class ... Prefix>
	struct Filter_pack_with_prefix_s
		: Filter_pack_with_prefix_s<Filter, typename P::Remove_first, Prefix...> {};

	template <template <class> class Filter, Pack_concept P, class ... Prefix>
	requires (P::Count > 0 && Filter<typename P::First>::value)
		struct Filter_pack_with_prefix_s<Filter, P, Prefix...>
		: Filter_pack_with_prefix_s<Filter, typename P::Remove_first, Prefix..., typename P::First> {};

	template <template <class> class Filter, Pack_concept P, class ... Prefix>
	requires (P::Count == 0)
		struct Filter_pack_with_prefix_s<Filter, P, Prefix...>
		: std::type_identity<Packs::Pack<Prefix...>> {};

	template <template <class> class Filter, Pack_concept P, class ... Prefix>
	using Filter_pack_with_prefix_t = Filter_pack_with_prefix_s<Filter, P, Prefix...>::type;
#pragma endregion

#pragma region ....Filter_pack
	template <template <class> class Filter, Pack_concept P>
	using Filter_pack_t = Filter_pack_with_prefix_t<Filter, P>;
#pragma endregion

#pragma region ....Transform_pack_with_prefix
	template <template<class> class Transform, Pack_concept P, class ... Prefix>
	struct Transform_pack_with_prefix_s : Transform_pack_with_prefix_s<
			Transform,
			typename P::Remove_first,
			Prefix...,
			typename Transform<typename P::First>::type
	> {};

	template <template<class> class Transform, Pack_concept P, class ... Prefix>
		requires (P::Count == 0)
	struct Transform_pack_with_prefix_s<Transform, P, Prefix...>
		: std::type_identity<Pack<Prefix...>> {};

	template <template<class> class Transform, Pack_concept P, class ... Prefix>
	using Transform_pack_with_prefix_t = Transform_pack_with_prefix_s<Transform, P, Prefix...>::type;
#pragma endregion

#pragma region ....Transform_pack
	template <template<class> class Transform, Pack_concept P>
	using Transform_pack_t = Transform_pack_with_prefix_t<Transform, P>;
#pragma endregion

#pragma region ....Sort_pack
	template <template <class, class> class Less_than, Pack_concept P>
	struct Sort_pack_s;

	template <template <class, class> class Less_than, Pack_concept P>
	using Sort_pack_t = Sort_pack_s<Less_than, P>::type;

	template <template <class, class> class Less_than, Pack_concept P>
	struct Sort_pack_s
	{
	private:
		using Pivot = P::First;
		using Tail = P::Remove_first;
		template <class T> using Left_of_pivot = Less_than<T, Pivot>;
		template <class T> using Not_left_of_pivot = std::negation<Less_than<T, Pivot>>;

		using Left_pack = Filter_pack_t<Left_of_pivot, Tail>;
		using Right_pack = Filter_pack_t<Not_left_of_pivot, Tail>;

		using Left_sorted_pack = Sort_pack_t<Less_than, Left_pack>;
		using Right_sorted_pack = Sort_pack_t<Less_than, Right_pack>;
		using Left_pack_with_pivot = Append_to_pack_t<Left_sorted_pack, Pivot>;
	public:
		using type = Concat_packs_t<Left_pack_with_pivot, Right_sorted_pack>;
	};

	template <template <class, class> class Less_than, Pack_concept P>
	requires (P::Count <= 1)
		struct Sort_pack_s<Less_than, P>
		: std::type_identity<P> {};
#pragma endregion
}

