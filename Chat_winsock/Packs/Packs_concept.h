#pragma once

#include <type_traits>

namespace Packs
{
	template<class...Ts> struct Pack;

#pragma region ....Is_pack
	template <class T>
	struct Is_pack_s : std::false_type {};

	template <class ...Ts>
	struct Is_pack_s<Packs::Pack<Ts...>> : std::true_type {};

	template <class T>
	constexpr bool Is_pack_v = Is_pack_s<T>::value;
#pragma endregion

	template <class T>
	concept Pack_concept = Is_pack_v<T>;
}