#include "Tcp_stream.h"

#include "Address_lookup.h"
#include <functional>

using namespace Results;

Result<Success<Tcp_stream>, No_address_found, Wsa_error>
Tcp_stream::Connect(const std::string& hostname, const std::string& port_or_service)
{
	using Return_type = Result<Success<Tcp_stream>, No_address_found, Wsa_error>;

	Address_list addresses;
	{
		auto addresses_result = Address_lookup::for_connecting(hostname.c_str(), port_or_service.c_str());

		if (!addresses_result.Is_success())
			return addresses_result.Get_non_success();

		addresses = std::move(addresses_result.Get_success().Value);
	}


	std::optional<Return_type> first_connect_error;

	for (const auto& address : addresses)
	{
		auto create_result = Socket::Create_socket(address);

		// If we failed in _creating_ a socket we might as well give up. I.e. return the error.
		if (!create_result.Is_success())
			return create_result.Get_non_success();

		Socket socket = std::move(create_result.Get_success().Value);

		auto connect_result = socket.Connect(address);

		if (connect_result.Is_success())
			return Tcp_stream(std::move(socket));

		// We store the first error only, so on connect fail we return the error for the first address.
		if (!first_connect_error.has_value())
		{
			// We know the socket is not closed because we havent closed it.
			// We know it is not success because we checked it.
			// Sadly the type-system doesnt know.
			connect_result.Visit(
				[&](const auto& v) { first_connect_error = v; },
				[](const Socket_closed& v) { throw std::exception{ "Impossible" }; },
				[](const Success_concept auto& v) { throw std::exception{ "Impossible" }; }
			);
		}
	}

	// We have failed.
	// The loop over addresses would have returned already on success.
	// Now to return the proper error.

	if (addresses.Is_empty())
		return No_address_found{};

	// paranoia check - we know addresses was not empty so this should be impossible
	if (!first_connect_error.has_value())
		throw std::exception{ "Impossible error" };

	return std::move(first_connect_error.value());
}

Result<Success<Chars_sent>, Socket_closed, Wsa_error>
Tcp_stream::Send(const char* buffer, int data_length)
{
	using Return_type = Result<Success<Chars_sent>, Socket_closed, Wsa_error>;

	auto Can_we_write = [&]() {
		return m_Socket.Select(false, true, false, Socket_recv_timeout_millis);
	};

	// Success-true means we can write
	// Success-false means we timed out with nothing to read
	// ...
	// we try again until there is something to read (or an error)
	auto select_result = Can_we_write();
	while (select_result.Is_success() && (select_result.Get_success<bool>().Value == false))
		select_result = Can_we_write();

	if (!select_result.Is_success())
		return select_result.Visit(
			// we know its not success, the type-system dont know :(
			[](const Success_concept auto&) -> Return_type { throw std::exception{ "Impossible" }; },
			[](const auto& v) { return Return_type{ v }; }
	);

	return m_Socket.Send(buffer, data_length);
}

Result<Success<Chars_read>, End_of_stream, Socket_closed, Wsa_error>
Tcp_stream::Recv(char* buffer, int max_data_length)
{
	using Return_type = Result<Success<Chars_read>, End_of_stream, Socket_closed, Wsa_error>;

	auto Is_there_anything_to_read = [&]() {
		return m_Socket.Select(true, false, false, Socket_recv_timeout_millis);
	};

	// Success-true means there is something to read
	// Success-false means we timed out with nothing to read
	// ...
	// we try again until there is something to read (or an error)
	auto select_result = Is_there_anything_to_read();
	while (select_result.Is_success() && (select_result.Get_success<bool>().Value == false))
		select_result = Is_there_anything_to_read();

	if (!select_result.Is_success())
		return select_result.Visit(
			// we know its not success, the type-system dont know :(
			[](const Success_concept auto&) -> Return_type { throw std::exception{ "Impossible" }; },
			[](const auto& v) { return Return_type{ v }; }
	);

	return m_Socket.Recv(buffer, max_data_length);
}


