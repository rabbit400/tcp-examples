#include "Tcp_listener.h"
#include "Address_lookup.h"

#include <iostream>

using namespace Results;

Result<Success<Tcp_stream>, Incoming_connection_terminated, Socket_not_listening, Socket_closed, Wsa_error>
Tcp_listener::Accept_next_connection()
{
	using Return_type = Result<Success<Tcp_stream>, Incoming_connection_terminated, Socket_not_listening, Socket_closed, Wsa_error>;

	auto accept_result = m_Socket.Accept();

	if (!accept_result.Is_success())
	{
		return accept_result.Visit(
			[](const auto& v) { return Return_type{ v }; },
			[](const Success_concept auto& v) -> Return_type { throw new std::exception{ "Impossible" }; }
		);
	}

	Socket& new_socket = accept_result.Get_success().Value;
	return Tcp_stream{ std::move(new_socket) };
}

Result<Success<Tcp_listener>, No_address_found, Wsa_error>
Tcp_listener::Start_listener(const std::string& port_or_service)
{
	using Return_type = Result<Success<Tcp_listener>, No_address_found, Wsa_error>;

	Address_list addresses;
	{
		auto addresses_result = Address_lookup::for_listening(std::nullopt, port_or_service);
		if (!addresses_result.Is_success())
			return addresses_result.Get_non_success();
		addresses = std::move(addresses_result.Get_success().Value);
	}

	std::optional<Return_type> first_error;
	for (const auto& address : addresses)
	{
		auto create_result = Socket::Create_socket(address);

		// If we failed in _creating_ a socket we might as well give up here.
		if (!create_result.Is_success())
			return create_result.Get_non_success();

		Socket socket = std::move(create_result.Get_success().Value);

		auto bind_result = socket.Bind(address);
		if (!bind_result.Is_success())
		{
			if (!first_error.has_value())
			{
				bind_result.Visit(
					[&](const auto& v) { first_error = v; },
					// We know the socket is not closed because we havent closed it.
					// We know it is not success because we checked it.
					// Sadly the type-system doesnt know.
					[](const Socket_closed& v) { throw std::exception{ "Impossible" }; },
					[](const Success_concept auto& v) { throw std::exception{ "Impossible" }; }
				);
			}
			continue; // try next address
		}

		auto listen_result = socket.Listen();
		if (!listen_result.Is_success())
		{
			if (!first_error.has_value())
			{
				listen_result.Visit(
					[&](const auto& v) { first_error = v; },
					[](const Socket_closed& v) { throw std::exception{ "Impossible" }; },
					[](const Success_concept auto& v) { throw std::exception{ "Impossible" }; }
				);
			}
			continue; // try next address
		}

		return Tcp_listener{ std::move(socket) };
	}

	// We have failed.
	// The loop over addresses would have returned already on success.
	// Now to return the proper error.

	if (addresses.Is_empty())
		return No_address_found{};

	// paranoia check - we know addresses was not empty so this should be impossible
	if (!first_error.has_value())
		throw std::exception{ "Impossible error" };

	return std::move(first_error.value());
}
