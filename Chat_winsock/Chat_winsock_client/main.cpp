#include <string>
#include <iostream>

#include "Results.h"
#include "Winsock_api_init.h"
#include "Tcp_stream.h"
#include "Types.h"
#include "utf_conversion.h"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

using namespace Results;
const char * host = "localhost";
const char * port = "42069";

void test_dumd_stuff();

void Run_reader(Byte_reader&);

using Wstringish = decltype(Utf_conversion::Convert_u8_to_wcharish(std::u8string_view{}).Get_success().Value);

// TODO Results::Result<...>::Add_alternatives<decltype(
//Results::Result<Results::Success<Wstringish>, 

int main()
{
	test_dumd_stuff();

	std::cout << "Hello winsock chat client" << std::endl;
	std::cout << "Connecting..." << std::endl;

	std::u8string username;
#pragma region ....Query and read username from user
	{
		std::cout << "Whats your username?" << std::endl;

		std::wstring w_username;
		if (!std::getline(std::wcin, w_username))
		{
			std::cout << "Failed reading username." << std::endl;
			return 1;
		}

		auto conversion_result = Utf_conversion::Convert_wchar_to_u8(w_username);

		if (!conversion_result.Is_success())
		{
			std::cout << "Failed converting the username to utf8: " << conversion_result << std::endl;
			return 1;
		}

		username = std::move(conversion_result.Get_success().Value);
	}
#pragma endregion

	auto start_api_result = Winsock_api_init::Winsock_api_startup();
	if (!start_api_result.Is_success())
	{
		std::cout << "Failed initialization of winsock api: " << start_api_result << std::endl;
		return 1;
	}

	Tcp_stream stream;
	{
		auto connect_result = Tcp_stream::Connect(host, port);

		if (!connect_result.Is_success())
		{
			std::cout << "Failed connecting: " << connect_result << std::endl;
			return 1;
		}
		stream = std::move(connect_result.Get_success().Value);
	}
	
	Buffering_network_byte_writer stream_writer{ stream };
	Naive_network_byte_reader stream_reader{ stream };

	std::thread reader_thread{ [&stream_reader]() {Run_reader(stream_reader); } };
	reader_thread.detach();

	// set our username on the server
	stream_writer << (unsigned char)Message_type::Set_username;
	stream_writer << username;
	stream_writer.Flush();

	while (true)
	{
		std::wstring line;
		if (!std::getline(std::wcin, line))
		{
			std::cout << "Failed reading line from user." << std::endl;
			return 1;
		}

		auto conversion_result = Utf_conversion::Convert_wchar_to_u8(line);
		if (!conversion_result.Is_success())
		{
			std::cout << "Failed converting line to utf8: " << conversion_result << std::endl;
			return 1;
		}

		// sent "tell all" message
		stream_writer << (unsigned char)Message_type::Tell_all;
		stream_writer << conversion_result.Get_success().Value;
		stream_writer.Flush();
		if (!stream_writer)
		{
			std::cout << "Failed sending message." << std::endl;
			return 1;
		}
	}

	return 0;
}

void Run_reader(Byte_reader& reader)
{
	bool successfully_read_last_message = true;

	while (successfully_read_last_message)
	{
		successfully_read_last_message = false;

		unsigned char message_type_ubyte;
		reader >> message_type_ubyte;
		Message_type type = (Message_type)message_type_ubyte;
		switch (type)
		{
		case Message_type::Set_username:
		case Message_type::Tell_all:
		case Message_type::Whisper_to:
			std::wcout << "Protocol Error: Received client command from server." << std::endl;
			break;
		case Message_type::New_username:
		{
			std::u8string new_username;
			reader >> new_username;
			if (reader)
			{
				auto conv_result = Utf_conversion::Convert_u8_to_wcharish(new_username);
				if (conv_result.Is_success())
				{
					std::wcout
						<< "New username: "
						<< (const wchar_t *)conv_result.Get_success().Value.c_str()
						<< std::endl;
					successfully_read_last_message = true;
				}
			}
			break;
		}
		case Message_type::Told_from:
		{
			std::u8string username;
			std::u8string message;
			reader >> username >> message;
			if (reader)
			{
				auto conv_username_result = Utf_conversion::Convert_u8_to_wcharish(username);
				auto conv_message_result = Utf_conversion::Convert_u8_to_wcharish(message);
				if (conv_username_result.Is_success() && conv_message_result.Is_success())
				{
					std::wcout
						<< (const wchar_t*)conv_username_result.Get_success().Value.c_str()
						<< " : "
						<< (const wchar_t*)conv_message_result.Get_success().Value.c_str()
						<< std::endl;
					successfully_read_last_message = true;
				}
			}
			break;
		}
		case Message_type::Whispered_from:
		{
			std::u8string username;
			std::u8string message;
			reader >> username >> message;
			if (reader)
			{
				auto conv_username_result = Utf_conversion::Convert_u8_to_wcharish(username);
				auto conv_message_result = Utf_conversion::Convert_u8_to_wcharish(message);
				if (conv_username_result.Is_success() && conv_message_result.Is_success())
				{
					std::wcout
						<< (const wchar_t*)conv_username_result.Get_success().Value.c_str()
						<< " (whispers): "
						<< (const wchar_t*)conv_message_result.Get_success().Value.c_str()
						<< std::endl;
					successfully_read_last_message = true;
				}
			}
			break;
		}
		default:
			std::wcout << "Protocol Error: Received unrecognized message type: "
				<< (int)message_type_ubyte << std::endl;
			return;
		}
	}
	std::wcout << "Reader thread ended." << std::endl;
}

void test_dumd_stuff()
{
	Result<int, char> r1{ '3' };
	Result<int, char> r2{ r1 };

	//X x = X( 5 );
	//X y(x);
	//X z{};
}
