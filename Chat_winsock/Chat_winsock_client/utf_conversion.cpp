#include "utf_conversion.h"


Conversion_result<char8_t> Utf_conversion::Convert_u16_to_u8(std::u16string_view string_in)
{
	return Convert<char8_t>(string_in);
}

Conversion_result<char16_t> Utf_conversion::Convert_u8_to_u16(std::u8string_view string_in)
{
	return Convert<char16_t>(string_in);
}

Conversion_result<char8_t> Utf_conversion::Convert_u32_to_u8(std::u32string_view string_in)
{
	return Convert<char8_t>(string_in);
}

Conversion_result<char32_t> Utf_conversion::Convert_u8_to_u32(std::u8string_view string_in)
{
	return Convert<char32_t>(string_in);
}
