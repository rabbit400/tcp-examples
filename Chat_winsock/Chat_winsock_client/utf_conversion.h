#pragma once

#include <locale.h>
#include <string>
#include <locale>
#include <iostream>
#include "Results.h"

class Locale_raii final
{
	_locale_t m_Locale;
	Locale_raii(int category, const char* locale) { m_Locale = _create_locale(category, locale); }
	Locale_raii(const Locale_raii&) = delete;
	Locale_raii(Locale_raii&&) = delete;
	Locale_raii& operator = (const Locale_raii&) = delete;
	Locale_raii& operator = (Locale_raii&&) = delete;
public:
	~Locale_raii() { _free_locale(m_Locale); }
	_locale_t Locale() const { return m_Locale; }
	operator _locale_t () const { return m_Locale; }
	static Locale_raii Get_utf8_locale() { return Locale_raii(LC_ALL, ".utf8"); }
};


template <class char_out>
using Conversion_success = Results::Success<std::basic_string<char_out>>;

template <class char_t>
struct Conversion_error
{
	size_t Num_chars_converted;
	std::basic_string<char_t> partial_result;
	friend std::ostream& operator<<(std::ostream& stream, const Conversion_error<char_t> & err)
	{
		return stream << "Error converting to char type: " << typeid(char_t).name()
			<< ". After " << err.Num_chars_converted << " of chars successfully converted.";
	}
};

template<class char_out>
using Conversion_result = Results::Result<Conversion_success<char_out>, Conversion_error<char_out>>;

class Utf_conversion
{
	template <class Char_t> static void Grow(std::basic_string<Char_t>& str)
	{
		str.resize(str.size() + 1);
	}

public:

	template <class char_out, class char_in>
	static Results::Result<Results::Success<std::basic_string<char_out>>, Conversion_error<char_out>>
		Convert(const char_in* begin, const char_in* end)
	{
		// C++20 has template codecvt<Internal, External, State>.
		// This is only defined for utf16 or 32 as internal and utf8 as external.
		// I.e. only codecvt<char16_t, char8_t, ..> and codecvt<char32_t, char8_t, ..> exist.
		// Conversion is done calling facet.in(..) or facet.out(..).
		//   - in for external->internal
		//   - out for internal->external.
		// This means:
		//  - Utf8 must be the "external".
		//  - Utf16 or Utf32 is the "internal".
		// Hence we only support converting utf8 to/from utf16 or utf32
		// So we require (via constexpr if):
		//  - one char type is char8_t
		//  - the other is char16_t or char32_t

		constexpr bool input_is_utf8 = std::is_same_v<char_in, char8_t>;
		constexpr bool output_is_utf8 = std::is_same_v<char_out, char8_t>;

		// This validates input and output types:
		//  - _exactly_ one is utf8
		//  - the other is utf16 or utf32
		if constexpr (input_is_utf8)
		{
			static_assert(std::is_same_v<char_out, char16_t> || std::is_same_v<char_out, char32_t>,
				"Conversion only supported for utf 8 to/from utf 16 or utf 32.");
		}
		else if constexpr (output_is_utf8)
		{
			static_assert(std::is_same_v<char_in, char16_t> || std::is_same_v<char_in, char32_t>,
				"Conversion only supported for utf 8 to/from utf 16 or utf 32.");
		}
		else static_assert("Conversion only supported for utf 8 to/from utf 16 or utf 32.");

		using Codecvt_t = std::conditional_t<input_is_utf8,
			std::codecvt<char_out, char_in, std::mbstate_t>,
			std::codecvt<char_in, char_out, std::mbstate_t>>;

		const auto& facet = std::use_facet<Codecvt_t>(std::locale{});


		std::basic_string<char_out> string_out{};
		string_out.resize(0);
		//u8_string.resize(2 * u16_string.size());

		size_t num_chars_written = 0;
		size_t num_chars_read = 0;

		while (true)
		{
			std::mbstate_t state{};
			const char_in* begin_in = begin + num_chars_read;
			const char_in* end_in = end;
			const char_in* pos_in{};
			char_out* begin_out = string_out.data() + num_chars_written;
			char_out* end_out = string_out.data() + string_out.size();
			char_out* pos_out{};

			std::codecvt_base::result res;
			
			if constexpr (input_is_utf8)
				res = facet.in(state, begin_in, end_in, pos_in, begin_out, end_out, pos_out);
			else
				res = facet.out(state, begin_in, end_in, pos_in, begin_out, end_out, pos_out);

			num_chars_read += pos_in - begin_in;
			num_chars_written += pos_out - begin_out;

			switch (res)
			{
			case std::codecvt_base::partial:
				Grow(string_out);
				continue;

			case std::codecvt_base::ok:
				return Conversion_success<char_out>(string_out.substr(0, num_chars_written));

			case std::codecvt_base::error:
				return Conversion_error{ num_chars_read, string_out.substr(0, num_chars_written) };

			default:
			case std::codecvt_base::noconv:
				throw std::exception("impossible");
			}
		}
	}

	template <class char_out, class char_in>
	static Conversion_result<char_out>
		Convert(const std::basic_string_view<char_in>& string_in)
	{
		return Convert<char_out>(string_in.data(), string_in.data() + string_in.size());
	}

	static Conversion_result<char8_t> Convert_u16_to_u8(std::u16string_view string_in);
	static Conversion_result<char16_t> Convert_u8_to_u16(std::u8string_view string_in);
	static Conversion_result<char8_t> Convert_u32_to_u8(std::u32string_view string_in);
	static Conversion_result<char32_t> Convert_u8_to_u32(std::u8string_view string_in);
	
	// Assumes string_in is either utf16 or utf32 depending on sizeof(whar)
	// if sizeof(wchar_t) is weird it wont compile
	static Conversion_result<char8_t> Convert_wchar_to_u8(std::wstring_view string_in)
	{
		if constexpr (sizeof(wchar_t) == 2 && sizeof(char16_t) == 2)
		{
			auto view = std::u16string_view((const char16_t*)string_in.data(), string_in.size());
			return Convert_u16_to_u8(view);
		}
		if constexpr (sizeof(wchar_t) == 4 && sizeof(char32_t) == 4)
		{
			auto view = std::u32string_view((const char32_t*)string_in.data(), string_in.size());
			return Convert_u32_to_u8(view);
		}
		static_assert("size of wchar_t does not match chat16_t or char32_t");
	}

	// return std::u16string or std::u32string depending of sizeof(char)
	// if sizeof(wchar_t) is weird it wont compile
	static auto Convert_u8_to_wcharish(std::u8string_view string_in)
	{
		if constexpr (sizeof(wchar_t) == 2 && sizeof(char16_t) == 2)
		{
			return Convert_u8_to_u16(string_in);
		}
		if constexpr (sizeof(wchar_t) == 4 && sizeof(char32_t) == 4)
		{
			return Convert_u8_to_u32(string_in);
		}
		static_assert("size of wchar_t does not match chat16_t or char32_t");
	}


};

