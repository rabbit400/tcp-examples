#pragma once

//#include <inte>
#include <climits>
#include <cstdint>
#include "Tcp_stream.h"
#include <vector>

class Byte_writer
{
	static_assert(CHAR_BIT == 8);
public:
	virtual bool Has_failed() const = 0;
	virtual void Write_byte(char c) = 0;
	virtual ~Byte_writer() = default;
	
	operator bool () const { return !Has_failed(); }
};

class Byte_reader
{
	static_assert(CHAR_BIT == 8);
public:
	virtual char Read_byte() = 0;
	virtual bool Has_failed() const = 0;
	virtual ~Byte_reader() = default;
	
	operator bool () const { return !Has_failed(); }
};

template <class W> concept Byte_writer_c = std::derived_from<W, Byte_writer>;
template <class R> concept Byte_reader_c = std::derived_from<R, Byte_reader>;

template <Byte_writer_c W> W& operator << (W& w, char c) { w.Write_byte(c); return w; }
template <Byte_reader_c R> R& operator >> (R& r, char& c) { c = r.Read_byte(); return r; }

template <Byte_writer_c W> W& operator << (W& w, unsigned char uc)
{
	return w << (char)uc;
}
template <Byte_reader_c R> R& operator >> (R& r, unsigned char& uc)
{
	char c = (char)uc;
	r >> c;
	uc = (unsigned char)c;
	return r;
}

template <Byte_writer_c W> W& operator << (W& w, uint32_t ui)
{
	w << (unsigned char)(ui % 256);
	ui /= 256;
	w << (unsigned char)(ui % 256);
	ui /= 256;
	w << (unsigned char)(ui % 256);
	ui /= 256;
	w << (unsigned char)(ui % 256);
	return w;
}
template <Byte_reader_c R> R& operator >> (R& r, uint32_t & ui)
{
	unsigned char uc0{}, uc1{}, uc2{}, uc3{};
	if (r >> uc0 >> uc1 >> uc2 >> uc3)
	{
		ui	= uc0
			+ uc1 * (uint32_t)(256)
			+ uc2 * (uint32_t)(256 * 256)
			+ uc3 * (uint32_t)(256 * 256 * 256);
	}
	return r;
}

template <Byte_writer_c W> W& operator << (W& w, const std::u8string & u8s)
{
	w << (uint32_t)u8s.size();
	for (auto u8_char : u8s)
		w << (unsigned char)u8_char;
	return w;
}
template <Byte_reader_c R> R& operator >> (R& r, std::u8string& u8s)
{
	uint32_t size{};
	r >> size;

	// if we failed reading size, the value could be nasty
	if (!r) return r;

	std::u8string ret{};
	ret.reserve(size);
	for (uint32_t i = 0; i < size && r; i++)
	{
		unsigned char c{};
		r >> c;
		ret.push_back(c);
	}

	if (r) u8s = std::move(ret);

	return r;
}

class Buffering_network_byte_writer : public Byte_writer
{
	Tcp_stream& m_Stream;
	std::vector<char> m_Buffer{};
	using Send_result = decltype(m_Stream.Send(nullptr, 1));
	std::optional<Send_result> m_Error{};
public:
	Buffering_network_byte_writer(Tcp_stream& stream) : m_Stream(stream) {}
	const auto& Error() const { return m_Error; }
	virtual void Write_byte(char c) override { m_Buffer.push_back(c); }
	void Flush()
	{
		size_t num_chars_to_write = m_Buffer.size();
		const char* position_of_next_char_to_write = m_Buffer.data();

		while (num_chars_to_write > 0 && !m_Error.has_value())
		{
			auto send_result = m_Stream.Send(position_of_next_char_to_write, num_chars_to_write);
			if (send_result.Is_success())
			{
				size_t num_chars_sent = (size_t)send_result.Get_success().Value.Num_chars;
				num_chars_to_write -= num_chars_sent;
				position_of_next_char_to_write += num_chars_sent;
			}
			else
			{
				m_Error.emplace(send_result);
			}
		}

		m_Buffer.clear();
	}
	virtual bool Has_failed() const override { return m_Error.has_value(); }
};

class Naive_network_byte_writer : public Byte_writer
{
	Tcp_stream& m_Stream;
	using Send_result = decltype(m_Stream.Send(nullptr, 1));
	std::optional<Send_result> m_Error{};
public:
	Naive_network_byte_writer(Tcp_stream& stream) : m_Stream(stream) {}
	const auto& Error() const       { return m_Error; }
	virtual void Write_byte(char c) override
	{
		if (m_Error.has_value()) return;
		auto send_result = m_Stream.Send(&c, 1);
		if (!send_result.Is_success()) m_Error.emplace(send_result);
	}
	virtual bool Has_failed() const override { return m_Error.has_value(); }
};

class Naive_network_byte_reader : public Byte_reader
{
	Tcp_stream& m_Stream;
	using Recv_result = decltype(m_Stream.Recv(nullptr, 1));
	std::optional<Recv_result> m_Error;
public:
	Naive_network_byte_reader(Tcp_stream& stream) : m_Stream(stream) {}
	const auto& Error() const { return m_Error; }
	virtual char Read_byte()
	{
		if (m_Error.has_value()) return '\0';
		
		char c{};
		auto recv_result = m_Stream.Recv(&c, 1);
		if (!recv_result.Is_success()) m_Error = recv_result;
		return c;
	}
	virtual bool Has_failed() const override { return m_Error.has_value(); }
};

static_assert(8 == CHAR_BIT);
enum class Message_type : unsigned char
{
	Set_username = 1,
	Whisper_to,
	Tell_all,
	Whispered_from,
	Told_from,
	New_username,
};

