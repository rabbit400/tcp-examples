#pragma once

#include <string>
#include <optional>

#include "Address_list.h"
#include "results.h"
#include "Winsock_result_types.h"

class Address_lookup
{
public:
	static Results::Result<Results::Success<Address_list>, Wsa_error>
		for_connecting(
			const std::string& host,
			const std::string& port_or_servicename);

	static Results::Result<Results::Success<Address_list>, Wsa_error>
		for_listening(
			const std::optional<std::string>& host,
			const std::string& port_or_servicename);
};
