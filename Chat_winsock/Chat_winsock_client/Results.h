#pragma once

#include <concepts>
#include <type_traits>
#include <variant>
#include <ostream>
#include "../Packs/Packs.h"

namespace Results
{
	template<typename T>
	concept Streamable_concept = requires (const T & t, std::ostream & stream)
	{
		stream << t;
	};

	template <Streamable_concept T, Streamable_concept ... Alts>
	std::ostream& operator << (std::ostream& stream, const std::variant<T, Alts...>& results)
	{
		std::visit(
			[&stream](const auto& result) { stream << result; },
			results
		);
		return stream;
	}

	template <typename ...>
	struct Success;

	template <>
	struct Success<>
	{
		friend std::ostream& operator<<(std::ostream& stream, const Success<>& v)
		{
			stream << "Success";
			return stream;
		}
	};

	template <typename T>
	struct Success<T>
	{
		using Value_type = T;
		T Value;
		Success() = default;
		Success(T&& t) :Value{ std::forward<T>(t) } {}
		Success(const T& t) :Value{ t } {}

		friend std::ostream& operator<<(std::ostream& stream, const Success<T>& v)
			requires Streamable_concept<T>
		{
			return stream << "Success with value: " << v.Value;
		}
		friend std::ostream& operator<<(std::ostream& stream, const Success<T>& v)
			requires (!Streamable_concept<T>)
		{
			return stream << "Success with non-streamable value";
		}
	};

	template <typename T>
	struct Is_success : std::false_type {};

	template <typename ... T>
	struct Is_success<Success<T...>> : std::true_type {};

	template<typename T>
	bool Is_success_value(const T& t) { return Is_success<T>::value; }

	template <typename T>
	struct Is_non_success : std::negation<Is_success<T>> {};


	template<typename T>
	concept Success_concept = Is_success<T>::value;

	template<typename T>
	concept Non_success_concept = Is_non_success<T>::value;

	template<class... Pack_types> struct overloaded : Pack_types... { using Pack_types::operator()...; };

	struct Bad_result_access : public std::exception
	{
		Bad_result_access() noexcept = default;
		virtual const char* what() const noexcept override { return "bad result access"; }
	};

	struct Impossible : public std::exception
	{
		Impossible() noexcept = default;
		virtual const char* what() const noexcept override { return "Impossible things happened!"; }
	};

	template<typename ... Alts> // short for Alernative_types
	struct Result
	{
		using Variant_t = std::variant<Alts...>;
		using Alternatives_pack = Packs::Pack<Alts...>;
		using Success_alternatives_pack = Packs::Filter_pack<Is_success, Alternatives_pack>;
		using Non_success_alternatives_pack = Packs::Filter_pack<Is_non_success, Alternatives_pack>;
		constexpr static size_t Num_alternatives = Alternatives_pack::Count;
		constexpr static size_t Num_success_alternatives = Success_alternatives_pack::Count;
		constexpr static size_t Num_non_success_alternatives = Non_success_alternatives_pack::Count;
		static_assert(Num_alternatives == Num_success_alternatives + Num_non_success_alternatives);

		std::variant<Alts...> Variant;

		Result() = default;

			template <class T>
			requires std::constructible_from<Variant_t, const T&>
		Result(const T& v) : Variant{ v } {}
			template <class T>
			requires std::constructible_from<Variant_t, T&&>
		Result(T&& v) : Variant{ std::move(v) } {}

			template <class T>
			requires std::assignable_from<Variant_t, const T&>
		auto& operator =(const T& v) { Variant = v; return *this; }
			template <class T>
			requires std::assignable_from<Variant_t, T&&>
		auto& operator =(T&& v) { Variant = std::move(v); return *this; }

		template <typename ... Vs>
		auto Visit(Vs&& ... visitors) const &
		{
			return std::visit(overloaded<Vs...>{ std::forward<Vs>(visitors)... }, Variant);
		}
		template <typename ... Vs>
		auto Visit(Vs&& ... visitors) &
		{
			return std::visit(overloaded<Vs...>{ std::forward<Vs>(visitors)... }, Variant);
		}
		template <typename ... Vs>
		auto Visit(Vs&& ... visitors) &&
		{
			return std::visit(overloaded<Vs...>{ std::forward<Vs>(visitors)... }, std::move(Variant));
		}

		friend std::ostream& operator << (std::ostream& stream, const Result& result)
		{
			result.Visit(
				[&stream](const auto& v) { stream << "Unstreamable result"; },
				[&stream](const Streamable_concept auto& v) { stream << v; } );
			return stream;
		}

		template<typename T> bool Holds() { return std::holds_alternative<T>(Variant); }

		template <typename T> const T& Get() const & { return std::get<T>(Variant); }
		template <typename T>       T& Get() &  { return std::get<T>(Variant); }
		template <typename T>      T&& Get() && { return std::get<T>(std::move(Variant)); }

		bool Is_success() const
		{
			return Visit([](const auto& a) { return Results::Is_success_value(a); });
		}

		template <typename ... T> const Success<T...>&  Get_success() const & { return Get<Success<T...>>(); }
		template <typename ... T>       Success<T...>&  Get_success()& { return Get<Success<T...>>(); }
		template <typename ... T>       Success<T...>&& Get_success()&& { return std::move(*this).Get<Success<T...>>(); }

		const auto& Get_success() const & requires (Num_success_alternatives == 1)
		{
			return Get<Success_alternatives_pack::First>();
		}
		auto& Get_success()& requires (Num_success_alternatives == 1)
		{
			return Get<Success_alternatives_pack::First>();
		}
		auto&& Get_success()&& requires (Num_success_alternatives == 1)
		{
			return std::move(*this).Get<Success_alternatives_pack::First>();
		}

		const auto& Get_non_success() const & requires (Num_non_success_alternatives == 1)
		{
			return Get<Non_success_alternatives_pack::First>();
		}
		auto& Get_non_success()& requires (Num_non_success_alternatives == 1)
		{
			return Get<Non_success_alternatives_pack::First>();
		}
		auto&& Get_non_success()&& requires (Num_non_success_alternatives == 1)
		{
			return std::move(*this).Get<Non_success_alternatives_pack::First>();
		}

		auto Get_success_result() const &
		{
			using Return_t = Packs::Apply_pack<Results::Result, Success_alternatives_pack>;
			return Visit(
				[](const Success_concept auto& v) -> Return_t { return v; },
				[](const Non_success_concept auto& v) -> Return_t { throw Bad_result_access(); }
			);
		}
		auto Get_success_result() &&
		{
			using Return_t = Packs::Apply_pack<Results::Result, Success_alternatives_pack>;
			return std::move(*this).Visit(
				[](Success_concept auto&& v) -> Return_t { return std::move(v); },
				[](const Non_success_concept auto& v) -> Return_t { throw Bad_result_access(); }
			);
		}

		auto Get_non_success_result() const &
		{
			using Return_t = Packs::Apply_pack<Results::Result, Non_success_alternatives_pack>;
			return Visit(
				[](const Non_success_concept auto& v) -> Return_t { return v; },
				[](const Success_concept auto& v) -> Return_t { throw Bad_result_access(); }
			);
		}
		auto Get_non_success_result() &&
		{
			using Return_t = Packs::Apply_pack<Results::Result, Non_success_alternatives_pack>;
			return std::move(*this).Visit(
				[](Non_success_concept auto && v) -> Return_t { return std::move(v); },
				[](const Success_concept auto& v) -> Return_t { throw Bad_result_access(); }
			);
		}
	};
}
