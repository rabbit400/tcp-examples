#pragma once

#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

#include <memory>
#include <mutex>
#include <variant>
#include <optional>

#include "Results.h"
#include "Winsock_result_types.h"

// Important:
// For reading, use the Select(...) function before calling Recv(...). 
// Only call Recv if there is something to read.
// In Select(...) use a timeout to not block forever.
// These rules are because any blocking call obstructs the socket closing.
// (I.e. CLose_eventually() means close after any blocking calls are ended.)
// 
// Details:
// Socket class has a shared pointer to a socket handle raii wrapper.
// The winsock-socket-handle is closed in the destructor.
// All operations keep a copy of the shared_ptr.
// This ensures the winsock-socket-handle is not closed during an operation.
//
// Our socket object is "closed" by setting the shared_ptr to null.
// This does not "winsock-close" the socket until all operations have finished.
// If there is a blocking operation the socket might stay "winsock-open" indefinitely.
// This is an inherent flaw in the winsock api.
// There is no safe way to interrupt a blocking call. 
//
class Socket
{
	struct Raii_socket_handle;

	mutable std::mutex m_Mutex;
	std::shared_ptr<Raii_socket_handle> m_Handle_ptr;

	std::shared_ptr<Raii_socket_handle> Get_handle_ptr();
	Socket(SOCKET handle);

public:
	Socket() = default;
	Socket(Socket&& moved_from) noexcept;
	Socket& operator =(Socket&& moved_from) noexcept;
	~Socket() noexcept;

	// This will "close" the socket object for any new calls.
	// If there are current blocking calls the underlying handle will not
	// be winsock-closed and availible for reuse until after they have finished.
	void Close_eventually() noexcept;

	bool Is_open();

	static Results::Result<Results::Success<Socket>, Wsa_error>
		Create_socket(int address_family, int socket_type, int protocol);

	static Results::Result<Results::Success<Socket>, Wsa_error>
		Create_socket(const addrinfo& address);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Connect(const sockaddr& binary_address, int lenght_of_address);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Connect(const addrinfo& address);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Bind(const sockaddr& binary_address, int lenght_of_address);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Bind(const addrinfo& address);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Listen(int backlog = SOMAXCONN);

	// returns Incoming_connection_terminated - there was a connection but its terminated already.
	// returns Socket_not_listening - probably you forgot to call socket.Listen before socket.Accept.
	Results::Result<
		Results::Success<Socket>,
		Incoming_connection_terminated,
		Socket_not_listening,
		Socket_closed,
		Wsa_error>
		Accept();

	Results::Result<
		Results::Success<Chars_sent>,
		Socket_closed,
		Wsa_error>
		Send(const char* buffer, int data_length);

	Results::Result<
		Results::Success<Chars_read>,
		End_of_stream,
		Socket_closed,
		Wsa_error>
		Recv(char* buffer, int max_data_length);

	Results::Result<Results::Success<bool>, Socket_closed, Wsa_error>
		Select(bool read, bool write, bool except, std::optional<int> timeout_millis);

	Results::Result<Results::Success<>, Socket_closed, Wsa_error>
		Set_socket_option(int level, int optname, const char* optval, int optlen);
};

