#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include "Results.h"
#include "Winsock_result_types.h"

class Winsock_api_init
{
	WSADATA m_Wsa_data;
	bool m_Moved_from;

	Winsock_api_init(const WSADATA& wsadata);

public:
	Winsock_api_init(Winsock_api_init&& moved_from) noexcept;
	Winsock_api_init& operator = (Winsock_api_init&& moved_from) noexcept;
	~Winsock_api_init() noexcept;

	const WSADATA& Wsa_data() const { return m_Wsa_data; }

	static Results::Result<Results::Success<Winsock_api_init>, Wsa_error>
		Winsock_api_startup();
};
