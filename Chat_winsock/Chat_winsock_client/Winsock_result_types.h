#pragma once

#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <ostream>

struct Wsa_error
{
	int Error_code;
	Wsa_error(int error) : Error_code{ error } {}

	friend std::ostream& operator<<(std::ostream& stream, const Wsa_error& v)
	{
		return stream << "Winsock api error: " << v.Error_code;
	}

	static Wsa_error WSA_Error_not_socket() { return Wsa_error(WSAENOTSOCK); }
	static Wsa_error Undocumented_error() { return Wsa_error(WSASYSCALLFAILURE); }

	static Wsa_error Get_wsa_last_error()
	{
		return Wsa_error{ WSAGetLastError() };
	}
};


struct Chars_sent
{
	int Num_chars;
	Chars_sent(int n) :Num_chars{ n } {}
	friend std::ostream& operator<<(std::ostream& stream, const Chars_sent& v)
	{
		return stream << "Sent " << v.Num_chars << " chars";
	}
};

struct Chars_read
{
	int Num_chars;
	Chars_read(int n) :Num_chars{ n } {}
	friend std::ostream& operator<<(std::ostream& stream, const Chars_read& v)
	{ return stream << "Received " << v.Num_chars << " chars"; }
};

struct End_of_stream
{
	friend std::ostream& operator<<(std::ostream& stream, const End_of_stream& v)
	{ return stream << "End of stream"; }
};

struct Socket_closed
{
	friend std::ostream& operator<<(std::ostream& stream, const Socket_closed& v)
	{ return stream << "Socket closed"; }
};

struct Socket_not_listening
{
	friend std::ostream& operator<<(std::ostream& stream, const Socket_not_listening& v)
	{ return stream << "Socket is not listening"; }
};

struct Incoming_connection_terminated
{
	friend std::ostream& operator<<(std::ostream& stream, const Incoming_connection_terminated& v)
	{ return stream << "Incomming connection was terminated"; }
};

struct No_address_found
{
	friend std::ostream& operator<<(std::ostream& stream, const No_address_found& v)
	{ return stream << "No address found"; }
};
