#include "Address_lookup.h"

#define WIN32_LEAN_AND_MEAN
#include <Ws2tcpip.h>

using namespace Results;

Result<Success<Address_list>, Wsa_error>
Address_lookup::
for_connecting(const std::string& hostname, const std::string& port_or_service_name)
{
	struct addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	const char* port_cstr = port_or_service_name.c_str();
	const char* hostname_cstr = hostname.c_str();

	// Resolve the server address and port
	addrinfo* addresses_head;
	int result_code = getaddrinfo(hostname_cstr, port_cstr, &hints, &addresses_head);
	if (result_code == 0)
		return Address_list{ addresses_head };
	else
		return Wsa_error{ result_code };
}

Result<Success<Address_list>, Wsa_error>
Address_lookup::
for_listening(const std::optional<std::string>& host, const std::string& port_or_service_name)
{
	struct addrinfo hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE; // this makes it an 'address' for listening

	const char* port_cstr = port_or_service_name.c_str();
	const char* hostname_cstr
		= host.has_value() ? host.value().c_str() : nullptr;

	addrinfo* addresses_head;
	int result_code = getaddrinfo(hostname_cstr, port_cstr, &hints, &addresses_head);

	if (result_code == 0)
		return Address_list{ addresses_head };
	else
		return Wsa_error{ result_code };
}

