#pragma once

#include "Socket.h"
#include "Results.h"
#include "Winsock_result_types.h"

class Tcp_stream
{
	friend class Tcp_listener;

	Socket m_Socket;
	Tcp_stream(Socket&& socket) : m_Socket(std::move(socket)) {}

public:

	Tcp_stream() = default;
	Tcp_stream(const Tcp_stream&) = delete;
	Tcp_stream(Tcp_stream&&) = default;
	Tcp_stream& operator =(const Tcp_stream&) = delete;
	Tcp_stream& operator =(Tcp_stream&&) = default;

	~Tcp_stream() = default;

	void Close() { m_Socket.Close_eventually(); }
	bool Is_open() { return m_Socket.Is_open(); }

	static Results::Result<Results::Success<Tcp_stream>, No_address_found, Wsa_error>
		Connect(const std::string& hostname, const std::string& port_or_service);

	Results::Result<Results::Success<Chars_sent>,Socket_closed,Wsa_error>
		Send(const char* buffer, int data_length);

	Results::Result<Results::Success<Chars_read>, End_of_stream, Socket_closed, Wsa_error>
		Recv(char* buffer, int max_data_length);

private:
	static constexpr int Socket_send_timeout_millis = 200;
	static constexpr int Socket_recv_timeout_millis = 200;
};

