#include "Address_list.h"

#define WIN32_LEAN_AND_MEAN
#include <Ws2tcpip.h>

void Address_list::move_from(Address_list& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return;
	reset();
	m_Head = moved_from.m_Head;
	moved_from.m_Head = nullptr;
}

Address_list::
Address_list(addrinfo* head)
	: m_Head{ head }
{}

Address_list::
Address_list(Address_list&& moved_from) noexcept
{
	move_from(moved_from);
}

Address_list&
Address_list::
operator = (Address_list&& moved_from) noexcept
{
	move_from(moved_from);
	return *this;
}

Address_list::
~Address_list()
{
	reset();
}

bool Address_list::Is_empty() const
{
	return m_Head == nullptr;
}

void Address_list::reset()
{
	if (m_Head != nullptr)
	{
		freeaddrinfo(m_Head);
		m_Head = nullptr;
	}
}

Address_list::Iterator
Address_list::begin() const
{
	return Iterator(m_Head);
}

Address_list::Iterator
Address_list::end() const
{
	return Iterator();
}
