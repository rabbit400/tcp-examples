#include <iostream>

#include "Winsock_api_init.h"
#include "Address_lookup.h"
#include "Tcp_stream.h"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

const char* hostname_cstr = "localhost";
const char* port_cstr = "42069";

// enable to show delimeters between the data blocks
const bool print_buffer_delimiters = true;
const int socket_read_buffer_length = 2048;

void Run_socket_read_loop(Tcp_stream & stream);
void Run_socket_write_loop(Tcp_stream & stream);

using namespace results;

int main()
{
	// 1) Initialize winsock api.
	// 2) Connect to an echo server.
	// 3) Start a thread reading from socket and writing to standard out.
	// 4) Start a thread reading from standard in and writing to socket.
	// 5) Wait for both threads to end.
	// 6) Do cleanup and exit. (Though we normally wont get here.)

	std::cout << "Hello third winsock echo client!" << std::endl;
	std::cout << "Connecting..." << std::endl;

	// 1) Initialize winsock api
	// start_api_result is our Raii object that calls WSACleanup in its destructor.
	auto start_api_result = Winsock_api_init::Winsock_api_startup();
	if (!start_api_result.Is_success())
	{
		std::cout << "Failed initialization of winsock api: " << start_api_result << std::endl;
		return 1;
	}

	// 2) Connect to an echo server.
	Tcp_stream stream;
	{
		auto connect_result = Tcp_stream::Connect(hostname_cstr, port_cstr);

		if (!connect_result.Is_success())
		{
			std::cout << "Failed connecting: " << connect_result << std::endl;
			return 1;
		}

		stream = std::move(connect_result.Get_success<Tcp_stream>().Value);
	}
	std::cout << "Connected" << std::endl;

	// 3) start a thread reading from socket and writing to standard out.
	auto socket_reader_thread = std::thread(Run_socket_read_loop, std::ref(stream));

	// 4) Start a thread reading from standard in and writing to socket.
	auto socket_writer_thread = std::thread(Run_socket_write_loop, std::ref(stream));

	// 5) Wait for both threads to end.
	// Will almost never happen cause there is no way for a user to exit :-)
	socket_reader_thread.join();
	socket_writer_thread.join();

	// 6) Do cleanup and exit. (Though we normally wont get here.)
	// Zero-code-cleanup with raii.
	std::cout << "Exit with fashion." << std::endl;
}

void Run_socket_write_loop(Tcp_stream & stream)
{
	bool no_fail_yet = true;
	std::string line;
	while (no_fail_yet && std::getline(std::cin, line))
	{
		line.push_back('\n'); // getline removed any line-ending so we add one

		// A single call to send(..) might not send all data.
		// Therefart we loop trying to send any remaining data untill all is sent.
		size_t chars_sent = 0;
		while (no_fail_yet && chars_sent < line.size())
		{
			char* position = line.data() + chars_sent;
			int chars_left_to_send = (int)line.size() - (int)chars_sent;

			auto send_result = stream.Send(position, chars_left_to_send);

			if (send_result.Is_success())
			{
				const Chars_sent& cs = send_result.Get_success().Value;
				chars_sent += cs.Num_chars;
			}
			else // failed to send data
			{    // exit our loops which will end the thread
				no_fail_yet = false;
				std::cout << "ERROR sending to socket: " << send_result << std::endl;
			}
		}
	}

	std::cout << "Socket writer loop ended." << std::endl;
}

void Run_socket_read_loop(Tcp_stream& stream)
{
	// By making the buffer length +1 we gain space to set \0 at the end.
	// Then we can write it as a c-style string to std::cout.
	char buffer[socket_read_buffer_length + 1];

	while (true)
	{
		auto recv_result = stream.Recv(buffer, socket_read_buffer_length);

		// if we faile, write error and end thread
		if (!recv_result.Is_success())
		{
			std::cout << "Failed reading from socket: " << recv_result;
			break;
		}

		// add \0 after bytes read to make it a proper c-style string
		int num_chars_read = recv_result.Get_success().Value.Num_chars;
		buffer[num_chars_read] = 0;

		std::cout << buffer;
		if (print_buffer_delimiters) std::cout << " | ";
		std::cout.flush();
	}

	std::cout << "Socket reader loop ended." << std::endl;
}