#pragma once

#include "Socket.h"
#include "results.h"

class Tcp_stream
{
	Socket m_Socket;
	Tcp_stream(Socket&& socket) : m_Socket(std::move(socket)) {}

public:

	Tcp_stream() = default;
	Tcp_stream(const Tcp_stream &) = delete;
	Tcp_stream(Tcp_stream &&) = default;
	Tcp_stream& operator =(const Tcp_stream &) = delete;
	Tcp_stream& operator =(Tcp_stream &&) = default;

	~Tcp_stream() = default;

	void Close() { m_Socket.Close_eventually(); }
	bool Is_open() { return m_Socket.Is_open(); }

	static results::Result<results::Success<Tcp_stream>, results::No_address_found, results::Wsa_error>
		Connect(const std::string& hostname, const std::string& port_or_service);

	results::Result<
		results::Success<results::Chars_sent>,
		results::Socket_closed,
		results::Wsa_error>
		Send(const char* buffer, int data_length);

	results::Result<
		results::Success<results::Chars_read>,
		results::End_of_stream,
		results::Socket_closed,
		results::Wsa_error>
		Recv(char* buffer, int max_data_length);

private:
	static constexpr int Socket_send_timeout_millis = 200;
	static constexpr int Socket_recv_timeout_millis = 200;
};

