#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

class Address_list
{
	addrinfo* m_Head = nullptr;
	void move_from(Address_list& moved_from) noexcept;
public:
	Address_list() = default;
	Address_list(addrinfo* head);
	Address_list(Address_list&& moved_from) noexcept;
	Address_list& operator = (Address_list&& moved_from) noexcept;
	~Address_list();

	class Iterator
	{
		const addrinfo* m_Current;
	public:
		Iterator() = default;
		Iterator(const addrinfo* begin) : m_Current{ begin } {}
		bool operator == (const Iterator& it) const = default;
		bool operator != (const Iterator& it) const = default;

		Iterator& operator++() { m_Current = m_Current->ai_next; return *this; }
		const addrinfo& operator *() const { return *m_Current; }
	};

	bool Is_empty() const;

	void reset();
	Iterator begin() const;
	Iterator end() const;
};
