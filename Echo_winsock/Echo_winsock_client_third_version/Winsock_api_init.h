#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include "results.h"

class Winsock_api_init
{
	WSADATA m_Wsa_data;
	bool m_Moved_from;

	Winsock_api_init(const WSADATA & wsadata);

public:
	Winsock_api_init(Winsock_api_init&& moved_from) noexcept;
	Winsock_api_init& operator = (Winsock_api_init&& moved_from) noexcept;
	~Winsock_api_init() noexcept;

	const WSADATA& Wsa_data() const { return m_Wsa_data; }

	static results::Result<results::Success<Winsock_api_init>, results::Wsa_error>
		Winsock_api_startup();
};
