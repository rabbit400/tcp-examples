#pragma once

#include <string>
#include <optional>

#include "Address_info_list.h"

// Functions and result type to do address lookup (getaddrinfo).
//
// Why?
//  - Use the raii type Address_info_list for results.
//  - Simplify address lookup by assuming tcp with ip4.
//  - Simplify address lookup by returning a single result object.
class Address_lookup
{
	Address_info_list m_Addresses;
	int m_Result_code = 0;
protected:
	Address_lookup(int result_code, Address_info_list&& addresses)
		: m_Result_code{ result_code }, m_Addresses{ std::move(addresses) } {}
public:
	Address_lookup() = default;
	Address_lookup(Address_lookup&& moved_from) = default;
	Address_lookup& operator = (Address_lookup&& moved_from) = default;

	int Result_code() const { return m_Result_code; }
	bool Success() const { return m_Result_code == 0; }
	const Address_info_list& addresses() const { return m_Addresses; }

	static Address_lookup for_connecting(const std::string& host, const std::string& port_or_servicename);
	static Address_lookup for_listening(const std::optional<std::string>& host, const std::string& port_or_servicename);
};
