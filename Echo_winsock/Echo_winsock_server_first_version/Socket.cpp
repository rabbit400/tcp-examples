#include "Socket.h"

// Sadly most usages of this function are not fully thread-safe.
// _Probably_ the error is very rare. Especially in this program.
// We will address this in later examples.
//
// Broken Scenario:
// Thread A:
//  - gets the handle here
//  - is paused
// Other threads _during pause_: 
//  - closes socket
//  - creates new socket
//  - the new socket gets the same handle
// Thread A
//  - wakes up
//  - uses the handle
// ISSUE:
//    Thread A uses a valid handle that is a completely different socket than intended.
// NOTE:
//    - This only when a handle is _reassigned_. If it is not reassigned thread A would
//      get a proper error result for a closed socket.
//    - The window for the pause is only between getting the handle (releasing the mutex)
//      and beginning the winsock call. When the winsock call is sufficiently far along
//      a closesocket call interrupts the blocking call - which is our aim.
// Not trivial to solve because:
//    When thread A uses the handle in a blocking call we can not hold the mutex.
//    The only sure way to interrupt a blocking call is with closesocket.
SOCKET Socket::Get_handle()
{
	std::lock_guard l{ m_Mutex };
	return m_Handle;
}

Socket::Socket(SOCKET handle, std::optional<int> creation_error) noexcept
	: m_Handle{ handle }, m_Creation_error{ creation_error }
{}

Socket::Socket(Socket&& moved_from) noexcept
{
	std::scoped_lock lock{ m_Mutex, moved_from.m_Mutex };
	m_Handle = moved_from.m_Handle;
	m_Creation_error = moved_from.m_Creation_error;
	// Make sure moved from doesnt close the handle.
	moved_from.m_Handle = INVALID_SOCKET;
}

Socket& Socket::operator =(Socket&& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return *this;
	std::scoped_lock guard{ m_Mutex, moved_from.m_Mutex };
	Close();

	m_Handle = moved_from.m_Handle;
	m_Creation_error = moved_from.m_Creation_error;

	// Make sure moved from doesnt close the handle.
	moved_from.m_Handle = INVALID_SOCKET;

	return *this;
}

Socket::Socket(int address_family, int socket_type, int protocol)
{
	m_Handle = socket(address_family, socket_type, protocol);
	if (m_Handle == INVALID_SOCKET)
		m_Creation_error = WSAGetLastError();
}

Socket::Socket(const addrinfo& address)
	: Socket(address.ai_family, address.ai_socktype, address.ai_protocol)
{ }

Socket::~Socket() { Close(); }

int Socket::Close()
{
	std::lock_guard l{ m_Mutex };
	if (m_Handle == INVALID_SOCKET)
		return WSAENOTSOCK;
	int ret = closesocket(m_Handle);
	m_Handle = INVALID_SOCKET;
	return ret;
}

bool Socket::Successfully_created() const
{
	return !m_Creation_error.has_value();
}

const std::optional<int> Socket::Creation_error() const
{
	return m_Creation_error;
}

bool Socket::Is_valid() const
{
	std::lock_guard l{ m_Mutex };
	return m_Handle != INVALID_SOCKET;
}

std::optional<int> Socket::Connect(const addrinfo& address)
{
	auto h = Get_handle();
	// result == 0 means success
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// result == anything else is not documented
	int connect_result = connect(h, address.ai_addr, (int)address.ai_addrlen);
	if (connect_result == 0)
		return std::nullopt;
	else if (connect_result == SOCKET_ERROR)
		return WSAGetLastError();
	else // undocumented connect_result
		return (int)WSASYSCALLFAILURE;
}

std::optional<int> Socket::Bind(const addrinfo& address)
{
	auto h = Get_handle();
	// result == 0 means success
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// result == anything else is not documented
	int bind_result = bind(h, address.ai_addr, (int)address.ai_addrlen);
	if (bind_result == 0)
		return std::nullopt;
	else if (bind_result == SOCKET_ERROR)
		return WSAGetLastError();
	else // undocumented bind_result
		return (int)WSASYSCALLFAILURE;
}

std::optional<int> Socket::Listen(int backlog)
{
	auto h = Get_handle();
	int listen_result = listen(h, backlog);
	// result == 0 means success
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// result == anything else is not documented
	if (listen_result == 0)
		return std::nullopt;
	else if (listen_result == SOCKET_ERROR)
		return WSAGetLastError();
	else // undocumented listen_result
		return (int)WSASYSCALLFAILURE;
}

Socket Socket::Accept()
{
	auto h = Get_handle();
	// handle result == INVALID_SOCKET means error - call WSAGetLastError()
	// handle result == anything else means Success, valid handle
	SOCKET new_handle = accept(h, nullptr, nullptr);
	if (new_handle == INVALID_SOCKET)
		return Socket(new_handle, WSAGetLastError());
	else
		return Socket(new_handle, std::nullopt);
}

// The return values/results from this function are a bit complicated.
// Read the comment in the header file.
std::optional<int>
Socket::
Receive(char* buffer, int max_chars, int* num_chars_received)
{
	auto h = Get_handle();
	// Right HERE we worry a lot about the thread-unsafety of Get_handle().
	// Because a common simple networking solution is one reader-thread per socket
	// calling Receive(..) repeatedly while a separate thread is responsible for
	// interrupting the reader-thread when its time to end. 
	//
	// Sadly the only sure way to interrupt is via closesocket. With closesocket
	// there is a risk that the handle, h, is closed and reassigned to a new 
	// socket right here. Which... would be bad.

	// result > 0 means success, result is number of characters read
	// result == 0 means socket is gracefully closed
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	//
	// weird results that should never happen:
	// result > max_chars is unclear from documentation
	//            we treat it as undocumented error
	//            though it is scary, does it mean a buffer overrun write?
	// result < 0  is not documented (except SOCKET_ERROR)
	int recv_result = recv(h, buffer, max_chars, 0);

	if (recv_result == SOCKET_ERROR) // documented error
	{
		*num_chars_received = 0;
		return WSAGetLastError();
	}
	else if (recv_result == 0) // socket gracefully closed
	{
		*num_chars_received = recv_result;
		return std::nullopt;
	}
	else if (recv_result > 0 && recv_result <= max_chars) // success
	{
		*num_chars_received = recv_result;
		return std::nullopt;
	}
	else // undocumented error
	{
		*num_chars_received = 0;
		return (int)WSASYSCALLFAILURE;
	}
}

std::optional<int>
Socket::
Send(const char* buffer, int num_chars_to_send, int* num_chars_sent)
{
	auto h = Get_handle();

	// result >= 0 means success, result is number of characters sent
	//         (yes 0 is weird, we treat it as success for now)
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// 
	// weird results that should never happen:
	// result > num_chars_to_send not documentated
	//            we treat it as undocumented error
	//            though it is scary, does it mean a buffer overrun read?
	// result < 0  is not documented (except SOCKET_ERROR)
	int send_result = send(h, buffer, num_chars_to_send, 0);

	if (send_result == SOCKET_ERROR) // documented error
	{
		*num_chars_sent = 0;
		return WSAGetLastError();
	}
	else if (send_result >= 0 && send_result <= num_chars_to_send) // success
	{
		*num_chars_sent = send_result;
		return std::nullopt;
	}
	else // undocumented error
	{
		*num_chars_sent = 0;
		return (int)WSASYSCALLFAILURE;
	}
}
