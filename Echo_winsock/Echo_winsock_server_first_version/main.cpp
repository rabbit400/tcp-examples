#include "Winsock_api_init.h"
#include "Address_lookup.h"
#include "Socket.h"

#include <iostream>
#include <thread>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

const char* hostname_cstr = "localhost";
const char* port_cstr = "42069";

const int socket_read_buffer_length = 2048;
// Enable to print all received data to console.
const bool write_data_to_standard_out = true;
// Enable to call Socket.Send for each byte one at a time instead of trying
// to send the whole buffer. This is inefficient but usefull to illustrate
// tcp behaviours in a client.
const bool send_a_single_byte_at_a_time = true;

void Run_a_reader_thread(Socket&& sock);

int main()
{
    // 1) Initialize the winsock api.
    // 2) Lookup address candidates list for a listening socket.
    // 3) Open a listening socket. 
    //     Foreach candidate address, retry untill success:
    //     a) Create a socket.
    //     b) Bind the socket.
    // 4) Start listening for new connections.
    // 5) Loop forever.
    //     a) Accept each new connection.
    //     b) Start its reader thread.
    //
    // The reader thread:
    // Loop till there is an error or graceful shutdown, doing:
    //   6) read a chunk of data from connection_socket
    //   7) write the data to connection_socket


	std::cout << "Hallu winsock server! (first version)" << std::endl;

	// 1) Initialize the winsock api.
	Winsock_api_init wsa_init_raii{};
    if (!wsa_init_raii.Success())
    {
        std::cout << "Failed initializing winsock api; WSAStartup failed: "
            << wsa_init_raii.Error_code()
            << std::endl;
        return 1;
    }
    // We do not need to call WSACleanup(). Its called in wsa_init_raii destructor.

    // 2) Lookup address data for a listening socket.
    auto addresses = Address_lookup::for_listening(std::nullopt, port_cstr);
    
    if (!addresses.Success())
    {
        std::cout << "Failed address lookup: "
            << addresses.Result_code()
            << std::endl;
        return 1;
    }
    // We do not need to call freeaddrinfo. Its called in addresses destructor.
    
    // 3) Open a listening socket for an addres:
    std::optional<Socket> listener;
    for (auto& address : addresses.addresses())
    {
        // a) Create a new socket.
        // b) Bind the socket.
        // on failure:
        // - destroy socket so we dont accidentially use it
        // - jump to next address

        listener.emplace(address);
        
        if (listener->Creation_error())
        {
            // TODO output address
            std::cout << "Failed creating socket: "
                << listener->Creation_error().value()
                << std::endl;
            listener.reset();
            continue;
        }

        // one reason this could fail is if the port is already bound
        // e.g. if youre already running this server
        auto bind_error = listener->Bind(address);

        if (bind_error.has_value())
        {
            // TODO output address
            std::cout << "Failed binding socket: "
                << bind_error.value()
                << std::endl;
            listener.reset();
            continue;
        }

        // Success! We have an open listener socket.
        break; // Exit this loop with a valid socket in listener.
    }
    // We do not need to call closesocket. Its called in listener destructor.

    if (!listener.has_value())
    {
        // one reason this happens is if the port was already bound
        // for example if youre already running this server
        std::cout << "Unable to open a listener socket." << std::endl;
        return 1;
    }

    // 4) Start listening for new connections.
    auto listen_error = listener->Listen();

    if (listen_error.has_value())
    {
        std::cout << "Failed enabling listening for new connections: "
            << listen_error.value()
            << std::endl;
        return 1;
    }
    
    // 5) Loop forever.
    while (true)
    {
        // 5 a) Accept each new connection.
        Socket new_connection_socket = listener->Accept();

        // 5 b) Start its reader thread.
        if (new_connection_socket.Successfully_created())
        {
            std::thread t = std::thread{ Run_a_reader_thread, std::move(new_connection_socket) };
            t.detach(); // let the thread keep running after we destroy t.
        }
        else
        {
            // TODO write info on who the connector was
            std::cout << "Failed accepting a new connection: "
                << new_connection_socket.Creation_error().value()
                << std::endl;
        }
    }

    std::cout << "exiting" << std::endl;
}

void Run_a_reader_thread(Socket&& connection_socket)
{
    // TODO write who was connected, i.e. ip-address, port, anything else?
    std::cout << "New reader thread!" << std::endl;

    // The +1 adds an extra char to buffer length so we have room for a \0 at then end.
    // Then we can send buffer as a c-style string to std::cout.
    char buffer[socket_read_buffer_length + 1];
    int num_chars_received;

    while (true)
    {
        // 6) read a chunk of data from connection_socket
        auto error_code = connection_socket.Receive(buffer, socket_read_buffer_length, &num_chars_received);

        if (error_code.has_value())
        {
            std::cout << "Error reading from socket: " << error_code.value() << std::endl;
            break; // exit read loop, will end the thread
        }
        if (num_chars_received == 0)
        {
            std::cout << "Connection gracefully closed. " << std::endl;
            break; // exit read loop, will end the thread
        }
        // these cases are impossible but we are very defensive here
        if (num_chars_received < 0)
            throw std::length_error("Socket.Receive read NEGATIVE number of chars");
        if (num_chars_received > socket_read_buffer_length)
            throw std::length_error("Socket.Receive read TOO MANY chars");

        if (write_data_to_standard_out)
        {
            buffer[num_chars_received] = 0;
            std::cout << ">" << buffer << std::endl;
        }

        // 7) write the data to connection_socket
        // Note that a send call might not write all data.
        // So we need to loop and try to send any remaining data until its all sent.
        bool failed_sending_data = false;
        int num_chars_sent = 0;
        while (num_chars_sent < num_chars_received && !failed_sending_data)
        {
            char * position_in_buffer = &buffer[num_chars_sent];
            int num_chars_to_send = num_chars_received - num_chars_sent;
            if (send_a_single_byte_at_a_time)
                num_chars_to_send = 1; // this is very inefficient

            // out param - it will receive value from the call
            int num_chars_sent_this_call;
            auto send_result = connection_socket.Send(position_in_buffer, num_chars_to_send, &num_chars_sent_this_call);
            
            if (send_result.has_value()) // error
            {
                std::cout << "Error sending data: " << send_result.value() << std::endl;
                failed_sending_data = true;
            }
            else
            {
                num_chars_sent += num_chars_sent_this_call;
            }
        }

        if (failed_sending_data)
            break; // exit read loop, will end the thread
    }
    // TODO write who was connected
    std::cout << "Reader thread ended." << std::endl;
}

