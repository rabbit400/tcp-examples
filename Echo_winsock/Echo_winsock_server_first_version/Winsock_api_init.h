#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

// RAII class to simplify start and cleanup of the winsock api.
// Constructor calls WSAStartup and destructor calls WSACleanup().
//
// Usage:
// Create an object before using winsock, destruct it when youre done.
// PLEASE check the result code with api_init.Success().
//
// Why?
// To use winsock functions you first need to call WSAStartup once or more.
// To close the api you call WSACleanup as many times as you have called
// WSAStartup.
//
// details:
// There is no copy constructor or assignment operator, only move = or move constructor.
class Winsock_api_init
{
	WSADATA m_Wsa_data;
	int m_Result_code;
public:
	Winsock_api_init();
	~Winsock_api_init();
	Winsock_api_init(Winsock_api_init&& moved_from) noexcept;
	Winsock_api_init& operator = (Winsock_api_init&& moved_from) noexcept;

	int Error_code() { return m_Result_code; }
	bool Success() { return m_Result_code == 0; }
	const WSADATA& Wsa_data() const { return m_Wsa_data; }
};
