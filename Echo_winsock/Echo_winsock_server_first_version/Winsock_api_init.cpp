#include "Winsock_api_init.h"


Winsock_api_init::Winsock_api_init()
{
	m_Result_code = WSAStartup(MAKEWORD(2, 2), &m_Wsa_data);
}

Winsock_api_init::Winsock_api_init(Winsock_api_init&& moved_from) noexcept
	: m_Wsa_data(moved_from.m_Wsa_data)
	, m_Result_code(moved_from.m_Result_code)
{
	// make sure moved_from doesnt call WSACleanup() in desctructor
	moved_from.m_Result_code = 1;
}

Winsock_api_init& Winsock_api_init::operator =(Winsock_api_init&& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return *this;
	if (m_Result_code == 0) WSACleanup();
	m_Wsa_data = moved_from.m_Wsa_data;
	m_Result_code = moved_from.m_Result_code;
	// make sure moved_from doesnt call WSACleanup() in destructor
	moved_from.m_Result_code = 1;
	return *this;
}

Winsock_api_init::~Winsock_api_init()
{
	if (m_Result_code == 0) WSACleanup();
}
