#pragma once

#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

#include <string>
#include <optional>
#include <mutex>

#include "Address_info_list.h"

// This class encapsulates a socket handle.
//
// Why?
//   - Raii.
//   - Enforcement of a single place for the handle.
//   - Improvements on thread-safety (though not fully safe... yet).
//   - Slightly clearer error indications for most calls.
//
// details:
// - For undocumented ("impossible") errors I have used WSASYSCALLFAILURE as return value.
//     Would make sense to abort() or throw exception instead.
// - There is no copy constructor or assignment operator, only move constructor.
//     Its not trivial to make a thread-safe move assignment. 
//     (But ya, it can be done, maybe in later examples.)
class Socket
{
    std::mutex mutable m_Mutex;
    SOCKET m_Handle;
    std::optional<int> m_Creation_error;

    // This function tries really hard but is not fully thread-safe.
    // See implementation for a long comment on why.
    SOCKET Get_handle();

    Socket(SOCKET handle, std::optional<int> creation_error) noexcept;

public:
    Socket(Socket&& moved_from) noexcept;
    Socket & operator =(Socket&& moved_from) noexcept;
    ~Socket();

    // These constructors can fail, check x.Successfully_created().
    // If creation failed check Creation_error() for error code.
    Socket(int address_family, int socket_type, int protocol);
    Socket(const addrinfo& address);

    int Close();

    bool Successfully_created() const;
    const std::optional<int> Creation_error() const;
    bool Is_valid() const;
    
    // All of these optional<int> are error codes.
    // If it has a value there was an ERROR and the value is the error code.
    std::optional<int> Connect(const addrinfo& address);
    std::optional<int> Bind(const addrinfo& address);
    std::optional<int> Listen(int backlog = SOMAXCONN);
    std::optional<int> Send(const char* buffer, int num_chars_to_send, int* num_chars_sent);

    // Accept always returns a Socket but can fail, check x.Successfully_created().
    // If creation failed check Creation_error() for error code.
    Socket Accept();

    // So...
    // The return values/results from this function are a bit complicated.
    // We will try making a neater result type using std::variant in later examples.
    //
    // 3 possible results:
    //  - Error:
    //      non-empty return value/error code (it comes from WSAGetLastError)
    //  - Success:
    //      empty return value/error code
    //      positive number of chars received - value is set in num_chars_received
    //  - Connection closed gracefully:
    //      empty return value/error code
    //      0 chars received - value is set in num_chars_received
    std::optional<int> Receive(char* buffer, int max_chars, int* num_chars_received);
};
