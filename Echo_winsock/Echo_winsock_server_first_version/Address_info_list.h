#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

// RAII class for the address list returned by address-lookup calls (getaddrinfo).
// Also provides foreach iteration.
// 
// Why?
// There is magic freeaddrinfo(..) you have to call to cleanup an addresslist.
//
// details:
// There is no copy constructor or assignment operator, only move = or move constructor.
class Address_info_list
{
	addrinfo* m_Head = nullptr;
	void move_from(Address_info_list& moved_from) noexcept;
public:
	Address_info_list() = default;
	Address_info_list(addrinfo* head);
	Address_info_list(Address_info_list&& moved_from) noexcept;
	Address_info_list& operator = (Address_info_list&& moved_from) noexcept;
	~Address_info_list();

	class Iterator
	{
		const addrinfo* m_Current;
	public:
		Iterator() = default;
		Iterator(const addrinfo* begin) : m_Current{ begin } {}
		bool operator == (const Iterator& it) const = default;
		bool operator != (const Iterator& it) const = default;

		Iterator& operator++() { m_Current = m_Current->ai_next; return *this; }
		const addrinfo& operator *() const { return *m_Current; }
	};

	void reset();
	Iterator begin() const;
	Iterator end() const;
};