Echo_client_first_version:
  Uses the straight C-style winsock api.
  main():
  - Creates a socket and connects it to a server.
  - Runs two threads.
  Socket write thread:
   - Reads lines from console.
   - Send the bytes to the socket.
  Socket read thread:
   - Reads data from socket.
   - Writes the bytes to console as c-style strings.
     (Optionally writes delimiters, " | ", between the data chunks.)

Echo_server_first_version:
  Introduces c++ classes wrapping the api calls:
   - Winsock_api_init: raii for winsock init and cleanup.
   - Address_info_list: raii and foreach (iterators) for addresses linked list.
   - Address_lookup: neater result types for address lookup.
   - Socket: encapsulation, raii, thread-safety-ish and neater result types.
  main():
   - Create a listening socket and bind it.
   - Foreach new connection start a reader thread.
  Reader thread:
   - read data from the connected socket
   - write it back to the connected socket
     (Optionally write data a single byte at a time.)




