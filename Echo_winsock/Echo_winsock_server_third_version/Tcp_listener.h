#pragma once

#include "Socket.h"
#include "Tcp_stream.h"

class Tcp_listener
{
	Socket m_Socket;
	Tcp_listener(Socket&& socket) : m_Socket(std::move(socket)) {}

public:

	Tcp_listener() = default;
	Tcp_listener(const Tcp_listener&) = delete;
	Tcp_listener(Tcp_listener&&) = default;
	Tcp_listener& operator =(const Tcp_listener&) = delete;
	Tcp_listener& operator =(Tcp_listener&&) = default;

	~Tcp_listener() = default;

	void Close() { m_Socket.Close_eventually(); }
	bool Is_open() { return m_Socket.Is_open(); }

	// returns Incoming_connection_terminated - there was a connection but its terminated already.
	results::Result<
		results::Success<Tcp_stream>,
		results::Incoming_connection_terminated,
		results::Socket_not_listening,
		results::Socket_closed,
		results::Wsa_error>
		Accept_next_connection();

	static results::Result<results::Success<Tcp_listener>, results::No_address_found, results::Wsa_error>
		Start_listener(const std::string& port_or_service);
};

