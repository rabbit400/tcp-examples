#include <iostream>

#include <optional>

#include "Winsock_api_init.h"
#include "Address_lookup.h"
#include "Socket.h"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

const char* port_cstr = "42069";

const int socket_read_buffer_length = 2048;
// Enable to print all received data to console.
const bool write_data_to_standard_out = true;
// Enable to call Socket.Send for each byte one at a time instead of trying
// to send the whole buffer. This is inefficient but usefull to illustrate
// tcp behaviours in a client.
const bool send_a_single_byte_at_a_time = true;

using namespace results;

void run_in_a_reader_thread(Socket&& socket);

int main()
{
	std::cout << "Hello winsock server! (second version)" << std::endl;

	// Raii object that calls WSACleanup in its destructor.
	std::optional<Winsock_api_init> winsock_init;
	{
		auto start_api_result = Winsock_api_init::Winsock_api_startup();
		if (!start_api_result.Is_success())
		{
			std::cout << "Failed initialization of winsock api: " << start_api_result << std::endl;
			return 1;
		}
		winsock_init = std::move(start_api_result.Get_success().Value);
	}

	Address_list addresses;
	{
		auto addresses_result = Address_lookup::for_listening(std::nullopt, port_cstr);
		if (!addresses_result.Is_success())
		{
			std::cout << "Failed adddress lookup: " << addresses_result << std::endl;
			return 1;
		}
		addresses = std::move(addresses_result.Get_success().Value);
	}

	Socket listener_socket;
	for (const auto& address : addresses)
	{
		auto create_result = Socket::Create_socket(address);

		if (!create_result.Is_success())
		{
			// If we failed in _creating_ a socket we might as well give up here.
			std::cout << "Failed creating socket: " << create_result << std::endl;
			return 1;
		}

		Socket& socket = create_result.Get_success().Value;

		auto bind_result = socket.Bind(address);

		if (!bind_result.Is_success())
		{
			// Bind failed, try the next address, i.e. continue.
			std::cout << "Failed binding to one address: " << bind_result << std::endl;
			continue;
		}

		auto listen_result = socket.Listen();
		if (listen_result.Is_success())
		{
			// Success! We have an open listener socket.
			listener_socket = std::move(socket);
			break;
		}
		else
		{
			// Listen failed, try the next address, i.e. continue.
			std::cout << "Failed opening socket for listening: " << listen_result << std::endl;
			continue;
		}
	}

	if (!listener_socket.Is_open())
	{
		std::cout << "Could not open any address for listening. Exiting. " << std::endl;
		return 1;
	}

	while (true)
	{
		auto accept_result = listener_socket.Accept();

		if (accept_result.Is_success())
		{
			Socket& socket = accept_result.Get_success().Value;
			std::thread t{ run_in_a_reader_thread, std::move(socket) };
			t.detach();
		}
		else if (accept_result.Holds<Incoming_connection_terminated>())
		{
			// The connection was closed before we accepted it.
			// This is not an error so we dont exit, just accept the next connection.
			std::cout << "A new connection was closed before we accepted it." << std::endl;
		}
		else
		{
			// error, exit program
			std::cout << "Error accepting connection: " << accept_result << std::endl;
			return 1;
		}
	}

	return 0;
}

void run_in_a_reader_thread(Socket&& reader_socket_)
{
	// TODO write who was connected, i.e. ip-address, port, anything else?
	std::cout << "New reader thread!" << std::endl;

	Socket socket = std::move(reader_socket_);

	// The +1 adds an extra char to buffer length so we have room for a \0 at then end.
	// Then we can send buffer as a c-style string to std::cout.
	char buffer[socket_read_buffer_length + 1];

	while (true)
	{
		auto recv_result = socket.Recv(buffer, socket_read_buffer_length);
		
		// end loop and reader thread if we failed
		if (!recv_result.Is_success())
		{
			std::cout << "Read from client failed: " << recv_result << std::endl;
			break;
		}

		int num_chars_received = recv_result.Get_success().Value.Num_chars;

		if (write_data_to_standard_out)
		{
			buffer[num_chars_received] = 0;
			std::cout << ">" << buffer << std::endl;
		}

		bool failed_sending_data = false;
		int num_chars_sent = 0;
		while (num_chars_sent < num_chars_received && !failed_sending_data)
		{
			char* position_in_buffer = &buffer[num_chars_sent];
			int num_chars_to_send = num_chars_received - num_chars_sent;
			if (send_a_single_byte_at_a_time)
				num_chars_to_send = 1; // this is very inefficient

			auto send_result = socket.Send(position_in_buffer, num_chars_to_send);

			if (send_result.Is_success())
			{
				int num_chars_sent_this_call = send_result.Get_success().Value.Num_chars;
				num_chars_sent += num_chars_sent_this_call;
			}
			else
			{
				std::cout << "Error sending data: " << send_result << std::endl;
				failed_sending_data = true;
			}
		}

		if (failed_sending_data)
			break; // exit read loop, will end the thread
	}

	std::cout << "Ending reader thread." << std::endl;
}
