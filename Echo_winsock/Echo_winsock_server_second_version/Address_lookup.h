#pragma once

#include <string>
#include <optional>
#include <variant>

#include "Address_list.h"
#include "results.h"

class Address_lookup
{
public:
	static results::Result<results::Success<Address_list>, results::Wsa_error>
		for_connecting(
			const std::string& host,
			const std::string& port_or_servicename);

	static results::Result<results::Success<Address_list>, results::Wsa_error>
		for_listening(
			const std::optional<std::string>& host,
			const std::string& port_or_servicename);
};
