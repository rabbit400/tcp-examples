#pragma once

#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

#include <memory>
#include <mutex>
#include <variant>
#include "results.h"

// Details:
// Socket class has a shared pointer to a socket handle raii wrapper.
// The winsock-socket-handle is closed in the destructor.
// All operations keep a copy of the shared_ptr.
// This ensures the winsock-socket-handle is not closed during an operation.
//
// Our socket object is "closed" by setting the shared_ptr to null.
// This does not "winsock-close" the socket until all operations have finished.
// If there is a blocking operation the socket might stay "winsock-open" indefinitely.
// We will address this in later examples.
// This is an inherent flaw in the winsock api.
// There is no safe way to interrupt a blocking call. 
class Socket
{
	struct Raii_socket_handle;

	mutable std::mutex m_Mutex;
	std::shared_ptr<Raii_socket_handle> m_Handle_ptr;

	std::shared_ptr<Raii_socket_handle> Get_handle_ptr();
	Socket(SOCKET handle);

public:
	Socket() = default;
	Socket(Socket&& moved_from) noexcept;
	Socket& operator =(Socket&& moved_from) noexcept;
	~Socket() noexcept;

	// This will "close" the socket object for any new calls.
	// If there are current blocking calls the underlying handle will not
	// be winsock-closed and availible for reuse until after they have finished.
	void Close_eventually() noexcept;

	bool Is_open();

	static results::Result<results::Success<Socket>, results::Wsa_error>
		Create_socket(int address_family, int socket_type, int protocol);

	static results::Result<results::Success<Socket>, results::Wsa_error>
		Create_socket(const addrinfo& address);

	results::Result<results::Success<>, results::Socket_closed, results::Wsa_error>
		Connect(const sockaddr& binary_address, int lenght_of_address);

	results::Result<results::Success<>, results::Socket_closed, results::Wsa_error>
		Connect(const addrinfo& address);

	results::Result<results::Success<>, results::Socket_closed, results::Wsa_error>
		Bind(const sockaddr& binary_address, int lenght_of_address);

	results::Result<results::Success<>, results::Socket_closed, results::Wsa_error>
		Bind(const addrinfo& address);

	results::Result<results::Success<>, results::Socket_closed, results::Wsa_error>
		Listen(int backlog = SOMAXCONN);
	
	// Incoming_connection_terminated - there was a connection but its terminated already.
	// Socket_not_listening - probably you forgot to call socket.Listen before socket.Accept.
	results::Result<
		results::Success<Socket>,
		results::Incoming_connection_terminated,
		results::Socket_not_listening,
		results::Socket_closed,
		results::Wsa_error>
	Accept();

	results::Result<
		results::Success<results::Chars_sent>,
		results::Socket_closed,
		results::Wsa_error>
	Send(const char* buffer, int data_length);

	results::Result<
		results::Success<results::Chars_read>,
		results::End_of_stream,
		results::Socket_closed,
		results::Wsa_error>
	Recv(char* buffer, int max_data_length);
};

