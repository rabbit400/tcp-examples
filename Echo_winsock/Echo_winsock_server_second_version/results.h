#pragma once
#pragma once
#define WIN32_LEAN_AND_MEAN

#include <WinSock2.h>

#include <concepts>
#include <ostream>
#include <variant>

// Result types for functions that can fail.
// Esentially return std::variant<R1, R2, ...> instead of error codes and pointers and stuffs.
// (Though our Result wrapper makes it neater.)
// 
// Result<T, Ts...> is a wrap of std::variant<T, Ts...>.
// Neater member functions for some messy std-functions:
//   - Holds<T>  std::holds_alternative as a member function.
//   - Get<T>()         std::get as a member function
//   - Visit<T>()       std::visit as a member function
//   - std::ostream& operator<<(...) if all alternatives are streamable (have that operator)
// Adds some simple functionalities for dectecting and getting a successful result:
//   - Is_success()     true is the result is a success.
//   - Get_success<T>() to save the result::Success part of Get<..> for a success result.
//   - Get_success()    when its a single Success type T is not needed.
//
// We define several result types:
// Success<>     - operation succeeded
// Success<T>    - operation succeeded and returned value of T
// Chars_sent
// Chars_read
// End_of_stream
// Socket_closed
// Wsa_error     - operation failed with a winsock error code
//
// All these can be streamed to std::out.
// So a Result using these can be streamed.
// And so error handling is simpler.
// 
// I.e:
//     If result.Is_success():
//         proceed using the value
//     Else: 
//         std::cout << result or Log_error(result) or whatugonnado.
//         retry, return false or whatever
// Without (neccessarily) writing error-type specific code.
// Error details are still there if we want them.
namespace results
{
	template<typename T>
	concept Streamable_concept = requires (const T & t, std::ostream & stream)
	{
		stream << t;
	};

	template <Streamable_concept T, Streamable_concept ... Rest>
	std::ostream& operator << (std::ostream& stream, const std::variant<T, Rest...>& results)
	{
		std::visit(
			[&stream](const auto& result) { stream << result; },
			results
		);
		return stream;
	}

	template <typename ...>
	struct Success;

	template <>
	struct Success<>
	{
		friend std::ostream& operator<<(std::ostream& stream, const Success<>& v)
		{
			stream << "Success";
			return stream;
		}
	};

	template <typename T>
	struct Success<T>
	{
		using Value_type = T;
		T Value;
		Success() = default;
		Success(T&& t) :Value{ std::forward<T>(t) } {}
		Success(const T& t) :Value{ t } {}

		friend std::ostream& operator<<(std::ostream& stream, const Success<T>& v)
			requires Streamable_concept<T>
		{
			return stream << "Success with value: " << v.Value;
		}
		friend std::ostream& operator<<(std::ostream& stream, const Success<T>& v)
			requires (!Streamable_concept<T>)
		{
			return stream << "Success with non-streamable value";
		}
	};

	template <typename T>
	struct Is_success { const static bool Value = false; };

	template <typename ... T>
	struct Is_success<Success<T...>> { const static bool Value = true; };

	template<typename T>
	bool Is_success_value(const T& t) { return Is_success<T>::Value; }


	template <typename ... Pack_types>
	struct Count_success_types;

	template <>
	struct Count_success_types<> { const static size_t Value = 0; };

	template <typename T, typename ... Pack_types>
	struct Count_success_types<T, Pack_types...>
	{
		const static size_t Value = (size_t)(Is_success<T>::Value ? 1 : 0)
			+ Count_success_types<Pack_types...>::Value;
	};


	template <typename ... Ts>
	struct First_success_type;

	template <typename T, typename ... Ts>
	requires Is_success<T>::Value
		struct First_success_type<T, Ts...> { using Type = T; };

	template <typename T, typename ... Ts>
	requires (!Is_success<T>::Value)
		struct First_success_type<T, Ts...> { using Type = First_success_type<Ts...>::Type; };


	struct Chars_sent
	{
		int Num_chars;
		Chars_sent(int n) :Num_chars{ n } {}
		friend std::ostream& operator<<(std::ostream& stream, const Chars_sent& v)
		{
			return stream << "Sent " << v.Num_chars << " chars";
		}
	};

	struct Chars_read
	{
		int Num_chars;
		Chars_read(int n) :Num_chars{ n } {}
		friend std::ostream& operator<<(std::ostream& stream, const Chars_read& v)
		{
			return stream << "Received " << v.Num_chars << " chars";
		}
	};

	struct End_of_stream
	{
		friend std::ostream& operator<<(std::ostream& stream, const End_of_stream& v)
		{
			return stream << "End of stream";
		}
	};

	struct Socket_closed
	{
		friend std::ostream& operator<<(std::ostream& stream, const Socket_closed& v)
		{
			return stream << "Socket closed";
		}
	};

	struct Socket_not_listening
	{
		friend std::ostream& operator<<(std::ostream& stream, const Socket_not_listening& v)
		{
			return stream << "Socket is not listening";
		}
	};

	struct Incoming_connection_terminated
	{
		friend std::ostream& operator<<(std::ostream& stream, const Incoming_connection_terminated& v)
		{
			return stream << "Incomming connection was terminated";
		}
	};

	struct Wsa_error
	{
		int Error_code;
		Wsa_error(int error) : Error_code{ error } {}
		friend std::ostream& operator<<(std::ostream& stream, const Wsa_error& v)
		{
			return stream << "Winsock api error: " << v.Error_code;
		}

		static Wsa_error WSA_Error_not_socket() { return Wsa_error(WSAENOTSOCK); }
		static Wsa_error Undocumented_error() { return Wsa_error(WSASYSCALLFAILURE); }

		static Wsa_error Get_wsa_last_error()
		{
			return Wsa_error{ WSAGetLastError() };
		}
	};

	struct Stream_result_struct
	{
		std::ostream& stream;
		void operator()(const auto& v) const { stream << "Unstreamable result"; }
		void operator()(const Streamable_concept auto& v) const { stream << v; }
	};


	template<typename R0, typename ... Rest>
	struct Result
	{
		constexpr static size_t Num_success_alternatives = Count_success_types<R0, Rest...>::Value;
		
		std::variant<R0, Rest...> Variant;

		Result(const std::variant<R0, Rest...>& v) : Variant{ v } {}
		Result(std::variant<R0, Rest...>&& v) : Variant{ std::move(v) } {}

		Result(const auto& v) : Variant{ v } {}
		template <typename T>
		Result(T&& v) : Variant{ std::forward<T>(v) } {}

		template <typename V> auto Visit(V&& visitor) const
		{
			return std::visit(std::forward<V>(visitor), Variant);
		}
		template <typename V> auto Visit(V&& visitor)
		{
			return std::visit(std::forward<V>(visitor), Variant);
		}

		friend std::ostream& operator<<(std::ostream& stream, const Result<R0, Rest...> & v)
		{
			v.Visit(Stream_result_struct{ stream });
			return stream;
		}

		template<typename T> bool Holds() { return std::holds_alternative<T>(Variant); }

		template <typename T> T& Get() { return std::get<T>(Variant); }
		template <typename T> const T& Get() const { return std::get<T>(Variant); }

		bool Is_success() const
		{
			return Visit([](const auto& a) {return results::Is_success_value(a); });
		}

		template <typename ... T> const Success<T...>& Get_success() const { return Get<Success<T...>>(); }
		template <typename ... T>       Success<T...>& Get_success()       { return Get<Success<T...>>(); }

		auto& Get_success() requires (Num_success_alternatives == 1)
		{
			return Get<First_success_type<R0, Rest...>::Type>();
		}
		const auto& Get_success() const requires (Num_success_alternatives == 1)
		{
			return Get<First_success_type<R0, Rest...>::Type>();
		}
	};
}
