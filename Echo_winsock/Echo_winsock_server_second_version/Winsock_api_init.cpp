#include "Winsock_api_init.h"

using namespace results;

Winsock_api_init::Winsock_api_init(const WSADATA & wsadata)
	: m_Wsa_data{wsadata}, m_Moved_from{false}
{}

Winsock_api_init::Winsock_api_init(Winsock_api_init&& moved_from) noexcept
	: m_Wsa_data(moved_from.m_Wsa_data)
	, m_Moved_from(moved_from.m_Moved_from)
{
	// make sure moved_from doesnt call WSACleanup() in desctructor
	moved_from.m_Moved_from = true;
}

Winsock_api_init& Winsock_api_init::operator =(Winsock_api_init&& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return *this;
	if (!m_Moved_from) WSACleanup();
	m_Wsa_data = moved_from.m_Wsa_data;
	m_Moved_from = moved_from.m_Moved_from;
	// make sure moved_from doesnt call WSACleanup() in destructor
	moved_from.m_Moved_from = 1;
	return *this;
}

Winsock_api_init::~Winsock_api_init() noexcept
{
	if (!m_Moved_from) WSACleanup();
}

Result<Success<Winsock_api_init>, Wsa_error>
Winsock_api_init::Winsock_api_startup()
{
	WSADATA wsadata;
	int startup_result = WSAStartup(MAKEWORD(2, 2), &wsadata);

	if (startup_result == 0)
		return Winsock_api_init(wsadata);
	else
		return Wsa_error{ startup_result };
}

