#include <iostream>

#include<type_traits>

#include "Winsock_api_init.h"
#include "Address_lookup.h"
#include "Socket.h"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

const char* hostname_cstr = "localhost";
const char* port_cstr = "42069";

// enable to show delimeters between the data blocks
const bool print_buffer_delimiters = true;
const int socket_read_buffer_length = 2048;

class Socket;
void Run_socket_read_loop(Socket& connection_socket);
void Run_socket_write_loop(Socket& connection_socket);

using namespace results;

// We create a socket and connect it.
// Then
// - Start a socket reader thread that reads bytes and writes them to console.
// - Start a socket writer thread that reads lines from console and writes to the socket.
// While
// - Main thread waits for both reader threads to end and then cleanup.
//       (Though neither thread ends unless there are network errors:
//        normal exit is user killing the program with ctrl-C.)
//
// If everything works we get:
// - User types lines into console.
// - The data is pingponged via an echo server.
// - Received in the reader thread and written to console.
// - If/when the connection goes down we _eventually_ exit.
//   (We dont exit till both threads has ended - i.e. seen network errors.)
//
// To keep the code simple:
// - user cannot retry if connecting fails
// - user can not reconnect
// - cannot exit the program from the program (use ctrl-C)
int main()
{
    std::cout << "Hellu second winsock echo client!" << std::endl;
    std::cout << "Connecting..." << std::endl;

    // 1) Initialize the winsock api.
    // 2) Lookup addresses for our host and port.
    // 3) Try connecting to each address in order.
    //    For each address:
    //      a) Create a new socket.
    //      b) Try connecting our socket to the adress.
    // 4) Start a thread reading from socket and writing to standard out.
    // 5) start a thread reading from standard in and writing to socket.
    // 6) Do cleanup and exit. (Though we normally wont get here.)

    // 1) Initialize the winsock api.
    Winsock_api_init winsock_init{};
    if (!winsock_init.Success())
    {
        std::cout << "Failed initializing winsock api: "
            << winsock_init.Error_code() << std::endl;
        return 1;
    }

    // 2) Lookup addresses for our host and port.
    Address_info_list addresses;
    {
        auto address_result = Address_lookup::for_connecting(hostname_cstr, port_cstr);

        // if we got a Success result..
        if (std::holds_alternative<Success<Address_info_list>>(address_result))
        {
            // Because Address_info_list is move-only type we need std::move.
            auto& success_value = std::get<Success<Address_info_list>>(address_result);
            addresses = std::move(success_value.Value);
        }
        else // we had some kind of error
        {
            std::cout << "Failed address lookup: " << address_result << std::endl;
            return 1;
        }
    }

    // 3) Try connecting to each address in order.
    //    For each address:
    //      a) Create a new socket.
    //      b) Try connecting our socket to the adress.
    Socket socket;
    {
        bool success = false;
        for (const auto& address : addresses)
        {
            auto create_result = Socket::Create_socket(address);

            // If we failed in _creating_ a socket we might as well give up here.
            if (!std::holds_alternative<Success<Socket>>(create_result))
            {
                std::cout << "Failed creating socket: " << create_result << std::endl;
                return 1;
            }

            // Because Socket is a move-only type we need std::move.
            auto& create_success = std::get<Success<Socket>>(create_result);
            socket = std::move(create_success.Value);

            auto connect_result = socket.Connect(address);

            // Success! exit the loop
            if (std::holds_alternative<Success<>>(connect_result))
            {
                success = true;
                break;
            }

            // Connection failed - try the next address (if there is one).
            // TODO: print the address that failed
            std::cout << "Failed connecting: " << connect_result << std::endl;
        }

        // Exit if we failed on all addresses.
        if (!success) return 1;
    }

    std::cout << "Connected" << std::endl;

    // 4) start a thread reading from socket and writing to standard out
    auto socket_reader_thread = std::thread(
        [&socket]() { Run_socket_read_loop(socket); }
    );
    // 5) start a thread reading from standard in and writing to socket
    auto socket_writer_thread = std::thread(
        [&socket]() { Run_socket_write_loop(socket); }
    );

    // Wait for both threads to end.
    // Will almost never happen cause there is no way for a user to exit :-)
    socket_reader_thread.join();
    socket_writer_thread.join();

    // 6) Do cleanup and exit. (Though we normally wont get here.)
    // Zero-code-cleanup with raii.
}

void Run_socket_write_loop(Socket& socket)
{
    bool no_fail_yet = true;
    std::string line;
    while (no_fail_yet && std::getline(std::cin, line))
    {
        line.push_back('\n'); // getline removed any line-ending so we add one

        // A single call to send(..) might not send all data.
        // Therefart we loop trying to send any remaining data untill all is sent.
        size_t chars_sent = 0;
        while (no_fail_yet && chars_sent < line.size())
        {
            char* position = line.data() + chars_sent;
            int chars_left_to_send = (int)line.size() - (int)chars_sent;

            auto send_result = socket.Send(position, chars_left_to_send);

            if (std::holds_alternative<Chars_sent>(send_result))
            {
                auto& success_result = std::get<Chars_sent>(send_result);
                chars_sent += success_result.Num_chars;
            }
            else // failed to send data
            {    // exit our loops which will end the thread
                no_fail_yet = false;
                std::cout << "ERROR sending to socket: " << send_result << std::endl;
            }
        }
    }

    std::cout << "Socket writer loop ended." << std::endl;
}

void Run_socket_read_loop(Socket& socket)
{
    // By making the buffer length +1 we gain space to set \0 at the end.
    // Then we can write it as a c-style string to std::cout.
    char buffer[socket_read_buffer_length + 1];

    while (true)
    {
        auto recv_result = socket.Recv(buffer, socket_read_buffer_length);

        // if we faile, write error and end thread
        if (!std::holds_alternative<Chars_read>(recv_result))
        { 
            std::cout << "Failed reading from socket: " << recv_result;
            break;
        }

        // add \0 after bytes read to make it a proper c-style string
        int num_chars_read = std::get<Chars_read>(recv_result).Num_chars;
        buffer[num_chars_read] = 0;

        std::cout << buffer;
        if (print_buffer_delimiters) std::cout << " | ";
        std::cout.flush();
    }

    std::cout << "Socket reader loop ended." << std::endl;
}