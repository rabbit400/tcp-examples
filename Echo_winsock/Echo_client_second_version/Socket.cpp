#include "Socket.h"

using namespace results;

struct Socket::Raii_socket_handle
{
	SOCKET Handle;
	Raii_socket_handle(SOCKET handle) noexcept : Handle{ handle } {}
	~Raii_socket_handle() noexcept { closesocket(Handle); }
};

std::shared_ptr<Socket::Raii_socket_handle>
Socket::Get_handle_ptr()
{
	std::lock_guard guard{ m_Mutex };
	return m_Handle_ptr;
}

Socket::Socket(SOCKET handle)
	: m_Mutex{}, m_Handle_ptr{}
{ // the extra scope removes warning C26111 from visual studio
	{ // probably not really needed but...
		std::lock_guard guard{ m_Mutex };
		m_Handle_ptr = std::make_shared<Raii_socket_handle>(handle);
	}
}

Socket& Socket::operator =(Socket&& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return *this;
	std::scoped_lock guard{ m_Mutex, moved_from.m_Mutex };
	// Note that assigning to our m_Handle here closes any existing handle... eventually.
	m_Handle_ptr = std::move(moved_from.m_Handle_ptr);
	// This should be a no-op after the move...
	// I dont trust my c++ so lets make sure.
	moved_from.m_Handle_ptr.reset();
	return *this;
}

Socket::Socket(Socket&& moved_from) noexcept
	: m_Mutex{}, m_Handle_ptr{}
{
	std::scoped_lock guard{ m_Mutex, moved_from.m_Mutex };
	m_Handle_ptr = std::move(moved_from.m_Handle_ptr);
	// This should be a no-op after the move...
	// I dont trust my c++ so lets make sure.
	moved_from.m_Handle_ptr.reset();
}

Socket::~Socket() noexcept
{
	// By calling Close_eventually() we release the handle pointer with thread-safety.
	// It would still be released in our destructor but not while holding the mutex.
	Close_eventually();
}

void Socket::Close_eventually() noexcept
{
	std::lock_guard guard{ m_Mutex };
	m_Handle_ptr.reset();
}

bool Socket::Is_open()
{
	std::lock_guard guard{ m_Mutex };
	return (bool)m_Handle_ptr;
}

std::variant<Success<>, Socket_closed, Wsa_error>
Socket::Connect(const addrinfo& address)
{
	// We keep the handle alive (not closed in winsock) with a shared_ptr.
	auto handle_ptr = Get_handle_ptr();
	if (!handle_ptr) return Socket_closed();

	// result == 0 means success
	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// result == anything else is not documented, we treat it same as SOCKET_ERROR
	int connect_result = connect(handle_ptr->Handle, address.ai_addr, (int)address.ai_addrlen);

	if (connect_result == 0)
		return Success{};
	else
		return Wsa_error::Get_wsa_last_error();
}

std::variant<Chars_sent, Socket_closed, Wsa_error>
Socket::Send(const char* buffer, int data_length)
{
	// We keep the handle alive (not closed in winsock) with a shared_ptr.
	auto handle_ptr = Get_handle_ptr();
	if (!handle_ptr) return Socket_closed();

	// result == SOCKET_ERROR means error - call WSAGetLastError() for details
	// result >= 0 means num chars written
	// result < 0 isnt documented, we treat it same as SOCKET_ERROR
	int send_result = send(handle_ptr->Handle, buffer, data_length, 0);

	if (send_result >= 0)
		return Chars_sent{ send_result };
	else
		return Wsa_error::Get_wsa_last_error();
}

std::variant<Chars_read, End_of_stream, Socket_closed, Wsa_error>
	Socket::Recv(char* buffer, int max_data_length)
{
	// We keep the handle alive (not closed in winsock) with a shared_ptr.
	auto handle_ptr = Get_handle_ptr();
	if (!handle_ptr) return Socket_closed();

	// result == SOCKET_ERROR means error - call WSAGetLastError()
	// result == 0 means socket is closed neatly
	// result > 0 means num chars read
	// result < 0 isnt documented, we treat it same as SOCKET_ERROR
	int recv_result = recv(handle_ptr->Handle, buffer, max_data_length, 0);
	if (recv_result == SOCKET_ERROR)
		return Wsa_error::Get_wsa_last_error();
	else if (recv_result == 0)
		return End_of_stream{};
	else if (recv_result > 0)
		return Chars_read{ recv_result };
	else
		return Wsa_error::Undocumented_error();
}

std::variant<Success<Socket>, Wsa_error>
Socket::Create_socket(int address_family, int socket_type, int protocol)
{
	SOCKET handle = socket(address_family, socket_type, protocol);
	if (handle == INVALID_SOCKET)
		return Wsa_error::Get_wsa_last_error();
	else
		return Socket(handle);
}

std::variant< Success<Socket>, Wsa_error>
Socket::Create_socket(const addrinfo& address)
{
	return Create_socket(address.ai_family, address.ai_socktype, address.ai_protocol);
}
