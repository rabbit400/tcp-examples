#pragma once
#define WIN32_LEAN_AND_MEAN

#include <WinSock2.h>

#include <concepts>
#include <ostream>
#include <variant>

// Result types for functions that can fail.
// Let them return std::variant<R1, R2, ...> instead of error codes and pointers and stuffs.
// 
// We define several result types:
// Success<>     - operation succeeded
// Success<T>    - operation succeeded and returned value of T
// Chars_sent
// Chars_read
// End_of_stream
// Socket_closed
// Wsa_error     - operation failed with a winsock error code
//
// It is neat when all results can be streamed to std::out.
// Then the std::variant<R1, R2, ...> is streamable as well and error handling is simpler.
// I.e:
//     If result is success:
//         proceed using the value
//     Else: 
//         send the variant to cout or logging
//         retry, return false or whatever
// With no need to look at what the specific error was.
// (Of course the error details are still there if we want them.)
namespace results
{
	template<typename T>
	concept Streamable_concept = requires (const T & t, std::ostream & stream)
	{
		stream << t;
	};


	template <Streamable_concept T, Streamable_concept ... Rest>
	std::ostream& operator << (std::ostream& stream, const std::variant<T, Rest...> & results)
	{
		std::visit(
			[&stream](const auto & result){ stream << result; },
			results
		);
		return stream;
	}

	template <typename ...>
	struct Success;

	template <>
	struct Success<>
	{
		friend std::ostream& operator<<(std::ostream& stream, const Success<>& v)
		{
			stream << "Success";
			return stream;
		}
	};

	template <typename T>
	struct Success<T>
	{
		T Value;
		Success() = default;
		Success(T && t) :Value{ std::forward<T>(t) } {}
		Success(const T & t) :Value{ t } {}

		friend std::ostream& operator<<(std::ostream & stream, const Success<T>&v)
			requires Streamable_concept<T>
		{
			return stream << "Success with value: " << v.Value;
		}
		friend std::ostream& operator<<(std::ostream & stream, const Success<T>&v)
			requires (!Streamable_concept<T>)
		{
			return stream << "Success with non-streamable value";
		}
	};

	struct Chars_sent
	{
		int Num_chars;
		Chars_sent(int n) :Num_chars{ n } {}
		friend std::ostream& operator<<(std::ostream& stream, const Chars_sent& v)
		{
			return stream << "Sent " << v.Num_chars << " chars";
		}
	};

	struct Chars_read
	{
		int Num_chars;
		Chars_read(int n) :Num_chars{ n } {}
		friend std::ostream& operator<<(std::ostream& stream, const Chars_read& v)
		{
			return stream << "Received " << v.Num_chars << " chars";
		}
	};

	struct End_of_stream
	{
		friend std::ostream& operator<<(std::ostream& stream, const End_of_stream& v)
		{
			return stream << "End of stream";
		}
	};

	struct Socket_closed
	{
		friend std::ostream& operator<<(std::ostream& stream, const Socket_closed& v)
		{
			return stream << "Socket closed";
		}
	};

	struct Wsa_error
	{
		int Error_code;
		Wsa_error(int error) : Error_code { error } {}

		friend std::ostream& operator<<(std::ostream& stream, const Wsa_error& v)
		{
			return stream << "Winsock api error: " << v.Error_code;
		}

		static Wsa_error WSA_Error_not_socket() { return Wsa_error(WSAENOTSOCK); }
		static Wsa_error Get_wsa_last_error()
		{
			return Wsa_error{ WSAGetLastError() };
		}
		static Wsa_error Undocumented_error() { return Wsa_error(WSASYSCALLFAILURE); }
	};
}

