#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

class Winsock_api_init
{
	WSADATA m_Wsa_data;
	int m_Result_code;
public:
	Winsock_api_init();
	~Winsock_api_init();
	Winsock_api_init(Winsock_api_init&& moved_from) noexcept;
	Winsock_api_init& operator = (Winsock_api_init&& moved_from) noexcept;

	int Error_code() { return m_Result_code; }
	bool Success() { return m_Result_code == 0; }
	const WSADATA& Wsa_data() const { return m_Wsa_data; }
};
