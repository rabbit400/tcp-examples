#pragma once

#include <string>
#include <optional>
#include <variant>

#include "Address_info_list.h"
#include "results.h"

class Address_lookup
{
public:
	static std::variant<results::Success<Address_info_list>, results::Wsa_error>
		for_connecting(const std::string& host, const std::string& port_or_servicename);
	static std::variant<results::Success<Address_info_list>, results::Wsa_error>
		for_listening(const std::optional<std::string>& host, const std::string& port_or_servicename);
};
