#include "Address_info_list.h"

#define WIN32_LEAN_AND_MEAN
#include <Ws2tcpip.h>

void Address_info_list::move_from(Address_info_list& moved_from) noexcept
{
	if (this == &moved_from) // self assignment
		return;
	reset();
	m_Head = moved_from.m_Head;
	moved_from.m_Head = nullptr;
}

Address_info_list::
Address_info_list(addrinfo* head)
	: m_Head{ head }
{}

Address_info_list::
Address_info_list(Address_info_list&& moved_from) noexcept
{
	move_from(moved_from);
}

Address_info_list&
Address_info_list::
operator = (Address_info_list&& moved_from) noexcept
{
	move_from(moved_from);
	return *this;
}

Address_info_list::
~Address_info_list()
{
	reset();
}

void Address_info_list::reset()
{
	if (m_Head != nullptr)
	{
		freeaddrinfo(m_Head);
		m_Head = nullptr;
	}
}

Address_info_list::Iterator
Address_info_list::begin() const
{
	return Iterator(m_Head);
}

Address_info_list::Iterator
Address_info_list::end() const
{
	return Iterator();
}
