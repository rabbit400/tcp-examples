#define WIN32_LEAN_AND_MEAN

#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <iostream>
#include <thread>
#include <string>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
// This works on my machine...
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

const char* hostname_cstr = "localhost";
const char* port_cstr = "42069";

// enable to show delimeters between the data blocks
const bool print_buffer_delimiters = true;
const int socket_read_buffer_length = 2048;

void Run_socket_read_loop(SOCKET connection_socket);
void Run_socket_write_loop(SOCKET connection_socket);

// We create a socket and connect it.
// Then
// - Start a socket reader thread that reads bytes and writes them to console.
// - Start a socket writer thread that reads lines from console and writes to the socket.
// While
// - Main thread waits for both reader threads to end and then cleanup.
//       (Though neither thread ends unless there are network errors:
//        normal exit is user killing the program with ctrl-C.)
//
// If everything works we get:
// - User types lines into console.
// - The data is pingponged via an echo server.
// - Received in the reader thread and written to console.
// - If/when the connection goes down we _eventually_ exit.
//   (We dont exit till both threads has ended - i.e. seen network errors.)
//
// To keep the code simple:
// - user cannot retry if connecting fails
// - user can not reconnect
// - cannot exit the program from the program (use ctrl-C)
int main()
{
    std::cout << "Hellu winsock echo client!" << std::endl;
    std::cout << "Connecting..." << std::endl;

    // 1) Initialize the winsock api.
    // 2) Lookup addresses for our host and port.
    // 3) Try connecting to each address in order.
    //    For each address:
    //      a) Create a new socket.
    //      b) Try connecting our socket to the adress.
    // 4) Start a thread reading from socket and writing to standard out.
    // 5) start a thread reading from standard in and writing to socket.
    // 6) Do cleanup and exit. (Though we normally wont get here.)

    // 1) Initialize the winsock api.
    WSADATA wsa_data;
    int wsa_startup_result = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (wsa_startup_result != 0)
    {
        std::cout << "Failed initializing winsock api; WSAStartup failed: "
            << wsa_startup_result
            << std::endl;
        return 1;
    }
    // we now should call WSACleanup() to clean up after us

    // 2) get a linked list of addresses for our host and port
    addrinfo* connection_addresses = NULL;
    {
        struct addrinfo hints;
        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_INET; // for IPv4
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        int get_address_result
            = getaddrinfo(hostname_cstr, port_cstr, &hints, &connection_addresses);
        if (get_address_result != 0)
        {
            std::cout << "getaddrinfo failed with error: "
                << get_address_result
                << std::endl;
            WSACleanup();
            return 1;
        }
        // TODO: write the addresses we found
    }
    // we now should call freeaddrinfo(address_result) to clean up after us

    // 3) Try connecting to each address in order.
    // We leave the loop on first success.
    // If all addresses fail: our_socket = INVALID_SOCKET
    // Initialized as INVALID_SOCKET to be correct for empty list of addresses.
    SOCKET our_socket = INVALID_SOCKET;
    for (addrinfo* current_address = connection_addresses;
        current_address != NULL;
        current_address = current_address->ai_next)
    {
        // 3a) create a new fresh socket
        //        on fail we cleanup everything and exit program

        // On success socket(..) returns a socket handle/id.
        // On failure socket(..) returns INVALID_SOCKET and the
        // error code is found by calling WSAGetLastError() immediately.
        our_socket = socket(
            current_address->ai_family,
            current_address->ai_socktype,
            current_address->ai_protocol);
        if (our_socket == INVALID_SOCKET)
        {
            auto error_code = WSAGetLastError();
            std::cout << "Failed creting socket, WSAGetLastError(): "
                << error_code
                << std::endl;
            freeaddrinfo(connection_addresses);
            WSACleanup();
            return 1;
        }
        // we now should call closesocket(our_socket) to clean up after us

        // 3b) try connecting our socket to the adress
        //        on fail we cleanup the socket and try next address

        // On success connect(..) returns zero.
        // On failure socket(..) returns SOCKET_ERROR and the
        // error code is found by calling WSAGetLastError() immediately.
        int connect_result = connect(
            our_socket,
            current_address->ai_addr,
            (int)current_address->ai_addrlen);
        if (connect_result != 0)
        {
            // TODO: write the address that failed
            std::cout << "Failed connecting to server, WSAGetLastError(): "
                << WSAGetLastError()
                << std::endl;
            closesocket(our_socket);
            our_socket = INVALID_SOCKET;
        }
        else
        {
            break; // we connected! end the loop and send stuffs!
        }
    }

    // regardless we wont use the list of addresses anymore so clean them out now
    freeaddrinfo(connection_addresses);

    // if we failed to connect we exit (any opened socket was clsoed already)
    if (our_socket == INVALID_SOCKET)
    {
        std::cout << "Giving up on connecting." << std::endl;
        WSACleanup();
        return 1;
    }
    
    std::cout << "Connected" << std::endl;
    // 4) start a thread reading from socket and writing to standard out
    auto socket_reader_thread = std::thread(
        [our_socket]() { Run_socket_read_loop(our_socket); }
    );
    // 5) start a thread reading from standard in and writing to socket
    auto socket_writer_thread = std::thread(
        [our_socket]() { Run_socket_write_loop(our_socket); }
    );
    
    // wait for both threads to end and then do our cleanup...
    //   we will almost never do this because there is no way for a user to exit :-)
    //   still good to show proper cleanup
    // NOTE neither of the loops notifies or stops the other on error
    //   obviously we should stop the other thread when one gets an error... in later examples

    // 6) cleanup before exit (though we normally wont get here )
    socket_reader_thread.join();
    socket_writer_thread.join();
    closesocket(our_socket);
    WSACleanup();
}

void Run_socket_write_loop(SOCKET our_socket)
{
    std::string line;
    while (std::getline(std::cin, line))
    {
        line.push_back('\n'); // getline removed any line-ending so we add one

        // A single call to send(..) might not send all data.
        // Therefart we loop trying to send any remaining data untill all is sent.
        size_t chars_sent = 0;
        while (chars_sent < line.size())
        {
            char* position = line.data() + chars_sent;
            int chars_left_to_send = (int)line.size() - (int)chars_sent;
            // result == SOCKET_ERROR:
            //     error - call WSAGetLastError() for details
            // result >= 0:
            //     means num chars written
            // result < 0:
            //     not documented
            int send_result = send(
                our_socket,
                position,
                chars_left_to_send,
                0);

            if (send_result >= 0)
            {
                chars_sent += send_result;
            }
            else // if we dont succeeded we write error info and end thread
            {
                if (send_result == SOCKET_ERROR) // if recv returned an error
                    std::cout
                    << "ERROR sending to socket, WSAGetLastError(): "
                    << WSAGetLastError()
                    << std::endl;
                else // if recv returned an undocumented result
                    std::cout
                    << "Undocumented send result: "
                    << send_result
                    << ", WSAGetLastError(): "
                    << WSAGetLastError()
                    << std::endl;

                std::cout << "Socket writer loop ended." << std::endl;
                return;
            }
        }
    }

    std::cout << "Socket writer loop ended." << std::endl;
}

void Run_socket_read_loop(SOCKET our_socket)
{
    // By making the buffer length +1 we gain space to set \0 at the end.
    // Then we can write it as a c-style string to std::cout.
    char buffer[socket_read_buffer_length + 1];

    while (true)
    {
        // recv_result > 0:
        //     success, the value is number of chars read into our buffer
        // recv_result == 0:
        //     socket is gracefully closed for reading
        //     this probably means the other side sent shutdown/"end of stream"
        // recv_result == SOCKET_ERROR:
        //     error - call WSAGetLastError() for error code
        // recv_result < 0 (not SOCKET_ERROR):
        //     not documented
        int recv_result = recv(our_socket, buffer, socket_read_buffer_length, 0);
        
        if (recv_result > 0)
        {
            // recv_result is here how many bytes were put into our buffer
            int num_chars_read = recv_result;
            // add \0 after these bytes to make it a proper c-style string
            buffer[num_chars_read] = 0;
            std::cout << buffer;
            if (print_buffer_delimiters) std::cout << " | ";
            std::cout.flush();
        }
        else // if we dont succeeded we write error info and end thread
        {
            if (recv_result == 0) // if socket was closed for reading
                std::cout
                << "Could not read, socket has gracefully closed for reading."
                << std::endl;
            else if (recv_result == SOCKET_ERROR) // if recv returned an error
                std::cout
                << "ERROR reading from socket, WSAGetLastError(): "
                << WSAGetLastError()
                << std::endl;
            else // if recv returned an undocumented result
                std::cout
                << "Undocumented recv result: "
                << recv_result
                << ", WSAGetLastError(): "
                << WSAGetLastError()
                << std::endl;

            std::cout << "Socket reader loop ended." << std::endl;
            return;
        }
    }
}
